
let _ =
    let prog = Parser.parse Sys.argv.(1) in
    let prog = PostParsingTransform.transform prog in
    let (prog, first_free_mem_cell) = ScopeSolver.solve_scopes prog in
    let prog = SimpleConstantFolding.optimize prog in
    CalledProcs.build_info prog;
    if Errors.ok () then begin
        let code = Code.create () in
        CodeGen.gen_program first_free_mem_cell code prog;
        let code = CodeOptimise.optimise code in
        Code.print code
    end
