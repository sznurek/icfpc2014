
(* Conventions
   H - Stack pointer
*)

let next_fresh_id = ref 0
let fresh_label () =
    let r = !next_fresh_id in
    next_fresh_id := r + 1;
    "l@" ^ (string_of_int r)

type state_data =
    { sd_free_regs         : string list
    ; sd_free_mem_cell     : int
    ; sd_inline_exit_label : string option
    ; sd_code              : Code.t
    ; sd_proc_defs         : Ast.proc list
    }

class state data =
object
    method add_instr ?comment instr =
        Code.add_instr data.sd_code ?comment instr

    method add_label label =
        Code.add_label data.sd_code label

    method add_info msg =
        Code.add_info data.sd_code msg

    method first_free_mem_cell = data.sd_free_mem_cell

    method alloc = 
        ( data.sd_free_mem_cell
        , new state { data with sd_free_mem_cell = data.sd_free_mem_cell + 1 }
        )

    method alloc_reg =
        match data.sd_free_regs with
        | [] -> Errors.fatal_error "Jeb sie! Too complex array expression."
        | reg :: regs -> (reg, new state { data with sd_free_regs = regs })

    method inlined_state l = new state { data with sd_inline_exit_label = Some l }
    method inline_exit_label = data.sd_inline_exit_label

    method get_proc name =
        List.find (fun proc -> proc.Ast.proc_name = name) data.sd_proc_defs
end

let create_init_state first_free_mem_cell code prog =
    new state
        { sd_free_regs         = ["G"; "F"; "E"; "D"; "C"]
        ; sd_free_mem_cell     = first_free_mem_cell
        ; sd_inline_exit_label = None
        ; sd_code              = code
        ; sd_proc_defs         = prog.Ast.prog_procs
        }

let gen_preamble code =
    Code.add_instr code (Code.MOV(Code.Reg("H"), Code.Const(255)))

let convert_constant pos c =
    if c > 255 || c < -128 then
        Errors.warning_p pos "Constant %d oudside of bounds." c;
    (c land 255)

let addr_to_mem addr =
    match addr.Ast.e_kind with
    | Ast.EConst c ->
        let c' = convert_constant addr.Ast.e_pos c in
        Some (Code.Mem c')
    | _ -> None

let expr_to_param expr =
    match expr.Ast.e_kind with
    | Ast.ELValue { Ast.lv_kind = Ast.LV_Reg reg } ->
        Some (Code.Reg reg)
    | Ast.ELValue { Ast.lv_kind = Ast.LV_Mem addr } ->
        addr_to_mem addr
    | _ -> None

let expr_to_param_or_const expr =
    match expr.Ast.e_kind with
    | Ast.EConst c -> Some (Code.Const (convert_constant expr.Ast.e_pos c))
    | _ -> expr_to_param expr

let op_mov target src = Code.MOV(target, src)
let mk_op op =
    match op with
    | Ast.OpAdd -> fun p1 p2 -> Code.ADD(p1, p2)
    | Ast.OpSub -> fun p1 p2 -> Code.SUB(p1, p2)
    | Ast.OpMul -> fun p1 p2 -> Code.MUL(p1, p2)
    | Ast.OpDiv -> fun p1 p2 -> Code.DIV(p1, p2)
    | Ast.OpAnd -> fun p1 p2 -> Code.AND(p1, p2)
    | Ast.OpOr  -> fun p1 p2 -> Code.OR (p1, p2)
    | Ast.OpXor -> fun p1 p2 -> Code.XOR(p1, p2)

let is_mov f =
    match f (Code.Const 0) (Code.Const 0) with
    | Code.MOV _ -> true
    | _ -> false

let convert_param p =
    match p.Ast.ap_kind with
    | Ast.AP_Const   c -> Code.Const(convert_constant p.Ast.ap_pos c)
    | Ast.AP_Label   l -> Code.Label l
    | Ast.AP_Reg     r -> Code.Reg r
    | Ast.AP_RegMem  r -> Code.RegMem r
    | Ast.AP_VarAddr _ -> failwith "Bad asm instr param in codeGen."
    | Ast.AP_VarVal  _ -> failwith "Bad asm instr param in codeGen."
    | Ast.AP_Mem     m -> Code.Mem m

let gen_asm_code (st : state) instr =
    match instr.Ast.i_kind with
    | Ast.I_MOV(p1, p2) -> st#add_instr(Code.MOV(convert_param p1, convert_param p2))
    | Ast.I_INC p       -> st#add_instr(Code.INC(convert_param p))
    | Ast.I_DEC p       -> st#add_instr(Code.DEC(convert_param p))
    | Ast.I_ADD(p1, p2) -> st#add_instr(Code.ADD(convert_param p1, convert_param p2))
    | Ast.I_SUB(p1, p2) -> st#add_instr(Code.SUB(convert_param p1, convert_param p2))
    | Ast.I_MUL(p1, p2) -> st#add_instr(Code.MUL(convert_param p1, convert_param p2))
    | Ast.I_DIV(p1, p2) -> st#add_instr(Code.DIV(convert_param p1, convert_param p2))
    | Ast.I_AND(p1, p2) -> st#add_instr(Code.AND(convert_param p1, convert_param p2))
    | Ast.I_OR (p1, p2) -> st#add_instr(Code.OR (convert_param p1, convert_param p2))
    | Ast.I_XOR(p1, p2) -> st#add_instr(Code.XOR(convert_param p1, convert_param p2))
    | Ast.I_JEQ(l, p1, p2) -> st#add_instr(Code.JEQ(l, convert_param p1, convert_param p2))
    | Ast.I_JLT(l, p1, p2) -> st#add_instr(Code.JLT(l, convert_param p1, convert_param p2))
    | Ast.I_JGT(l, p1, p2) -> st#add_instr(Code.JGT(l, convert_param p1, convert_param p2))
    | Ast.I_INT n -> st#add_instr(Code.INT n)
    | Ast.I_HLT   -> st#add_instr Code.HLT

let rec gen_expr_code (st : state) op target expr =
    match expr.Ast.e_kind with
    | Ast.EConst c ->
        let c' = convert_constant expr.Ast.e_pos c in
        st#add_instr (op target (Code.Const c'))
    | Ast.ELValue lv ->
        begin match lv.Ast.lv_kind with
        | Ast.LV_Reg reg  -> st#add_instr (op target (Code.Reg reg))
        | Ast.LV_Mem addr ->
            begin match addr_to_mem addr with
            | None ->
                let (reg, st') = st#alloc_reg in
                gen_expr_code st op_mov (Code.Reg reg) addr;
                st#add_instr (op target (Code.RegMem reg))
            | Some mem ->
                st#add_instr (op target mem)
            end
        | _ -> failwith "Bad code in codeGen"
        end
    | Ast.EOp(expr_op, e1, e2) ->
        begin match expr_to_param e1 with
        | Some p when p = target && is_mov op ->
            gen_expr_code st (mk_op expr_op) target e2
        | _ ->
            let (mem, st') = st#alloc in
            gen_expr_code st op_mov (Code.Mem mem) e1;
            gen_expr_code st' (mk_op expr_op) (Code.Mem mem) e2;
            st#add_instr (op target (Code.Mem mem))
        end

let gen_assign_code (st : state) lv expr =
    match lv.Ast.lv_kind with
    | Ast.LV_Reg reg  -> gen_expr_code st op_mov (Code.Reg reg) expr
    | Ast.LV_Mem addr ->
        begin match addr_to_mem addr with
        | None ->
            let (reg, st') = st#alloc_reg in
            gen_expr_code st op_mov (Code.Reg reg) addr;
            gen_expr_code st' op_mov (Code.RegMem reg) expr
        | Some mem -> gen_expr_code st op_mov mem expr
        end
    | _ -> failwith "Bad code in codeGen"

let gen_jump_code (st : state) op tbl fbl e1 e2 =
    match expr_to_param_or_const e1 with
    | Some p ->
        gen_expr_code st op p e2;
        st#add_instr (Code.MOV(Code.Reg "PC", Code.Label fbl))
    | None ->
        let (mem, st') = st#alloc in
        gen_expr_code st op_mov (Code.Mem mem) e1;
        gen_expr_code st' op (Code.Mem mem) e2;
        st#add_instr (Code.MOV(Code.Reg "PC", Code.Label fbl))

let rec gen_cond_code (st : state) tbl fbl cond =
    match cond.Ast.c_kind with
    | Ast.CEq(e1, e2) ->
        gen_jump_code st (fun p1 p2 -> Code.JEQ(tbl, p1, p2)) tbl fbl e1 e2
    | Ast.CLt(e1, e2) ->
        gen_jump_code st (fun p1 p2 -> Code.JLT(tbl, p1, p2)) tbl fbl e1 e2
    | Ast.CGt(e1, e2) ->
        gen_jump_code st (fun p1 p2 -> Code.JGT(tbl, p1, p2)) tbl fbl e1 e2
    | Ast.CAnd(c1, c2) ->
        let l = fresh_label () in
        gen_cond_code st l fbl c1;
        st#add_label l;
        gen_cond_code st tbl fbl c2
    | Ast.COr(c1, c2) ->
        let l = fresh_label () in
        gen_cond_code st tbl l c1;
        st#add_label l;
        gen_cond_code st tbl fbl c2
    | Ast.CNot c ->
        gen_cond_code st fbl tbl c

let rec gen_stmt_code (st : state) stmt =
    match stmt.Ast.s_kind with
    | Ast.SScope(ss, _) ->
        List.iter (gen_stmt_code st) ss
    | Ast.SInt n ->
        st#add_instr (Code.INT n)
    | Ast.SAssign(lv, expr) ->
        gen_assign_code st lv expr
    | Ast.SIf(cond, s1, s2) ->
        let tbl = fresh_label () in
        let fbl = fresh_label () in
        let exitl = fresh_label () in
        gen_cond_code st tbl fbl cond;
        st#add_label tbl;
        gen_stmt_code st s1;
        st#add_instr (Code.MOV(Code.Reg "PC", Code.Label exitl));
        st#add_label fbl;
        gen_stmt_code st s2;
        st#add_label exitl
    | Ast.SWhile(cond, s) ->
        let cond_l  = fresh_label () in
        let begin_l = fresh_label () in
        let end_l   = fresh_label () in
        st#add_label cond_l;
        gen_cond_code st begin_l end_l cond;
        st#add_label begin_l;
        gen_stmt_code st s;
        st#add_instr (Code.MOV(Code.Reg "PC", Code.Label cond_l));
        st#add_label end_l
    | Ast.SVar _ | Ast.SArray _ -> ()
    | Ast.SCall(name, inlined) ->
        if inlined then begin
            let l = fresh_label () ^ "inlined_ret_" ^ name in
            st#add_info ("inlined proc " ^ name);
            let st' = st#inlined_state l in
            gen_stmt_code st' (st#get_proc name).Ast.proc_body;
            st#add_info "inlined end";
            st#add_label l
        end else begin
            let ret_point = fresh_label () in
            st#add_instr (Code.MOV(Code.RegMem "H", Code.Label ret_point));
            st#add_instr (Code.DEC(Code.Reg "H"));
            st#add_instr (Code.MOV(Code.Reg "PC", Code.Label name));
            st#add_label ret_point
        end
    | Ast.SRet ->
        begin match st#inline_exit_label with
        | None ->
            st#add_instr (Code.INC(Code.Reg "H"));
            st#add_instr (Code.MOV(Code.Reg "PC", Code.RegMem "H"))
        | Some l ->
            st#add_instr (Code.MOV(Code.Reg "PC", Code.Label l))
        end
    | Ast.SHalt ->
        st#add_instr Code.HLT
    | Ast.SGoto l ->
        st#add_instr (Code.MOV(Code.Reg "PC", Code.Label l));
    | Ast.SLabel l ->
        st#add_label l
    | Ast.SAsm instr_list ->
        List.iter (gen_asm_code st) instr_list

let get_proc_code (st : state) proc =
    st#add_label proc.Ast.proc_name;
    gen_stmt_code st proc.Ast.proc_body;
    st#add_instr (Code.INC(Code.Reg "H"));
    st#add_instr (Code.MOV(Code.Reg "PC", Code.RegMem "H"))

let gen_program first_free_mem_cell code prog =
    gen_preamble code;
    let st = create_init_state first_free_mem_cell code prog in
    gen_stmt_code st prog.Ast.prog_body;
    List.iter (fun proc ->
        if CalledProcs.is_called proc.Ast.proc_name then get_proc_code st proc
    ) prog.Ast.prog_procs
