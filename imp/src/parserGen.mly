/* File: parserGen.mly */
%{
let mk_stmt n kind =
    { Ast.s_pos  = Parsing.rhs_start_pos n
    ; Ast.s_kind = kind
    }

let mk_cond n kind =
    { Ast.c_pos  = Parsing.rhs_start_pos n
    ; Ast.c_kind = kind
    }

let mk_expr n kind =
    { Ast.e_pos  = Parsing.rhs_start_pos n
    ; Ast.e_kind = kind
    }

let mk_lvalue n kind =
    { Ast.lv_pos  = Parsing.rhs_start_pos n
    ; Ast.lv_kind = kind
    }

let mk_instr kind =
    { Ast.i_pos  = Parsing.rhs_start_pos 1
    ; Ast.i_kind = kind
    }

let mk_asm_param kind =
    { Ast.ap_pos  = Parsing.rhs_start_pos 1
    ; Ast.ap_kind = kind
    }

let change_cond_pos n c = { c with Ast.c_pos = Parsing.rhs_start_pos n }
let change_expr_pos n e = { e with Ast.e_pos = Parsing.rhs_start_pos n }

let mk_scope n ss =
    mk_stmt n (Ast.SScope(ss, Fresh.fresh_id()))

%}

%token <string> ID
%token <string> PATH
%token <int>    NUM
%token EOF
%token BR_OPN BR_CLS CBR_OPN CBR_CLS SBR_OPN SBR_CLS
%token LOG_AND ASSIGN ASTERISK EQ LT LE GT GE NEQ MINUS LOG_OR PLUS SEMI 
%token SLASH XOR AND OR LOG_NOT COLON COMMA
%token KW_DO KW_DONE KW_ELIF KW_ELSE KW_IF KW_THEN KW_CALL KW_END KW_INT 
%token KW_PROC KW_REG KW_RET KW_VAR KW_WHILE KW_HALT KW_ASM KW_GOTO KW_INLINE
%token I_MOV I_INC I_DEC I_ADD I_SUB I_MUL I_DIV I_AND I_OR I_XOR I_JLT I_JGT 
%token I_JEQ I_INT I_HLT

%start main
%type <Ast.program> main

%left LOG_OR
%left LOG_AND
%left LOG_NOT
%left EQ NEQ LT LE GT GE
%left AND OR XOR
%left PLUS MINUS
%left ASTERISK SLASH

%%

/* ========================================================================= */

main
: proc_list stmt_list EOF {
        { Ast.prog_procs = $1
        ; Ast.prog_body  = mk_scope 2 $2
        }
    }
;

proc_list
: /* empty */ { [] }
| proc_def proc_list { $1 :: $2 }
;

proc_def
: KW_PROC ID stmt_list KW_END {
        { Ast.proc_pos  = Parsing.rhs_start_pos 1
        ; Ast.proc_name = $2
        ; Ast.proc_body = mk_scope 3 $3
        }
    }
;

/* ========================================================================= */

stmt
: CBR_OPN stmt_list CBR_CLS { mk_scope 1 $2 }
| KW_INT const_expr_atom    { mk_stmt 1 (Ast.SInt $2) }
| lvalue ASSIGN expr { mk_stmt 2 (Ast.SAssign($1, $3)) }
| KW_IF cond KW_THEN stmt_list if_stmt_cont {
        mk_stmt 1 (Ast.SIf($2, mk_scope 4 $4, $5))
    }
| KW_WHILE cond KW_DO stmt_list KW_DONE {
        mk_stmt 1 (Ast.SWhile($2, mk_scope 4 $4))
    }
| KW_VAR ID { mk_stmt 1 (Ast.SVar $2) }
| KW_VAR ID SBR_OPN const_expr SBR_CLS {
        mk_stmt 1 (Ast.SArray($2, $4))
    }
| KW_CALL   ID { mk_stmt 1 (Ast.SCall($2, false)) }
| KW_INLINE ID { mk_stmt 1 (Ast.SCall($2, true)) }
| KW_RET     { mk_stmt 1 Ast.SRet }
| KW_HALT    { mk_stmt 1 Ast.SHalt }
| KW_GOTO ID { mk_stmt 1 (Ast.SGoto $2) }
| ID COLON   { mk_stmt 1 (Ast.SLabel $1) }
| KW_ASM asm_instr_list KW_END { mk_stmt 1 (Ast.SAsm $2) }
;

if_stmt_cont
: KW_ELSE stmt_list KW_END { mk_scope 2 $2 }
| KW_ELIF cond KW_THEN stmt_list if_stmt_cont { 
        mk_stmt 1 (Ast.SIf($2, mk_scope 4 $4, $5))
    }
;

stmt_list
: /* empty */ { [] }
| stmt stmt_list { $1 :: $2 }
;

/* ========================================================================= */

asm_instr
: I_MOV asm_param COMMA asm_param { mk_instr (Ast.I_MOV($2, $4)) }
| I_INC asm_param                 { mk_instr (Ast.I_INC $2) }
| I_DEC asm_param                 { mk_instr (Ast.I_DEC $2) }
| I_ADD asm_param COMMA asm_param { mk_instr (Ast.I_ADD($2, $4)) }
| I_SUB asm_param COMMA asm_param { mk_instr (Ast.I_SUB($2, $4)) }
| I_MUL asm_param COMMA asm_param { mk_instr (Ast.I_MUL($2, $4)) }
| I_DIV asm_param COMMA asm_param { mk_instr (Ast.I_DIV($2, $4)) }
| I_AND asm_param COMMA asm_param { mk_instr (Ast.I_AND($2, $4)) }
| I_OR  asm_param COMMA asm_param { mk_instr (Ast.I_OR ($2, $4)) }
| I_XOR asm_param COMMA asm_param { mk_instr (Ast.I_XOR($2, $4)) }
| I_JEQ ID COMMA asm_param COMMA asm_param { mk_instr (Ast.I_JEQ($2, $4, $6)) }
| I_JLT ID COMMA asm_param COMMA asm_param { mk_instr (Ast.I_JLT($2, $4, $6)) }
| I_JGT ID COMMA asm_param COMMA asm_param { mk_instr (Ast.I_JGT($2, $4, $6)) }
| I_INT const_expr_atom { mk_instr (Ast.I_INT $2) }
| I_HLT     { mk_instr Ast.I_HLT }
;

asm_instr_list
: /* empty */ { [] }
| asm_instr asm_instr_list { $1 :: $2 }
;

asm_param
: const_expr_atom { mk_asm_param (Ast.AP_Const $1) }
| ID        { mk_asm_param (Ast.AP_Label $1) }
| KW_REG ID { mk_asm_param (Ast.AP_Reg $2) }
| SBR_OPN KW_REG ID SBR_CLS { mk_asm_param (Ast.AP_RegMem $3) }
| SBR_OPN ID offset SBR_CLS { mk_asm_param (Ast.AP_VarAddr($2, $3)) }
| SBR_OPN const_expr SBR_CLS       { mk_asm_param (Ast.AP_Mem $2) }
| CBR_OPN ID offset CBR_CLS { mk_asm_param (Ast.AP_VarVal($2, $3)) }
;

offset
: /* empty */ { 0  }
| PLUS  const_expr_atom { $2 }
| MINUS const_expr_atom { - $2 }
;

/* ========================================================================= */

cond
: expr EQ  expr { mk_cond 2 (Ast.CEq($1, $3)) }
| expr NEQ expr { mk_cond 2 (Ast.CNot (mk_cond 2 (Ast.CEq($1, $3)))) }
| expr LT  expr { mk_cond 2 (Ast.CLt($1, $3)) }
| expr LE  expr { mk_cond 2 (Ast.CNot (mk_cond 2 (Ast.CGt($1, $3)))) }
| expr GT  expr { mk_cond 2 (Ast.CGt($1, $3)) }
| expr GE  expr { mk_cond 2 (Ast.CNot (mk_cond 2 (Ast.CLt($1, $3)))) }
| cond LOG_OR  cond { mk_cond 2 (Ast.COr($1, $3)) }
| cond LOG_AND cond { mk_cond 2 (Ast.CAnd($1, $3)) }
| LOG_NOT cond { mk_cond 1 (Ast.CNot $2) }
| BR_OPN cond BR_CLS { change_cond_pos 1 $2 }
;

/* ========================================================================= */

expr
: NUM    { mk_expr 1 (Ast.EConst $1)  }
| lvalue { mk_expr 1 (Ast.ELValue $1) }
| expr PLUS     expr { mk_expr 2 (Ast.EOp(Ast.OpAdd, $1, $3)) }
| expr MINUS    expr { mk_expr 2 (Ast.EOp(Ast.OpSub, $1, $3)) }
| expr ASTERISK expr { mk_expr 2 (Ast.EOp(Ast.OpMul, $1, $3)) }
| expr SLASH    expr { mk_expr 2 (Ast.EOp(Ast.OpDiv, $1, $3)) }
| expr AND      expr { mk_expr 2 (Ast.EOp(Ast.OpAnd, $1, $3)) }
| expr OR       expr { mk_expr 2 (Ast.EOp(Ast.OpOr,  $1, $3)) }
| expr XOR      expr { mk_expr 2 (Ast.EOp(Ast.OpXor, $1, $3)) }
| BR_OPN expr BR_CLS { change_expr_pos 1 $2 }
;

/* ========================================================================= */

lvalue
: ID { mk_lvalue 1 (Ast.LV_Var $1) }
| ID SBR_OPN expr SBR_CLS { mk_lvalue 1 (Ast.LV_Array($1, $3)) }
| KW_REG ID { mk_lvalue 1 (Ast.LV_Reg $2) }
;

/* ========================================================================= */

const_expr
: const_expr PLUS     const_expr { $1 + $3 }
| const_expr MINUS    const_expr { $1 - $3 }
| const_expr ASTERISK const_expr { $1 * $3 }
| const_expr SLASH    const_expr { $1 / $3 }
| const_expr AND      const_expr { $1 land $3 }
| const_expr OR       const_expr { $1 lor  $3 }
| const_expr XOR      const_expr { $1 lxor $3 }
| const_expr_atom { $1 }
;

const_expr_atom
: NUM    { $1 }
| BR_OPN const_expr BR_CLS { $2 }
;

