
type label = string

type param =
| Const  of int
| Mem    of int
| Reg    of string
| RegMem of string
| Label  of string

type instr =
| MOV of param * param
| INC of param
| DEC of param
| ADD of param * param
| SUB of param * param
| MUL of param * param
| DIV of param * param
| AND of param * param
| OR  of param * param
| XOR of param * param
| JLT of label * param * param
| JEQ of label * param * param
| JGT of label * param * param
| INT of int
| HLT

type code_elem =
| CE_Instr of instr * string option
| CE_Label of label
| CE_Info  of string

type t = code_elem Queue.t

let string_of_param = function
  | Const n  -> string_of_int n
  | Mem m    -> Printf.sprintf "[%d]" m
  | Reg r    -> r
  | RegMem m -> Printf.sprintf "[%s]" m
  | Label l  -> l ^ ":"

let string_of_instr = function
  | MOV (dest, src) ->
      Printf.sprintf "MOV %s, %s" (string_of_param dest) (string_of_param src)
  | INC dest ->
      Printf.sprintf "INC %s" (string_of_param dest)
  | DEC dest ->
      Printf.sprintf "DEC %s" (string_of_param dest)
  | ADD (dest, src) ->
      Printf.sprintf "ADD %s, %s" (string_of_param dest) (string_of_param src)
  | SUB (dest, src) ->
      Printf.sprintf "SUB %s, %s" (string_of_param dest) (string_of_param src)
  | MUL (dest, src) ->
      Printf.sprintf "MUL %s, %s" (string_of_param dest) (string_of_param src)
  | DIV (dest, src) ->
      Printf.sprintf "DIV %s, %s" (string_of_param dest) (string_of_param src)
  | AND (dest, src) ->
      Printf.sprintf "AND %s, %s" (string_of_param dest) (string_of_param src)
  | OR  (dest, src) ->
      Printf.sprintf "OR %s, %s" (string_of_param dest) (string_of_param src)
  | XOR (dest, src) ->
      Printf.sprintf "XOR %s, %s" (string_of_param dest) (string_of_param src)
  | JLT (label, p1, p2) ->
      Printf.sprintf "JLT %s:, %s, %s" label (string_of_param p1) (string_of_param p2)
  | JEQ (label, p1, p2) ->
      Printf.sprintf "JEQ %s:, %s, %s" label (string_of_param p1) (string_of_param p2)
  | JGT (label, p1, p2) ->
      Printf.sprintf "JGT %s:, %s, %s" label (string_of_param p1) (string_of_param p2)
  | INT n ->
      Printf.sprintf "INT %d" n
  | HLT -> "HLT"

let string_of_code_elem = function
  | CE_Instr (instr, Some comment) -> Printf.sprintf "    %s    ; %s" (string_of_instr instr) comment
  | CE_Instr (instr, None)         -> "    " ^ string_of_instr instr
  | CE_Label label                 -> Printf.sprintf  "%s:" label
  | CE_Info  msg                   -> Printf.sprintf  "; %s" msg

let create () = Queue.create ()
let print q =
  Queue.iter (fun ce -> print_endline (string_of_code_elem ce)) q

let add_instr code ?comment instr =
    Queue.push (CE_Instr(instr, comment)) code
let add_label code label =
    Queue.push (CE_Label label) code
let add_info code msg =
    Queue.push (CE_Info msg) code

let to_list code =
    List.rev (Queue.fold (fun l e -> e :: l) [] code)

let of_list l =
    let code = create () in
    List.iter (fun e -> Queue.add e code) l;
    code
