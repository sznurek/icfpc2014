
let next_fresh_id = ref 0

let fresh_id () =
    let r = !next_fresh_id in
    next_fresh_id := r + 1;
    r
