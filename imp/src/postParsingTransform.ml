
let rec add_extra_scopes stmt =
    match stmt.Ast.s_kind with
    | Ast.SScope(ss, id) ->
        { stmt with Ast.s_kind = 
            Ast.SScope(add_extra_scopes_in_scope_begin ss, id) }
    | Ast.SInt _    -> stmt
    | Ast.SAssign _ -> stmt
    | Ast.SIf(cond, stmt1, stmt2) ->
        { stmt with Ast.s_kind =
            Ast.SIf(cond, add_extra_scopes stmt1, add_extra_scopes stmt2)
        }
    | Ast.SWhile(cond, stmt1) ->
        { stmt with Ast.s_kind =
            Ast.SWhile(cond, add_extra_scopes stmt1)
        }
    | Ast.SVar   _ -> stmt
    | Ast.SArray _ -> stmt
    | Ast.SCall  _ -> stmt
    | Ast.SRet     -> stmt
    | Ast.SHalt    -> stmt
    | Ast.SGoto _  -> stmt
    | Ast.SLabel _ -> stmt
    | Ast.SAsm _   -> stmt

and add_extra_scopes_in_scope_begin ss =
    match ss with
    | [] -> []
    | stmt :: ss ->
        begin match stmt.Ast.s_kind with
        | Ast.SVar _ | Ast.SArray _ -> 
            stmt :: add_extra_scopes_in_scope_begin ss
        | _ ->
            add_extra_scopes stmt :: add_extra_scopes_in_scope ss
        end

and add_extra_scopes_in_scope ss =
    match ss with
    | [] -> []
    | stmt :: ss' ->
        begin match stmt.Ast.s_kind with
        | Ast.SVar _ | Ast.SArray _ ->
            [ { Ast.s_pos  = stmt.Ast.s_pos
              ; Ast.s_kind = Ast.SScope(
                    add_extra_scopes_in_scope_begin ss, 
                    Fresh.fresh_id ())
              } ]
        | _ ->
            add_extra_scopes stmt :: add_extra_scopes_in_scope ss'
        end

let add_extra_scopes_in_prog prog =
    { Ast.prog_procs = List.map (fun proc ->
            { proc with Ast.proc_body = add_extra_scopes proc.Ast.proc_body }
        ) prog.Ast.prog_procs
    ; Ast.prog_body  = add_extra_scopes prog.Ast.prog_body
    }

let transform prog =
    add_extra_scopes_in_prog prog
    
