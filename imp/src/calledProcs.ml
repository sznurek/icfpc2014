
let called_set = Hashtbl.create 32
let visited_set = Hashtbl.create 32

let is_proc_named name proc =
    proc.Ast.proc_name = name

let rec build_info_stmt procs stmt =
    match stmt.Ast.s_kind with
    | Ast.SInt _ | Ast.SAssign _ | Ast.SVar _ | Ast.SArray _ | Ast.SRet 
        | Ast.SHalt | Ast.SGoto _ | Ast.SLabel _ | Ast.SAsm _ -> ()
    | Ast.SScope(ss, _) -> List.iter (build_info_stmt procs) ss
    | Ast.SIf(_, s1, s2) ->
        build_info_stmt procs s1;
        build_info_stmt procs s2
    | Ast.SWhile(_, s) ->
        build_info_stmt procs s
    | Ast.SCall(name, inlined) ->
        if not inlined then
            Hashtbl.add called_set name ();
        if not (Hashtbl.mem visited_set name) then begin
            Hashtbl.add visited_set name ();
            build_info_stmt procs (List.find (is_proc_named name) procs).Ast.proc_body
        end

let build_info prog =
    build_info_stmt prog.Ast.prog_procs prog.Ast.prog_body

let is_called name = Hashtbl.mem called_set name
