(* File: lexer.mll *)

{

exception Invalid_character of char
exception Invalid_number    of string

let kw_map = Hashtbl.create 32
let _ =
    Hashtbl.add kw_map "asm"       ParserGen.KW_ASM;
    Hashtbl.add kw_map "goto"      ParserGen.KW_GOTO;
	Hashtbl.add kw_map "do"        ParserGen.KW_DO;
	Hashtbl.add kw_map "done"      ParserGen.KW_DONE;
	Hashtbl.add kw_map "elif"      ParserGen.KW_ELIF;
	Hashtbl.add kw_map "else"      ParserGen.KW_ELSE;
	Hashtbl.add kw_map "if"        ParserGen.KW_IF;
	Hashtbl.add kw_map "then"      ParserGen.KW_THEN;
    Hashtbl.add kw_map "call"      ParserGen.KW_CALL;
    Hashtbl.add kw_map "end"       ParserGen.KW_END;
    Hashtbl.add kw_map "int"       ParserGen.KW_INT;
    Hashtbl.add kw_map "proc"      ParserGen.KW_PROC;
    Hashtbl.add kw_map "reg"       ParserGen.KW_REG;
    Hashtbl.add kw_map "ret"       ParserGen.KW_RET;
    Hashtbl.add kw_map "var"       ParserGen.KW_VAR;
    Hashtbl.add kw_map "while"     ParserGen.KW_WHILE;
    Hashtbl.add kw_map "halt"      ParserGen.KW_HALT;
    Hashtbl.add kw_map "inline"    ParserGen.KW_INLINE;
    
    Hashtbl.add kw_map "MOV" ParserGen.I_MOV;
    Hashtbl.add kw_map "INC" ParserGen.I_INC;
    Hashtbl.add kw_map "DEC" ParserGen.I_DEC;
    Hashtbl.add kw_map "ADD" ParserGen.I_ADD;
    Hashtbl.add kw_map "SUB" ParserGen.I_SUB;
    Hashtbl.add kw_map "MUL" ParserGen.I_MUL;
    Hashtbl.add kw_map "DIV" ParserGen.I_DIV;
    Hashtbl.add kw_map "AND" ParserGen.I_AND;
    Hashtbl.add kw_map "OR"  ParserGen.I_OR;
    Hashtbl.add kw_map "XOR" ParserGen.I_XOR;
    Hashtbl.add kw_map "JEQ" ParserGen.I_JEQ;
    Hashtbl.add kw_map "JLT" ParserGen.I_JLT;
    Hashtbl.add kw_map "JGT" ParserGen.I_JGT;
    Hashtbl.add kw_map "INT" ParserGen.I_INT;
    Hashtbl.add kw_map "HLT" ParserGen.I_HLT;
	()

let nat_regexp = Str.regexp "^[-]?[0-9]+$"

let tokenize_number str =
	if Str.string_match nat_regexp str 0 then
		ParserGen.NUM (int_of_string str)
	else
		raise (Invalid_number str)


let trimQuotes s = String.sub s 1 (String.length s - 2)

}

let vstart = ['a'-'z' 'A'-'Z' '_']
let digit  = ['0'-'9']
let vchar  = vstart | digit

rule token = parse
	  [ ' ' '\t' '\r' ] { token lexbuf }
	| '\n'   { Lexing.new_line lexbuf; token lexbuf }
	| "//"   { line_comment lexbuf }
	| "(*"   { block_comment 1 lexbuf }
	| (vstart vchar*) as x {
			try Hashtbl.find kw_map x with
			| Not_found -> ParserGen.ID x
		}
    | '"' ([^ '"' ]*) '"' as x {
             ParserGen.PATH (trimQuotes x)
        }
	| ('-'? digit vchar*) as x { tokenize_number x }
	| '('    { ParserGen.BR_OPN   }
	| ')'    { ParserGen.BR_CLS   }
	| '{'    { ParserGen.CBR_OPN  }
	| '}'    { ParserGen.CBR_CLS  }
	| '['    { ParserGen.SBR_OPN  }
	| ']'    { ParserGen.SBR_CLS  }
	| "&&"   { ParserGen.LOG_AND  }
	| ":="   { ParserGen.ASSIGN   }
	| '*'    { ParserGen.ASTERISK }
	| '='    { ParserGen.EQ       }
	| '<'    { ParserGen.LT       }
	| "<="   { ParserGen.LE       }
	| '>'    { ParserGen.GT       }
	| ">="   { ParserGen.GE       }
	| "<>"   { ParserGen.NEQ      }
	| '-'    { ParserGen.MINUS    }
	| "||"   { ParserGen.LOG_OR   }
	| '+'    { ParserGen.PLUS     }
	| ';'    { ParserGen.SEMI     }
	| '/'    { ParserGen.SLASH    }
    | '^'    { ParserGen.XOR      }
    | '&'    { ParserGen.AND      }
    | '|'    { ParserGen.OR       }
    | '!'    { ParserGen.LOG_NOT  }
    | ':'    { ParserGen.COLON    }
    | ','    { ParserGen.COMMA    }
	| eof    { ParserGen.EOF      }
	| _ as c { raise (Invalid_character c) }

and line_comment = parse
	  '\n' { Lexing.new_line lexbuf; token lexbuf }
	| eof  { ParserGen.EOF }
	| _    { line_comment lexbuf }

and block_comment depth = parse
	  "(*" { block_comment (depth+1) lexbuf }
	| "*)" {
		if depth = 1 then token lexbuf 
		else block_comment (depth-1) lexbuf }
	| '\n' { Lexing.new_line lexbuf; block_comment depth lexbuf }
	| eof  {
			Errors.error_p lexbuf.Lexing.lex_curr_p
				"End of file inside block comment.";
			ParserGen.EOF
		}
	| _ { block_comment depth lexbuf }
