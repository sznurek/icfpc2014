
type label = string

type param =
| Const  of int
| Mem    of int
| Reg    of string
| RegMem of string
| Label  of string

type instr =
| MOV of param * param
| INC of param
| DEC of param
| ADD of param * param
| SUB of param * param
| MUL of param * param
| DIV of param * param
| AND of param * param
| OR  of param * param
| XOR of param * param
| JLT of label * param * param
| JEQ of label * param * param
| JGT of label * param * param
| INT of int
| HLT

type code_elem =
| CE_Instr of instr * string option
| CE_Label of label
| CE_Info  of string

type t

val create : unit -> t
val print  : t -> unit

val add_instr : t -> ?comment:string -> instr -> unit
val add_label : t -> label -> unit
val add_info  : t -> string -> unit

val to_list : t -> code_elem list
val of_list : code_elem list -> t
