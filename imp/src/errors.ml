(* File: errors.ml *)

exception Fatal_error

let string_of_pos pos =
	Printf.sprintf "file %s, line %d, column %d"
		pos.Lexing.pos_fname
		pos.Lexing.pos_lnum
		(pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

let error_counter = ref 0

let error_p pos msg =
	error_counter := !error_counter + 1;
	Printf.eprintf ("%s:\nError: " ^^ msg ^^ "\n\n") (string_of_pos pos)

let warning_p pos msg =
	Printf.eprintf ("%s:\nWarning: " ^^ msg ^^ "\n\n") (string_of_pos pos)

let note_p pos msg =
	Printf.eprintf ("%s:\nNote: " ^^ msg ^^ "\n\n") (string_of_pos pos)

let error msg =
	error_counter := !error_counter + 1;
	Printf.eprintf ("Error: " ^^ msg ^^ "\n\n")

let fatal_error msg =
  Printf.kfprintf (fun _ -> raise Fatal_error) stderr ("Fatal error: " ^^ msg ^^ "\n\n")

let fatal_error_p pos msg =
  Printf.kfprintf (fun _ -> raise Fatal_error) stderr ("%s: \nFatal error: " ^^ msg ^^ "\n\n") (string_of_pos pos)

let note msg =
	Printf.eprintf ("Note: " ^^ msg ^^ "\n\n")

let not_implemented name =
	error "`%s' is not implemented!" name;
	raise Fatal_error

let ok () = !error_counter = 0
