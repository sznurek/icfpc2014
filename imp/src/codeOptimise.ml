
let find_used_labels_in_param used_labels param =
    match param with
    | Code.Label l ->
        Hashtbl.add used_labels l ()
    | _ -> ()

let find_used_labels used_labels instr =
    match instr with
    | Code.INC p | Code.DEC p ->
        find_used_labels_in_param used_labels p
    | Code.MOV(p1, p2) | Code.ADD(p1, p2) | Code.SUB(p1, p2) | Code.MUL(p1, p2)
    | Code.DIV(p1, p2) | Code.AND(p1, p2) | Code.OR(p1, p2)  | Code.XOR(p1, p2) ->
        find_used_labels_in_param used_labels p1;
        find_used_labels_in_param used_labels p2
    | Code.JEQ(l, p1, p2) | Code.JGT(l, p1, p2) | Code.JLT(l, p1, p2) ->
        Hashtbl.add used_labels l ();
        find_used_labels_in_param used_labels p1;
        find_used_labels_in_param used_labels p2
    | Code.INT _ | Code.HLT -> ()

let rec label_is_here l code =
    match code with
    | Code.CE_Label l' :: code ->
        l = l' || label_is_here l code
    | Code.CE_Info msg :: code -> label_is_here l code
    | _ -> false

let rec remove_dummy_instr code =
    match code with
    | [] -> []
    | instr :: code ->
        begin match instr with
        | Code.CE_Instr(Code.MOV(p1, p2), _) when p1 = p2 ->
            remove_dummy_instr code
        | Code.CE_Instr(Code.MOV(Code.Reg "PC", Code.Label l), _) when label_is_here l code ->
            remove_dummy_instr code
        | Code.CE_Instr(
            ( Code.MOV(Code.Reg "PC", _) | Code.HLT
            ), _) ->
            instr :: remove_dead_code code
        | _ -> instr :: remove_dummy_instr code
        end
and remove_dead_code code =
    match code with
    | [] -> []
    | Code.CE_Instr _  :: code -> remove_dead_code code
    | Code.CE_Info msg :: code -> Code.CE_Info msg :: remove_dead_code code
    | _ -> remove_dummy_instr code

let remove_unused_labels code =
    let used_labels = Hashtbl.create 32 in
    List.iter (fun e ->
        match e with 
        | Code.CE_Instr(i, _) -> find_used_labels used_labels i
        | _ -> ()
    ) code;
    List.filter (fun e ->
        match e with
        | Code.CE_Label l -> Hashtbl.mem used_labels l
        | _ -> true
    ) code

let rec loop_optimize old_code =
    let new_code = remove_dummy_instr old_code in
    let new_code = remove_unused_labels new_code in
    if old_code = new_code then new_code
    else loop_optimize new_code

let optimise code =
    let code = Code.to_list code in
    let code = loop_optimize code in
    Code.of_list code
