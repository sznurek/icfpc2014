open Ast

type var_box =
  { varbox_name : string
  ; varbox_size : int
  }

type visit_status = Visiting | Visited

let mk_stmt o n = {o with s_kind = n }
let mk_expr o n = {o with e_kind = n }
let mk_cond o n = {o with c_kind = n }
let mk_lvalue o n = {o with lv_kind = n }
let mk_asm_param o n = {o with ap_kind = n }
let mk_instr o n = { o with i_kind = n }

let extract_var s =
  match s.s_kind with
  | SVar name        -> Some { varbox_name = name; varbox_size = 1 }
  | SArray (name, n) -> Some { varbox_name = name; varbox_size = n }
  | _                -> None

let rec only_values = function
  | [] -> []
  | None   :: xs -> only_values xs
  | Some x :: xs -> x :: only_values xs

let total_size vs     = List.fold_left (fun s vb -> s + vb.varbox_size) 0 vs

let scope_vars ss =
  only_values (List.map extract_var ss)

let maximum_address vs = List.fold_left max 0 (List.map snd vs)

let strip_scope = function
  | { s_kind = SScope (ss, id) } -> (ss, id)
  | _                            -> failwith "strip scope failed"

let rec find_proc procs name =
  match procs with
  | []      -> Errors.fatal_error "No proc named %s" name
  | p :: ps ->
      if name = p.proc_name
        then p
        else find_proc ps name

let solve_bounds program =
  let visited = Hashtbl.create 256 in
  let bounds = Hashtbl.create 256 in
  let rec map_scopes f = function
    | [] -> []
    | { s_kind = SScope (ss, id)   } :: xs -> f (ss, id) :: map_scopes f xs
    | { s_kind = SIf(cond, s1, s2) } :: xs -> map_scopes f [ s1 ; s2 ] @ map_scopes f xs
    | { s_kind = SWhile(cond, s1)  } :: xs -> map_scopes f [ s1 ] @ map_scopes f xs
    | { s_kind = SCall(name, _)    } :: xs -> map_scopes f [ (find_proc program.prog_procs name).proc_body ] @ map_scopes f xs
    | x :: xs                              -> map_scopes f xs
  in let rec solve (ss, id) =
    if not (Hashtbl.mem visited id) then (
      Hashtbl.add visited id Visiting;
      let start = maximum_address (map_scopes solve ss) in
      let stop  = start + total_size (scope_vars ss) in
      Hashtbl.add visited id Visited;
      Hashtbl.add bounds id (start, stop);
      (start, stop)
    ) else if Hashtbl.find visited id = Visiting then (
      Errors.fatal_error "Cycle in scopes."
    ) else
      Hashtbl.find bounds id
  in
    List.iter (fun proc -> ignore (solve (strip_scope proc.proc_body))) program.prog_procs;
    ignore (solve (strip_scope program.prog_body));
    bounds

module Env = Map.Make (String)

let map_asm_param env param =
    mk_asm_param param
    begin match param.ap_kind with
    | AP_Const  c -> AP_Const  c
    | AP_Label  l -> AP_Label  l
    | AP_Reg    r -> AP_Reg    r
    | AP_RegMem r -> AP_RegMem r
    | AP_VarAddr(v, off) ->
        begin try AP_Const(Env.find v env + off) with
        | Not_found ->
            Errors.fatal_error_p param.Ast.ap_pos
              "Variable `%s' is not defined" v
        end
    | AP_VarVal(v, off) ->
        begin try AP_Mem(Env.find v env + off) with
        | Not_found ->
            Errors.fatal_error_p param.Ast.ap_pos
              "Variable `%s' is not defined" v
        end
    | AP_Mem   m -> AP_Mem m
    end

let map_instr env instr =
    mk_instr instr
    begin match instr.i_kind with
    | I_MOV(p1, p2) -> I_MOV(map_asm_param env p1, map_asm_param env p2)
    | I_INC p       -> I_INC(map_asm_param env p)
    | I_DEC p       -> I_DEC(map_asm_param env p)
    | I_ADD(p1, p2) -> I_ADD(map_asm_param env p1, map_asm_param env p2)
    | I_SUB(p1, p2) -> I_SUB(map_asm_param env p1, map_asm_param env p2)
    | I_MUL(p1, p2) -> I_MUL(map_asm_param env p1, map_asm_param env p2)
    | I_DIV(p1, p2) -> I_DIV(map_asm_param env p1, map_asm_param env p2)
    | I_AND(p1, p2) -> I_AND(map_asm_param env p1, map_asm_param env p2)
    | I_OR (p1, p2) -> I_OR (map_asm_param env p1, map_asm_param env p2)
    | I_XOR(p1, p2) -> I_XOR(map_asm_param env p1, map_asm_param env p2)
    | I_JEQ(l, p1, p2) -> I_JEQ(l, map_asm_param env p1, map_asm_param env p2)
    | I_JLT(l, p1, p2) -> I_JLT(l, map_asm_param env p1, map_asm_param env p2)
    | I_JGT(l, p1, p2) -> I_JGT(l, map_asm_param env p1, map_asm_param env p2)
    | I_INT n -> I_INT n
    | I_HLT   -> I_HLT
    end

let rec map_expr env expr =
    mk_expr expr
    begin match expr.e_kind with
    | Ast.EConst _   -> expr.e_kind
    | Ast.ELValue lv -> Ast.ELValue (map_lvalue env lv)
    | Ast.EOp(op, e1, e2) -> Ast.EOp(op, map_expr env e1, map_expr env e2)
    end
and map_lvalue env lvalue =
    mk_lvalue lvalue
    begin match lvalue.lv_kind with
    | Ast.LV_Var x ->
        begin try Ast.LV_Mem { e_pos = lvalue.lv_pos; e_kind = EConst(Env.find x env)} with
        | Not_found ->
            Errors.fatal_error_p lvalue.Ast.lv_pos
              "Variable `%s' is not defined" x
        end
    | Ast.LV_Array (x, n) -> 
        begin try 
          Ast.LV_Mem { e_pos = lvalue.lv_pos; e_kind = EOp(OpAdd,
            { e_pos = lvalue.lv_pos; e_kind = EConst(Env.find x env) },
            map_expr env n) }
        with
        | Not_found ->
            Errors.fatal_error_p lvalue.Ast.lv_pos
              "Variable `%s' is not defined" x
        end
    | _ -> lvalue.lv_kind
    end

let rec map_cond env cond =
    mk_cond cond
    begin match cond.c_kind with
    | Ast.CEq(e1, e2)  -> Ast.CEq(map_expr env e1, map_expr env e2)
    | Ast.CLt(e1, e2)  -> Ast.CLt(map_expr env e1, map_expr env e2)
    | Ast.CGt(e1, e2)  -> Ast.CGt(map_expr env e1, map_expr env e2)
    | Ast.CAnd(c1, c2) -> Ast.CAnd(map_cond env c1, map_cond env c2)
    | Ast.COr(c1, c2)  -> Ast.COr (map_cond env c1, map_cond env c2)
    | Ast.CNot c1      -> Ast.CNot(map_cond env c1)
    end

let rec extend_env env svs start =
    match svs with
    | [] -> env
    | sv :: svs ->
          extend_env (Env.add sv.varbox_name start env) svs (start + sv.varbox_size)

let rec map_stmt env bounds s = 
  mk_stmt s 
  begin match s.s_kind with
  | SInt _ | SCall _ | SRet | SHalt | SVar _ | SArray _ | SLabel _ | SGoto _ -> s.s_kind
  | SScope  (ss, id)       -> SScope (solve_scope_using_bounds env bounds (ss, id), id)
  | SAssign (lv, expr)     -> SAssign (map_lvalue env lv, map_expr env expr)
  | SIf     (cond, s1, s2) -> SIf (map_cond env cond, map_stmt env bounds s1, map_stmt env bounds s2)
  | SWhile  (cond, stmt)   -> SWhile (map_cond env cond, map_stmt env bounds stmt)
  | SAsm    instr_list     -> SAsm(List.map (map_instr env) instr_list)
  end
and solve_scope_using_bounds env bounds (ss, id) =
  try
    let env' = extend_env env (scope_vars ss) (fst (Hashtbl.find bounds id)) in
    List.map (map_stmt env' bounds) ss
  with
  | Not_found ->
    failwith (Printf.sprintf "Scope %d not solved!" id)

let map_proc env bounds p =
  let s = map_stmt env bounds p.proc_body in
  { p with proc_body = s}

let map_program env bounds program =
  let stmt = map_stmt env bounds program.prog_body in
  let (ss, id) = strip_scope stmt in
  let env' = extend_env env (scope_vars ss) (fst (Hashtbl.find bounds id)) in
    { prog_procs = List.map (map_proc env' bounds) program.prog_procs
    ; prog_body  = stmt
    }

let first_free_mem_cell bounds =
  Hashtbl.fold (fun _ (_, stop) m -> max stop m) bounds 0

let solve_scopes program =
  let bounds = solve_bounds program in
  let env = Env.empty in
  (map_program env bounds program, first_free_mem_cell bounds)

