(* File: ast.mli *)

type reg = string

type asm_param =
    { ap_pos  : Lexing.position
    ; ap_kind : asm_param_kind
    }
and asm_param_kind =
| AP_Const   of int
| AP_Label   of string
| AP_Reg     of reg
| AP_RegMem  of reg
| AP_VarAddr of string * int
| AP_VarVal  of string * int
| AP_Mem     of int

type asm_instr =
    { i_pos  : Lexing.position
    ; i_kind : asm_inst_kind
    }
and asm_inst_kind =
| I_MOV of asm_param * asm_param 
| I_INC of asm_param
| I_DEC of asm_param
| I_ADD of asm_param * asm_param
| I_SUB of asm_param * asm_param
| I_MUL of asm_param * asm_param
| I_DIV of asm_param * asm_param
| I_AND of asm_param * asm_param
| I_OR  of asm_param * asm_param
| I_XOR of asm_param * asm_param
| I_JEQ of string * asm_param * asm_param
| I_JLT of string * asm_param * asm_param
| I_JGT of string * asm_param * asm_param
| I_INT of int
| I_HLT

type op =
| OpAdd
| OpSub
| OpMul
| OpDiv
| OpAnd
| OpOr
| OpXor

type lvalue =
    { lv_pos  : Lexing.position
    ; lv_kind : lvalue_kind
    }
and lvalue_kind =
| LV_Var   of string
| LV_Array of string * expr
| LV_Reg   of reg
| LV_Mem   of expr

and expr =
    { e_pos  : Lexing.position
    ; e_kind : expr_kind
    }
and expr_kind =
| EConst  of int
| ELValue of lvalue
| EOp     of op * expr * expr

type cond =
    { c_pos  : Lexing.position
    ; c_kind : cond_kind
    }
and cond_kind =
| CEq  of expr * expr
| CLt  of expr * expr
| CGt  of expr * expr
| CAnd of cond * cond
| COr  of cond * cond
| CNot of cond

type stmt =
    { s_pos  : Lexing.position
    ; s_kind : stmt_kind
    }
and stmt_kind =
| SScope  of stmt list * int
| SInt    of int
| SAssign of lvalue * expr
| SIf     of cond * stmt * stmt
| SWhile  of cond * stmt
| SVar    of string
| SArray  of string * int
| SCall   of string * bool
| SRet
| SHalt
| SGoto   of string
| SLabel  of string
| SAsm    of asm_instr list

type proc =
    { proc_pos  : Lexing.position
    ; proc_name : string
    ; proc_body : stmt
    }

type program =
    { prog_procs : proc list
    ; prog_body  : stmt
    }
