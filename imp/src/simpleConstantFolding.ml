
open Ast

let mk_stmt o n = {o with s_kind = n }
let mk_expr o n = {o with e_kind = n }
let mk_cond o n = {o with c_kind = n }
let mk_lvalue o n = {o with lv_kind = n }
let mk_asm_param o n = {o with ap_kind = n }
let mk_instr o n = { o with i_kind = n }

let perform_op op c1 c2 = (op (c1 land 255) (c2 land 255)) land 255

let optimize_op op e1 e2 =
    match op, e1.e_kind, e2.e_kind with
    | OpAdd, EConst c1,  EConst c2  -> EConst(perform_op ( + ) c1 c2)
    | OpAdd, e,          EConst 0   -> e
    | OpAdd, EConst 0,   e          -> e
    | OpSub, EConst c1,  EConst c2  -> EConst(perform_op ( - ) c1 c2)
    | OpSub, e,          EConst 0   -> e
    | OpMul, EConst c1,  EConst c2  -> EConst(perform_op ( * ) c1 c2)
    | OpMul, _,          EConst 0   -> EConst 0
    | OpMul, e,          EConst 1   -> e
    | OpMul, EConst 0,   _          -> EConst 0
    | OpMul, EConst 1,   e          -> e
    | OpDiv, _,          EConst 0   -> Errors.fatal_error_p e2.e_pos "Division by zero"
    | OpDiv, EConst c1,  EConst c2  -> EConst(perform_op ( / ) c1 c2)
    | OpDiv, e,          EConst 1   -> e
    | OpAnd, EConst c1,  EConst c2  -> EConst(perform_op ( land ) c1 c2)
    | OpAnd, _,          EConst 0   -> EConst 0
    | OpAnd, e,          EConst 255 -> e
    | OpAnd, EConst 0,   _          -> EConst 0
    | OpAnd, EConst 255, e          -> e
    | OpOr,  EConst c1,  EConst c2  -> EConst(perform_op ( lor ) c1 c2)
    | OpOr,  e,          EConst 0   -> e
    | OpOr,  _,          EConst 255 -> EConst(255)
    | OpOr,  EConst 0,   e          -> e
    | OpOr,  EConst 255, _          -> EConst(255)
    | OpXor, EConst c1,  EConst c2  -> EConst(perform_op ( lxor ) c1 c2)
    | OpXor, e,          EConst 0   -> e
    | OpXor, EConst 0,   e          -> e
    | _ -> EOp(op, e1, e2)

let rec optimize_lvalue lvalue =
    mk_lvalue lvalue
    begin match lvalue.lv_kind with
    | LV_Var _ | LV_Array _ | LV_Reg _ -> lvalue.lv_kind
    | LV_Mem addr -> LV_Mem(optimize_expr addr)
    end
and optimize_expr expr =
    mk_expr expr
    begin match expr.e_kind with
    | EConst _ -> expr.e_kind
    | ELValue lv -> ELValue (optimize_lvalue lv)
    | EOp(op, e1, e2) ->
        let e1 = optimize_expr e1 in
        let e2 = optimize_expr e2 in
        optimize_op op e1 e2
    end

let rec optimize_cond cond =
    mk_cond cond
    begin match cond.c_kind with
    | CEq(e1, e2)  -> CEq(optimize_expr e1, optimize_expr e2)
    | CLt(e1, e2)  -> CLt(optimize_expr e1, optimize_expr e2)
    | CGt(e1, e2)  -> CGt(optimize_expr e1, optimize_expr e2)
    | CAnd(c1, c2) -> CAnd(optimize_cond c1, optimize_cond c2)
    | COr(c1, c2)  -> COr (optimize_cond c1, optimize_cond c2)
    | CNot c       -> CNot(optimize_cond c)
    end

let rec optimize_stmt stmt =
    mk_stmt stmt 
    begin match stmt.s_kind with
    | SInt _ | SVar _ | SArray _ | SCall _ | SRet | SHalt | SGoto _ | SLabel _ | SAsm _ -> stmt.s_kind
    | SScope(ss, id)    -> SScope(List.map optimize_stmt ss, id)
    | SAssign(lv, e)    -> SAssign(optimize_lvalue lv, optimize_expr e)
    | SIf(cond, s1, s2) -> SIf(optimize_cond cond, optimize_stmt s1, optimize_stmt s2)
    | SWhile(cond, s)   -> SWhile(optimize_cond cond, optimize_stmt s)
    end

let optimize_proc proc =
    { proc with Ast.proc_body = optimize_stmt proc.Ast.proc_body 
    }

let optimize prog =
    { Ast.prog_procs = List.map optimize_proc prog.Ast.prog_procs
    ; Ast.prog_body  = optimize_stmt prog.Ast.prog_body
    }
