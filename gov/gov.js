#!/usr/bin/env node

var program = require("commander");
var fs = require("fs");
var spawn = require("child_process").spawn;
var path = require("path");
var _ = require("underscore");

program
	.version("1.2.3")
	.option("-s, --state <state_file>", "file with description of initial world state")
	.option("-p, --lambdaman_code <ghc_file>", "file with the lambda man code")
	.option("-e, --ghost_codes <gcc_file,gcc_file,...>", "files with the ghosts ai code")
	.option("-l, --lambdaman_machine <executable>", "executable file responsible for execution of lambdaman ai code")
	.option("-g, --ghost_machine <executable>", "executable file responsible for execution of ghosts ai code")
	.option("-o, --offline", "offline mode")
	.option("-d, --draw", "draw mode")
	.option("-t, --throttle <ticks>", "slowdown parameter")
	.option("-m, --map", "get map in theris format")
	.parse(process.argv);

function Player() {
	this.maxCycles = this.cycles = 0;
}

Player.prototype.changeDirection = function(newDir, state) {
	if (newDir < 4) {
		this.direction = newDir;
	} else {
		this.direction = 0;
	}
};

Player.prototype.move = function(state) {
	state.movePlayer(this);
};

Player.prototype.returnToStart = Ghost.prototype.returnToStart = function() {
	this.coords = this.startingLocation.slice();
	this.direction = this.startingDirection;
	this.vit = 0;
};

function Ghost() {
	this.maxCycles = this.cycles = 0;
}

Ghost.prototype.changeDirection = function(newDir, state) {
	if (newDir < 4) {
		this.direction = newDir;
	} else {
		this.direction = 0;
	}
};

Ghost.prototype.move = function(state) {
	state.moveGhost(this);
};

function State(isOffline, isDrawMode, throttle) {
	this.isOffline = isOffline;
	this.isDrawMode = isDrawMode;
	this.throttle = throttle;
	this.tick = 1;
	this.frightCountdown = 0;
	this.events = [ [ 127 * 200, "fruit_on" ], [ 127 * 280, "fruit_off"], [ 127 * 400, "fruit_on" ], [ 127 * 480, "fruit_off" ] ];

	this.scores = {
		pill: 10,
		powerPill: 50,
		fruit: [ 0, 100, 300, 500, 500, 700, 700, 1000, 1000, 2000, 2000, 3000, 3000, 5000 ],
		ghost: [ 200, 400, 800, 1600 ]
	};

	this.tileCodes = {
		wall: 0,
		free: 1,
		pill: 2,
		powerPill: 3,
		fruit: 4,
		start: 5,
		ghost: 6
	};

	this.directions = {
		up: 0,
		right: 1,
		down: 2,
		left: 3
	};

	this.oppositeDirections = [ this.directions.down, this.directions.left, this.directions.up, this.directions.right ];

	this.ghostVitality = {
		standard: 0,
		fright: 1,
		invisible: 2
	};

	this.durations = {
		frightDuration: 127 * 20,

		playerMove: 127,
		playerEatingPenalty: 10,

		ghostMove: [ 130, 132, 134, 136 ],
		ghostFrightMove: [ 195, 198, 201, 204 ],

		fruitCountdown: 80 * 127
	};
}

State.prototype.parse = function(s) {
	var t = (function() {
		var tokens = s.split(/[\n\t\r ]+/);
		var p = 0;
		return { 
			s: function() {
				return tokens[p++];
			},
			i: function() {
				return parseInt(tokens[p++]);
			}
		};
	})();

	this.width = parseInt(t.i());
	this.height = parseInt(t.i());
	this.tiles = [];
	this.numPills = 0;
	for (var j = 0; j < this.height; ++j) {
		var row = t.s();
		for (var i = 0; i < this.width; ++i) {
			this.tiles[i + j * this.width] = parseInt(row.charAt(i));
			if (this.tiles[i + j * this.width] == this.tileCodes.pill) {
				this.numPills++;
			}
		}
	}
	this.mapLevel = Math.min(this.scores.fruit.length - 1, ~~((this.width * this.height + 99) / 100));

	this.players = [];
	var numLambdaMans = 1; // t();
	for (var i = 0; i < numLambdaMans; ++i) {
		var player = new Player();
		player.vit = t.i();
		player.coords = [ t.i(), t.i() ];
		player.direction = t.i();
		player.lives = t.i();
		player.score = t.i();
		
		player.nextMoveTick = this.durations.playerMove;
		player.ghostMultiplier = 0;
		player.startingLocation = player.coords.slice();
		player.startingDirection = player.direction;
		this.players.push(player);
	}

	this.ghosts = [];
	var numGhosts = t.i();
	for (var i = 0; i < numGhosts; ++i) {
		var ghost = new Ghost();
		ghost.vit = t.i();
		ghost.coords = [ t.i(), t.i() ];
		ghost.direction = t.i();

		this.ghosts.push(ghost);
	}

	// ghosts are sorter by coordinates
	this.ghosts.sort(function(a, b) { return (a.y - b.y) * 256 + a.x - b.x; });
	_.each(this.ghosts, function(ghost, i) {
		ghost.moveDuration = this.durations.ghostMove[i % 4];
		ghost.frightPenalty = this.durations.ghostFrightMove[i % 4] - this.durations.ghostMove[i % 4];
		ghost.nextMoveTick = ghost.moveDuration;
		ghost.startingDirection = ghost.direction;
		ghost.startingLocation = ghost.coords.slice();
		ghost.previousDirection = ghost.direction;
	}, this);

	this.fruitCountdown = t.i();

	this.lastTick = 127 * this.width * this.height * 16;
};

State.prototype.stringify = function() {
	var o = "";
	o += this.width + " " + this.height + "\n";
	for (var j = 0; j < this.height; ++j) {
		for (var i = 0; i < this.width; ++i) {
			o += this.tiles[i + j * this.width];
		}
		o += "\n";
	}

	_.each(this.players, function(player) {
		o += [player.vit, player.coords[0], player.coords[1], player.direction, player.lives, player.score].join(" ") + "\n";
	}, this);

	o += this.ghosts.length + "\n";
	_.each(this.ghosts, function(ghost) {
		o += [ghost.vit, ghost.coords[0], ghost.coords[1], ghost.direction].join(" ") + "\n";
	}, this);

	o += this.fruitCountdown + "\n";
	return o;
};

State.prototype.printMap = function() {
	this.draw(true);
};

State.prototype.draw = function(drawFruitAnyway) {
	process.stdout.write("\u001B[2J\u001B[0;0f");
	var o = "";
	for (var j = 0; j < this.height; ++j) {
		for (var i = 0; i < this.width; ++i) {
			var ti = i + j * this.width;
			if (_.find(this.players, function(p) { return p.coords[0] == i  && p.coords[1] == j; })) {
				if (this.frightCountdown > 0) {
					o += "&";
				} else {
					o += "\\";
				}
			} else if (_.find(this.ghosts, function(p) { return p.coords[0] == i  && p.coords[1] == j; })) {
				o += "=";
			} else if (this.tiles[ti] == this.tileCodes.free) {
				o += " ";
			} else if (this.tiles[ti] == this.tileCodes.wall) {
				o += "#";
			} else if (this.tiles[ti] == this.tileCodes.pill) {
				o += ".";
			} else if (this.tiles[ti] == this.tileCodes.powerPill) {
				o += "o";
			} else if (this.tiles[ti] == this.tileCodes.fruit && (this.fruitCountdown > 0 || drawFruitAnyway)) {
				o += "%"
			} else {
				o += " ";
			}
		}
		o += "\n";
	}
	process.stdout.write(o);
}

State.prototype.initMachines = function(lmc, ghc, playerCode, ghostCodes, callback) {
	if (!this.isOffline) {
		_.each(this.players, function(player, i) {
			player.machine = new Machine(lmc, [ playerCode ]);
		}, this);

		_.each(this.ghosts, function(ghost, i) {
			ghost.machine = new Machine(ghc, [ ghostCodes[i % ghostCodes.length], i]);
		}, this);

		this.multiRequest(this.players.concat(this.ghosts),
			"INIT " + this.stringify(),
			function(entity, params) {
				if (params[0] == "DONE") {
					var cycles = parseInt(params[1]);
					var mem = parseInt(params[2]);
				} else {
					console.log("Protocol error: " + params[0] + " : " + params);
					process.exit(1);
				}
			},
			callback
		);
	} else {
		callback();
	}
};

State.prototype.start = function() {
	this.isRunning = true;
	this.preTick();
};

State.prototype.getNextTile = function(coords, direction) {
	var i = this.getTileIndex(coords);
	if (direction == this.directions.up) {
		return i - this.width; 
	} else if (direction == this.directions.right) { 
		return i + 1; 
	} else if (direction == this.directions.down) { 
		return i + this.width;
	} else { // left
		return i - 1;
	}
};

State.prototype.getTileCoords = function(t) {
	return [ t % this.width, ~~(t / this.width) ];
};

State.prototype.getTileIndex = function(coords) {
	return coords[0] + coords[1] * this.width;
};

State.prototype.movePlayer = function(e) {
	var t = this.getNextTile(e.coords, e.direction);
	if (this.tiles[t] != this.tileCodes.wall) {
		var olcords = e.coords;
		e.coords = this.getTileCoords(t);
	}
};

State.prototype.moveGhost = function(e) {
	var ways = _.map([0, 1, 2, 3], function(d) { return [d, this.getNextTile(e.coords, d) ] }, this);
	var passableWays = _.filter(ways, function(w) { return this.tiles[w[1]] != this.tileCodes.wall; }, this);

	if (passableWays.length == 1) { // blind
		e.direction = passableWays[0][0];
		e.previousDirection = e.direction;
		e.coords = this.getTileCoords(passableWays[0][1]);
	} else if (passableWays.length >= 2) {
		if (e.direction != this.oppositeDirections[e.previousDirection]
			&& _.find(passableWays, function(w) { return w[0] == e.direction; })) { // valid move
			var t = this.getNextTile(e.coords, e.direction);
			e.previousDirection = e.direction;
			e.coords = this.getTileCoords(t);
		} else {
			var t = this.getNextTile(e.coords, e.previousDirection);
			if (this.tiles[t] != this.tileCodes.wall) { // try move in previous
				e.direction = e.previousDirection;
				e.coords = this.getTileCoords(t);
			} else {
				for (var i = 0; i < passableWays.length; ++i) {
					if (passableWays[i][0] != this.oppositeDirections[e.previousDirection]) {
						e.direction = passableWays[i][0];
						e.previousDirection = e.direction;
						e.coords = this.getTileCoords(passableWays[i][1]);
						break;
					}
				}
			}
		}
	}
};

State.prototype.preTick = function() {
	if (this.fruitCountdown > 0) {
		this.fruitCountdown--;
	}
	if (this.frightCountdown > 0) {
		this.frightCountdown--;
		if (this.frightCountdown == 0) {
			_.each(this.ghosts, function(ghost) { ghost.vit = this.ghostVitality.standard; }, this);
		}
	}

	this.movingEntities = [];

	// (1) players move
	_.each(this.players, function(player) {
		if (player.vit > 0) {
			player.vit--;
		}
		if (player.nextMoveTick == this.tick) {
			player.nextMoveTick = this.tick + this.durations.playerMove;
			this.movingEntities.push(player);
		}
	}, this);

	// (1) ghosts move
	_.each(this.ghosts, function(ghost) {
		if (ghost.nextMoveTick == this.tick) {
			ghost.nextMoveTick = this.tick + ghost.moveDuration;
			if (ghost.vit == this.ghostVitality.fright) {
				ghost.nextMoveTick += ghost.frightPenalty;
			}
			this.movingEntities.push(ghost);
		}
	}, this);

	if (!this.isOffline) {
		if (this.movingEntities.length > 0) {
			var serialized = this.stringify();
			var state = this;
			this.multiRequest(this.movingEntities,
				"STEP " + serialized,
				function(entity, params) {
					if (params[0] == "DONE") {
						var cycles = parseInt(params[1]);
						var mem = parseInt(params[2]);
						entity.cycles += cycles;
						entity.maxCycles = Math.max(cycles, entity.maxCycles);
						entity.changeDirection(parseInt(params[3]), state);
						entity.move(state);
					} else {
						console.log("Protocol error: " + params[0] + " : " + params);
						process.exit(1);
					}
				},
				function() {
					state.postTick();
				}
			);
		} else {
			this.postTick(true);
		}
	} else {
		this.postTick();
	}
}

State.prototype.postTick = function(immediate) {

	// (2) any actions (fruit?)
	_.each(this.events, function(e) {
		if (e[0] == this.tick) {
			if (e[1] == "fruit_on") {
				this.fruitCountdown = this.durations.fruitCountdown;
			}
		}
	}, this);

	// (3) Pills, powerpills and fruit
	_.each(this.players, function(player) {
		var isEating = false;
		var ti = this.getTileIndex(player.coords);
		if (this.tiles[ti] == this.tileCodes.pill) {
			player.score += this.scores.pill;
			this.tiles[ti] = 1;
			this.numPills--;
			isEating = true;
		} else if (this.tiles[ti] == this.tileCodes.powerPill) {
			player.score += this.scores.powerPill;
			player.ghostMultiplier = 0;
			player.vit = this.durations.frightDuration;
			_.each(this.ghosts, function(ghost) { 
				ghost.vit = this.ghostVitality.fright;
				ghost.nextMoveTick += ghost.frightPenalty;
				ghost.direction = this.oppositeDirections[ghost.previousDirection];
				ghost.previousDirection = ghost.direction;
			}, this);
			this.frightCountdown = this.durations.frightDuration;
			this.tiles[ti] = 1;
			isEating = true;
		} else if (this.fruitCountdown > 0 && this.tiles[ti] == this.tileCodes.fruit) {
			player.score += this.scores.fruit[this.mapLevel];
			this.fruitCountdown = 0;
			isEating = true;
		}

		if (isEating) {
			player.nextMoveTick += this.durations.playerEatingPenalty;
		}
	}, this);

	// (4) killing
	playerLoop: for (var i = 0; i < this.players.length; ++i) {
		var pti = this.getTileIndex(this.players[i].coords);
		ghostLoop: for (var j = 0; j < this.ghosts.length; ++j) {
			var gti = this.getTileIndex(this.ghosts[j].coords);
			if (gti == pti) {
				if (this.players[i].vit > 0 && this.ghosts[j].vit == this.ghostVitality.fright) { // eat ghost
					this.players[i].score += this.scores.ghost[this.players[i].ghostMultiplier];
					this.players[i].ghostMultiplier = Math.min(this.players[i].ghostMultiplier + 1, this.scores.ghost.length - 1);
					this.ghosts[j].returnToStart();
					this.ghosts[j].vit = this.ghostVitality.invisible;
					continue ghostLoop;
				} else if (this.ghosts[j].vit != this.ghostVitality.invisible) { // eat player
					//TODO: should I reset their sheduled moves?
					this.players[i].returnToStart();
					this.players[i].lives--;
					_.each(this.ghosts, function(ghost) { ghost.returnToStart(); }, this);
					break playerLoop; //XXX
				}
			}
		}
	}

	// (5) win condition
	if (this.numPills == 0) {
		_.each(this.players, function(player) { player.score *= (player.lives+1); },this);
		this.isRunning = false;
	}

	// (6) lose condition
	var numPlayersAlive = _.filter(this.players, function(player) { return player.lives > 0; }).length;
	if (numPlayersAlive == 0) {
		this.isRunning = false;
	}

	this.tick++;
	if (this.tick == this.lastTick) {
		this.isRunning = false;
	} 

	if (this.isDrawMode && this.movingEntities.length > 0) {
		process.stdout.write("Tick: " + this.tick + "\n");
		this.draw();
		_.each(this.players.concat(this.ghosts), function(e) {
			if (e.machine.err == "") {
				process.stderr.write(e.machine.lasterr);
			} else {
				process.stderr.write(e.machine.err);
				e.machine.lasterr = e.machine.err;
				e.machine.err = "";
			}
		}, this);
	} else {
		_.each(this.players.concat(this.ghosts), function(e) {
			if (e.machine.err != "") {
				process.stderr.write(e.machine.err + "\n");
				e.machine.err = "";
			}
		});
	}


	if (this.isRunning) {
		if (immediate && !this.throttle) {
			this.preTick();
		} else {
			var state = this;
			setTimeout(function() {
				state.preTick()
			}, this.throttle);
		}
	} else {
		_.each(this.players, function(player, i) {
			console.log("Player " + i + " score: " + player.score);
		},this);
		_.each(this.players.concat(this.ghosts), function(e, i) {
			console.log("Entity " + e.machine.cmd + "(" + i + ") cycles: " + e.cycles + ", maxCycles: " + e.maxCycles);
		},this);
		this.multiRequest(this.players.concat(this.ghosts), "STOP",
			function(entity, params) {
				if (params[0] != "ROGER_THAT") {
					console.log("Child process doesn't obey.");
					process.exit(1);
				}
			},
			function() {
				process.exit(0);
			}
		);
	}
}

State.prototype.multiRequest = function(entities, request, callback, post) {
	var numRequests = entities.length;
	if (numRequests == 0) {
		post();
	} else {
		_.each(entities, function(entity) {
			entity.machine.request(
				request,
				function(data) {
					callback(entity, data.split(/[\n\t\r ]+/));
					numRequests--;
					if (numRequests == 0) {
						post();
					}
				}
			);
		});
	}
};

function Machine(cmd, params) {
	this.cmd = cmd;
	this.lasterr = "";
	this.err = "";
	this.child = spawn(cmd, params);
	this.child.stdout.setEncoding("ascii");
	this.child.stdin.setEncoding("ascii");

	var machine = this;
	this.child.stderr.on("data", function(d) {
		machine.err += d;
	});

	this.child.on("exit", function(code, signal) {
		console.log("Child process (" + cmd + ": " + params + ") closed with status " + code + "(sig: " + signal + ").");
	});
}

Machine.prototype.request = function(msg, callback) {
	this.child.stdout.once("data", callback);
	this.child.stdin.write(msg + "\n");
};

Machine.prototype.close = function() {
	this.child.kill();
};

if (program.state && program.lambdaman_code && program.ghost_codes && program.lambdaman_machine && program.ghost_machine) {
	var stateSrc = fs.readFileSync(program.state, "ascii");
	var state = new State(program.offline, program.draw, program.throttle ? program.throttle : 0);
	state.parse(stateSrc);

	if (program.map) {
		state.printMap();
	} else {
		state.initMachines(program.lambdaman_machine, program.ghost_machine, program.lambdaman_code, program.ghost_codes.split(","),
			function() { state.start(); }
		);
	}
} else {
	program.help();
}
