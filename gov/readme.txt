usage:
	governor <initial_state> <ghc_code> <lmc_code>

Phase 0
At start, governor spawns child processes:
	ghc <ghc_code> <ghost_number>
	lmc <lmc_code>

Phase 1
Just after spawn
	Req: 
		INIT state
	Resp:
		DONE cycles mem
		ERROR msg

Phase 2
Just after init of both ghc and lmc is DONE
	Req: 
		STEP state
	Resp:
		DONE cycles mem move
		ERROR msg

	Req: STOP
	Resp:
		ROGER_THAT




State:
	width height 
	row<1>
	row<2>
	...
	row<height-1>
	row<height>

	lambdaman_vit lambdaman_x lambdaman_y lambdaman_dir lambdaman_lives lambdaman_score

	num_ghosts
	ghost<1>_vit ghost<1>_x ghost<1>_y ghost<1>_dir
	...
	ghost<num_ghosts>_vit ghost<num_ghosts>_x ghost<num_ghosts>_y ghost<num_ghosts>_dir
	fruit_countdown







