// lambda bot - stinky
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>

struct Player {
	int vit;
	int x;
	int y;
	int dir;
	int lives;
	int score;

	void read() {
		std::cin >> vit >> x >> y >> dir >> lives >> score;
	}
};

struct Ghost {
	int vit;
	int x;
	int y;
	int dir;

	void read() {
		std::cin >> vit >> x >> y >> dir;
	}
};

struct State {
	int width;
	int height;
	int **map;
	Player player;
	int numGhosts;
	Ghost *ghosts;
	int fruitCounter;
};

State state;

char buf[1024];

void parseState() {
	int w;
	std::cin >> state.width >> state.height;
	if (state.map == NULL) {
		state.map = new int*[state.width];
		for (int i = 0; i < state.width; ++i) {
			state.map[i] = new int[state.height];
		}
	}
	for (int j = 0; j < state.height; ++j) {
		std::cin >> buf;
		for (int i = 0; i < state.width; ++i) {
			state.map[i][j] = buf[i] - '0';
		}
	}

	state.player.read();
	std::cin >> state.numGhosts;
	if (state.ghosts == NULL) {
		state.ghosts = new Ghost[state.numGhosts];
	}
	for (int i = 0; i < state.numGhosts; ++i) {
		state.ghosts[i].read();
	}
	std::cin >> state.fruitCounter;
}


int pf[255][255];
int walls[255][255];
int neighbours[4][2] = { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 } };
int nope = -10000000;
int temptation;
int traise = 10000;

int step() {
	Player * me = &state.player;

	temptation += traise;
	traise += 10;

	// block blind gaps
	for (int i = 1; i < state.width - 1; ++i) {
		for (int j = 1; j < state.height - 1; ++j ) {
			if (walls[i][j]) {
				state.map[i][j] = 0;
			}
		}
	}

	// check if blind gap
	int junction = 0;
	for (int i = 0; i < 4; ++i) {
		junction += state.map[me->x+neighbours[i][0]][me->y+neighbours[i][1]] ? 1 : 0;
	}
	if (junction == 1) {
		walls[me->x][me->y] = 1;
		pf[me->x][me->y] = 0;
	}

	// add tempation and vaporize spirit
	for (int i = 1; i < state.width - 1; ++i) {
		for (int j = 1; j < state.height - 1; ++j ) {
			if (walls[i][j]) {
				state.map[i][j] = 0;
			}
			if (state.map[i][j] == 2 || (state.map[i][j] == 4 && state.fruitCounter > 0)) {
				pf[i][j] = temptation;
			} else if (state.map[i][j]) {
				pf[i][j] += 2;
			}
		}
	}

	// put spirits
	for (int i = 0; i < state.numGhosts; ++i) {
		pf[state.ghosts[i].x][state.ghosts[i].y] = nope;
	}

	// first propagation
	for (int i = 1; i < state.width - 1; ++i) {
		for (int j = 1; j < state.height - 1; ++j ) {
			if (pf[i][j]==nope) {
				continue;
			}
			if (state.map[i][j] != 0) {
				int sum = 0;
				int c = 0;
				for (int k = 0; k < 4; ++k) {
					if (state.map[i-neighbours[k][0]][j-neighbours[k][1]] != 0) {
						c++;
						sum += pf[i-neighbours[k][0]][j-neighbours[k][1]];
					}
				}
				if (c != 0) {
					pf[i][j] = sum / c;
				}
			}
		}
	}

	// second propagation
	for (int i = state.width-2; i >= 1; --i) {
		for (int j = state.height-2; j >= 1; --j ) {
			if (pf[i][j]==nope) {
				continue;
			}
			if (state.map[i][j] != 0) {
				int sum = 0;
				int c = 0;
				for (int k = 0; k < 4; ++k) {
					if (state.map[i-neighbours[k][0]][j-neighbours[k][1]] != 0) {
						c++;
						sum += pf[i-neighbours[k][0]][j-neighbours[k][1]];
					}
				}
				if (c != 0) {
					pf[i][j] = sum / c;
				}
			}
		}
	}

	// decision
	int maxsmell = -1;
	int maxsmelldir = 0;
	for (int i = 0 ; i < 4; ++i) {
		if (state.map[me->x+neighbours[i][0]][me->y+neighbours[i][1]] != 0) {
			int s = pf[me->x+neighbours[i][0]][me->y+neighbours[i][1]];
			if (maxsmell == -1 || s > maxsmell) {
				maxsmell = s;
				maxsmelldir = i;
			}
		}
	}

	//debug
    /*
	for (int j = 0; j < state.height; ++j) {
		for (int i = 0; i < state.width; ++i) {
			std::cerr << (state.map[i][j] == 0 ? "#" : (pf[i][j] > 0 ? "." :  " ")) << "";
		}
			std::cerr<<"\n";
	}
    */

	return maxsmelldir;
}

int main(int argc, char **argv) {
	do {
		std::cin >> buf;
		if (strcmp(buf,"INIT")==0) {
			parseState();
			std::cout << "DONE 0 0\n";
		} else if (strcmp(buf,"STEP")==0) {
			parseState();
			int result = step();
			std::cout << "DONE 0 0 " << result << "\n";
		} else if (strcmp(buf,"STOP")==0) {
			std::cout << "ROGER_THAT\n"; 
			std::cout.flush();
			break;
		} else {
			std::cout << "ERROR wtf\n";
		}
		std::cout.flush();
	} while (true);
	return 0;
}

