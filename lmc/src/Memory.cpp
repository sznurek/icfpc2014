#include "Memory.hpp"
#include <stdexcept>

Memory::Memory() {
}

Memory::~Memory() {
}


const Value &Memory::getHeapCar(const Value &valueHeapRef)
{
    HeapRef hr = valueHeapRef.getHeapReference();
    const auto &cell = heap.reference(hr);
    return cell.getValue1();
}

const Value &Memory::getHeapCdr(const Value &valueHeapRef)
{
    HeapRef hr = valueHeapRef.getHeapReference();
    const auto &cell = heap.reference(hr);
    return cell.getValue2();
}


uint32_t Memory::getClosureCode(const Value &valueHeapRef)
{
    return getHeapCar(valueHeapRef).intValue();
}

const Frame &Memory::getClosureFrame(const Value &valueHeapRef)
{
    return getHeapCdr(valueHeapRef).asFrame();
}


Value Memory::allocateConsCell(const Value &value1, const Value &value2)
{
    return Value::createCons(heap.allocateConsCell(value1, value2));
}

Value Memory::allocateClosureCell(uint32_t code, const Frame &frame)
{
    return Value::createClosure(heap.allocateClosureCell(code, frame));
}

