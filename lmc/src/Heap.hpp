#pragma once

#include "DataTypes.hpp"
#include "HeapRef.hpp"
#include <vector>

class Heap
{
public:
    Heap();

    class Cell
    {

    public:
        Cell(const Value &value1, const Value &value2);
        Cell();

        Tag getTag() const;

        const Value &getValue1() const;
        const Value &getValue2() const;

    private:
        Tag     tag;

        Value   value1;

        Value   value2;
    };


    HeapRef allocateConsCell(const Value &value1, const Value &value2);
    HeapRef allocateClosureCell(uint32_t code, const Frame &env);

    size_t getCountOfActiveCells();

    const Cell &reference(HeapRef heapRef) const;
private:
    std::vector<Cell> container;

    HeapRef allocateCell();

    unsigned int firstFreeCell;
};
