#include "Game.hpp"

#include <iostream>

Map parseMap(std::istream& is) {
    int width, height;
    std::vector<std::vector<unsigned char> > fields;

    is >> width >> height;
    fields.resize(height);

    // HACK
    std::string row;

    for(int i = 0; i < height; i++) {
        fields[i].resize(width);

        is >> row;

        for(int j = 0; j < width; j++) {
            fields[i][j] = row[j] - '0';
        }
    }

    return Map(fields);
}

LambdaMan parseLambdaMan(std::istream& is) {
    int vit, x, y, dir, lives, score;

    is >> vit >> x >> y >> dir >> lives >> score;
    return LambdaMan(vit, Position(x, y), dir, lives, score);
}

GhostState parseGhostState(std::istream& is) {
    int vitality, x, y, direction;

    is >> vitality >> x >> y >> direction;
    return GhostState(Position(x, y), (unsigned char)vitality, (unsigned char)direction);
}

std::vector<GhostState> parseGhostStates(std::istream& is) {
    int n;
    std::vector<GhostState> result;

    is >> n;
    for(int i = 0; i < n; i++) {
        result.push_back(parseGhostState(is));
    }

    return result;
}

CurrentWorldState parseCurrentWorldState(std::istream& is) {
    Map map = parseMap(is);
    LambdaMan lambdaMan = parseLambdaMan(is);
    std::vector<GhostState> ghosts = parseGhostStates(is);
    int fruits;
    is >> fruits;

    return CurrentWorldState(lambdaMan, ghosts, map, fruits);
}

