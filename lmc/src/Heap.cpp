#include "Heap.hpp"
#include <stdexcept>

Heap::Heap()
    : firstFreeCell(0)
{
}

Heap::Cell::Cell()
{
}

Heap::Cell::Cell(const Value &value1, const Value &value2)
    : value1(value1)
    , value2(value2)
{
}

const Value &Heap::Cell::getValue1() const
{
    return value1;
}

const Value &Heap::Cell::getValue2() const
{
    return value2;
}

HeapRef Heap::allocateCell()
{
    auto index = firstFreeCell++;
    container.resize(index+1);
    return HeapRef(index);
}

const Heap::Cell &Heap::reference(HeapRef heapRef) const
{
    auto refIndex = heapRef.getIndex();
    if (firstFreeCell <= refIndex)
    {
        throw std::runtime_error("reference outside heap");
    }

    return container[refIndex];
}


HeapRef Heap::allocateConsCell(const Value &value1, const Value &value2)
{
    HeapRef ref = allocateCell();
    container[ref.getIndex()] = Cell(value1, value2);
    return ref;
}

HeapRef Heap::allocateClosureCell(uint32_t code, const Frame &frame)
{
    HeapRef ref = allocateCell();
    container[ref.getIndex()] = Cell(Value::createInteger(code) , Value::createFrame(frame));
    return ref;
}

size_t Heap::getCountOfActiveCells()
{
    return firstFreeCell;
}
