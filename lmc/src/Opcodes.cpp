#include "Opcodes.hpp"

const char *string_of_opcode(Opcode e)
{
    switch (e)
    {
        case LDC:
            return "LDC";

        case LD:
            return "LD";

        case ADD:
            return "ADD";

        case SUB:
            return "SUB";

        case MUL:
            return "MUL";

        case DIV:
            return "DIV";

        case CEQ:
            return "CEQ";

        case CGT:
            return "CGT";

        case CGTE:
            return "CGTE";

        case ATOM:
            return "ATOM";

        case CONS:
            return "CONS";

        case CAR:
            return "CAR";

        case CDR:
            return "CDR";

        case SEL:
            return "SEL";

        case JOIN:
            return "JOIN";

        case LDF:
            return "LDF";

        case AP:
            return "AP";

        case RTN:
            return "RTN";

        case DUM:
            return "DUM";

        case RAP:
            return "RAP";

        case STOP:
            return "STOP";

        case TSEL:
            return "TSEL";

        case TAP:
            return "TAP";

        case TRAP:
            return "TRAP";

        case ST:
            return "ST";

        case DBUG:
            return "DBUG";

        case BRK:
            return "BRK";

        default:
            break;
    }

    return "<INVALID INSTRUCTION>";
};

unsigned int parameterCount_of_opcode(Opcode e)
{
    switch (e)
    {

        case LDC:
            return 1;

        case LD:
            return 2;

        case ADD:
            return 0;

        case SUB:
            return 0;

        case MUL:
            return 0;

        case DIV:
            return 0;

        case CEQ:
            return 0;

        case CGT:
            return 0;

        case CGTE:
            return 0;

        case ATOM:
            return 0;

        case CONS:
            return 0;

        case CAR:
            return 0;

        case CDR:
            return 0;

        case SEL:
            return 2;

        case JOIN:
            return 0;

        case LDF:
            return 1;

        case AP:
            return 1;

        case RTN:
            return 0;

        case DUM:
            return 1;

        case RAP:
            return 1;

        case STOP:
            return 0;

        case TSEL:
            return 2;

        case TAP:
            return 1;

        case TRAP:
            return 1;

        case ST:
            return 2;

        case DBUG:
            return 0;

        case BRK:
            return 0;

        default:
            break;
    }

    return 0;
}

std::size_t getCountOfOpcodes()
{
    return BRK+1;
}


