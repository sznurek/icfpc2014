#pragma once

#include<stdexcept>

enum InterpreterErrorType {
    BAD_CODE_PTR, TAG_MISMATCH, CONTROL_MISMATCH, FRAME_MISMATCH, DIV_BY_ZERO
};

class InterpreterError : public std::runtime_error {
public:

    InterpreterError(InterpreterErrorType type);

    static const char *stringOfType(InterpreterErrorType type);
};
