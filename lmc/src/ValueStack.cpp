#include "ValueStack.hpp"
#include <stdexcept>
#include <iostream>

void ValueStack::push(const Value &value)
{
    container.push_back(value);
}

Value ValueStack::pop()
{
    if (container.empty())
    {
        throw std::runtime_error("stack is empty");
    }

    Value value = container.back();
    container.pop_back();
    return value;
}

size_t ValueStack::getSize() const
{
    return container.size();
}

void ValueStack::debugPrint() const 
{
    std::cerr << "STACK SIZE: " << container.size() << std::endl;
    for (unsigned int i = 0; i < 5 && i < container.size(); ++i) {
        std::cerr << container[i].debugString() << "  ";
    }
    std::cerr << std::endl;
}

void DataStack::pushInteger(int32_t integerValue)
{
    push(Value::createInteger(integerValue));
}

int32_t DataStack::popInteger()
{
    Value v = pop();
    v.assertTag(TAG_INT);
    return v.intValue();
}

void ControlStack::pushReturnAddress(uint32_t returnAddress)
{
    push(Value::createReturnAddress(returnAddress));
}

uint32_t ControlStack::popReturnAddress()
{
    Value v = pop();
    v.assertTag(TAG_RET);
    return v.intValue();
}

void ControlStack::pushFrame(const Frame &frame)
{
    push(Value::createFrame(frame));
}

Frame ControlStack::popFrame()
{
    Value v = pop();
    v.assertTag(TAG_ENV);
    return v.asFrame();
}

void ControlStack::pushJoin(uint32_t returnAddress)
{
    push(Value::createJoin(returnAddress));
}

uint32_t ControlStack::popJoin()
{
    Value v = pop();
    v.assertTag(TAG_JOIN);
    return v.intValue();
}

void ControlStack::pushStop()
{
    push(Value::createStop());
}
