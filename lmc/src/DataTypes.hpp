#pragma once

#include "Opcodes.hpp"
#include <cstdint>

#include "Value.hpp"
#include "Frame.hpp"

typedef struct {
    typedef int32_t Param;

    Opcode opcode;
    Param param1;
    Param param2;
} Instruction;

