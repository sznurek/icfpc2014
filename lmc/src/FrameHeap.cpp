#include "FrameHeap.hpp"

FrameHeap::FrameHeap() {
}

Frame FrameHeap::allocFrame(std::size_t size, Frame parent) {
    Frame frame = Frame::createFrame(size, parent);
    _knownFrames.push_back(frame);
    return frame;
}
