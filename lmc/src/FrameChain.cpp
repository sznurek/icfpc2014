#include "FrameChain.hpp"

#include "InterpreterError.hpp"

FrameChain::FrameChain() :
    _firstFrame(Frame::createNilFrame())
{
}

Value FrameChain::load(int frameId, int fieldId) {
    Frame frame = _firstFrame;
    for (; frameId; --frameId) {
        if (frame.isEmpty()) {
            throw InterpreterError(FRAME_MISMATCH);
        }
        frame = frame.parent();
    }
    if (frame.isEmpty() || frame.hasDumTag()) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    return frame.get(fieldId);
}

void FrameChain::store(int frameId, int fieldId, Value value) {
    Frame frame = _firstFrame;
    for (; frameId; --frameId) {
        if (frame.isEmpty()) {
            throw InterpreterError(FRAME_MISMATCH);
        }
        frame = frame.parent();
    }
    if (frame.isEmpty() || frame.hasDumTag()) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    return frame.set(fieldId, value);
}

Frame FrameChain::firstFrame() {
    return _firstFrame;
}

void FrameChain::setFirstFrame(Frame frame) {
    _firstFrame = frame;
}
