#pragma once

#include "DataTypes.hpp"

class FrameChain {
public:

    FrameChain();
    
    Value load(int frameId, int fieldId);
    void store(int frameId, int fieldId, Value value);

    Frame firstFrame();
    void setFirstFrame(Frame frame);

private:

    Frame _firstFrame;
};
