#pragma once

#include "Value.hpp"
#include <vector>

class ValueStack
{
public:
    void push(const Value &value);

    Value pop();

    size_t getSize() const;

    void debugPrint() const;
private:
    std::vector<Value> container;
};

class DataStack: public ValueStack
{
public:
    int32_t popInteger();
    void pushInteger(int32_t integerValue);
};

class ControlStack: public ValueStack
{
public:
    uint32_t popReturnAddress();
    void pushReturnAddress(uint32_t returnAddress);
    void pushStop();

    Frame popFrame();
    void pushFrame(const Frame &frame);

    uint32_t popJoin();
    void pushJoin(uint32_t returnAddress);
};
