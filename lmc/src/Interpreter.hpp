#pragma once

#include <vector>
#include "DataTypes.hpp"
#include "Memory.hpp"
#include <functional>

class Interpreter {
public:
    struct Statistics
    {
        size_t  countOfCPUTicks;
        size_t  countOfActiveHeapCells;
    };

    explicit Interpreter(std::vector<Instruction> code);
    ~Interpreter();

    Interpreter(const Interpreter&) = delete;
    const Interpreter& operator = (const Interpreter &) = delete;

    Statistics executeCode();

    Statistics executeCodeFromClosure(const Value &value, size_t argumentCount);

    void stopMachine();

    Memory &getMemory();

private:
    void executeOpcode_LDC(const Instruction &);
    void executeOpcode_LD(const Instruction &);
    void executeOpcode_ADD(const Instruction &);
    void executeOpcode_SUB(const Instruction &);
    void executeOpcode_MUL(const Instruction &);
    void executeOpcode_DIV(const Instruction &);
    void executeOpcode_CEQ(const Instruction &);
    void executeOpcode_CGT(const Instruction &);
    void executeOpcode_CGTE(const Instruction &);
    void executeOpcode_ATOM(const Instruction &);
    void executeOpcode_CONS(const Instruction &);
    void executeOpcode_CAR(const Instruction &);
    void executeOpcode_CDR(const Instruction &);
    void executeOpcode_SEL(const Instruction &);
    void executeOpcode_JOIN(const Instruction &);
    void executeOpcode_LDF(const Instruction &);
    void executeOpcode_AP(const Instruction &);
    void executeOpcode_RTN(const Instruction &);
    void executeOpcode_DUM(const Instruction &);
    void executeOpcode_RAP(const Instruction &);
    void executeOpcode_STOP(const Instruction &);
    void executeOpcode_TSEL(const Instruction &);
    void executeOpcode_TAP(const Instruction &);
    void executeOpcode_TRAP(const Instruction &);
    void executeOpcode_ST(const Instruction &);
    void executeOpcode_DBUG(const Instruction &);
    void executeOpcode_BRK(const Instruction &);

    void buildInterpreterTable();

    void executeOneInstructionUnderInstructionPointer();

    std::vector<Instruction> _code;
    std::vector<std::function<void (Interpreter &, const Instruction &)>> interpreterTable;
    unsigned int _control;

    Memory _memory;

    bool workingFlag;
};
