#include "Interpreter.hpp"
#include "InterpreterError.hpp"
#include "Debug.hpp"
#include <iostream>
#include <string>

Interpreter::Interpreter(std::vector<Instruction> code) :
    _code(code), _control(0), workingFlag(false)
{
    _memory.controlStack.pushStop();
    buildInterpreterTable();
}

Interpreter::~Interpreter() {
}

void Interpreter::executeOneInstructionUnderInstructionPointer()
{
    const auto &instr = _code[_control];

#if 0
    std::cerr << _control << ": " << string_of_opcode(instr.opcode) << std::endl;
    _memory.dataStack.debugPrint();
    std::string dupa;
    std::getline(std::cin, dupa);
#endif

    interpreterTable[instr.opcode](*this, instr);
}

Interpreter::Statistics Interpreter::executeCode()
{
    Statistics statistics{0,0};

    _memory.controlStack.pushStop();

    workingFlag     = true;

    while (workingFlag and _control < _code.size())
    {
        executeOneInstructionUnderInstructionPointer();
        statistics.countOfCPUTicks++;
    }
    statistics.countOfActiveHeapCells = _memory.heap.getCountOfActiveCells();

    workingFlag     = false;

    return statistics;
}

Interpreter::Statistics Interpreter::executeCodeFromClosure(const Value &value, size_t argumentCount)
{
    // we emulate the TAP instruction
    Instruction instruction;
    instruction.opcode = TAP;
    instruction.param1 = argumentCount;
    instruction.param2 = 0;

    _memory.dataStack.push(value);

    executeOpcode_TAP(instruction);

    return executeCode();
}

void Interpreter::stopMachine()
{
    workingFlag = false;
}

void Interpreter::executeOpcode_LDC(const Instruction &instr)
{
    _memory.dataStack.pushInteger(instr.param1);
    ++_control;
}

void Interpreter::executeOpcode_LD(const Instruction &instr)
{
    _memory.dataStack.push(_memory.frameChain.load(instr.param1, instr.param2));
    ++_control;
}

void Interpreter::executeOpcode_ADD(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger(x + y);
    ++_control;
}

void Interpreter::executeOpcode_SUB(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger(x - y);
    ++_control;
}

void Interpreter::executeOpcode_MUL(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger(x * y);
    ++_control;
}

void Interpreter::executeOpcode_DIV(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();

    if (y == 0) {
        throw InterpreterError(DIV_BY_ZERO);
    }

    _memory.dataStack.pushInteger(x / y);
    ++_control;
}

void Interpreter::executeOpcode_CEQ(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger((x == y)? 1 : 0);
    ++_control;
}

void Interpreter::executeOpcode_CGT(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger((x > y) ? 1 : 0);
    ++_control;
}

void Interpreter::executeOpcode_CGTE(const Instruction &)
{
    auto y = _memory.dataStack.popInteger();
    auto x = _memory.dataStack.popInteger();
    _memory.dataStack.pushInteger((x >= y ? 1 : 0));
    ++_control;
}

void Interpreter::executeOpcode_ATOM(const Instruction &)
{
    Value x = _memory.dataStack.pop();
    _memory.dataStack.pushInteger((x.tag() == TAG_INT) ? 1 : 0);
    ++_control;
}

void Interpreter::executeOpcode_CONS(const Instruction &)
{
    Value y = _memory.dataStack.pop();
    Value x = _memory.dataStack.pop();
    _memory.dataStack.push(_memory.allocateConsCell(x, y));
    ++_control;
}

void Interpreter::executeOpcode_CAR(const Instruction &)
{
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CONS) {
        throw InterpreterError(TAG_MISMATCH);
    }
    _memory.dataStack.push(_memory.getHeapCar(x));
    ++_control;
}

void Interpreter::executeOpcode_CDR(const Instruction &)
{
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CONS) {
        throw InterpreterError(TAG_MISMATCH);
    }
    _memory.dataStack.push(_memory.getHeapCdr(x));
    ++_control;
}

void Interpreter::executeOpcode_SEL(const Instruction &instr)
{
    auto x = _memory.dataStack.popInteger();

    _memory.controlStack.pushJoin(_control + 1);
    if (x == 0) {
        _control = instr.param2;
    } else {
        _control = instr.param1;
    }
}

void Interpreter::executeOpcode_JOIN(const Instruction &)
{
    _control = _memory.controlStack.popJoin();
}

void Interpreter::executeOpcode_LDF(const Instruction &instr)
{
    _memory.dataStack.push(_memory.allocateClosureCell(instr.param1, 
        _memory.frameChain.firstFrame()));
    ++_control;
}

void Interpreter::executeOpcode_AP(const Instruction &instr)
{
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CLOSURE) {
        throw InterpreterError(TAG_MISMATCH);
    }

    Frame fp = _memory.frameHeap.allocFrame(instr.param1, _memory.getClosureFrame(x));
    for (int i = instr.param1 - 1; i >= 0; --i) {
        fp.set(i, _memory.dataStack.pop());
    }
    _memory.controlStack.pushFrame(_memory.frameChain.firstFrame());
    _memory.controlStack.pushReturnAddress(_control+1);
    _memory.frameChain.setFirstFrame(fp);
    _control = _memory.getClosureCode(x);
}

void Interpreter::executeOpcode_RTN(const Instruction &)
{
    Value x = _memory.controlStack.pop();
    if (x.tag() == TAG_STOP) {
        stopMachine();
        return;
    } else if (x.tag() != TAG_RET) {
        throw InterpreterError(CONTROL_MISMATCH);
    }
    _memory.frameChain.setFirstFrame(_memory.controlStack.pop().asFrame());
    _control = x.intValue();
}

void Interpreter::executeOpcode_DUM(const Instruction &instr)
{
    Frame fp = _memory.frameHeap.allocFrame(instr.param1, _memory.frameChain.firstFrame());
    fp.setDumTag();
    _memory.frameChain.setFirstFrame(fp);
    ++_control;
}

void Interpreter::executeOpcode_RAP(const Instruction &instr)
{
    // TODO: Cos tu jest spieprzone
    // W specyfikacji sprawdzamy czy FRAME_SIZE(%e) == $n, ale wypelniamy $fp
    // Pozwolilem sobie napisac troche inaczej
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CLOSURE) {
        throw InterpreterError(TAG_MISMATCH);
    }
    Frame dum_fp = _memory.frameChain.firstFrame();
    if (!dum_fp.hasDumTag() || dum_fp.size() != instr.param1) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    for (int i = instr.param1 - 1; i >= 0; --i) {
        dum_fp.set(i, _memory.dataStack.pop());
    }
    dum_fp.unsetDumTag();
    _memory.controlStack.pushFrame(dum_fp.parent());
    _memory.controlStack.pushReturnAddress(_control+1);
    _memory.frameChain.setFirstFrame(_memory.getClosureFrame(x));
    _control = _memory.getClosureCode(x);
}

void Interpreter::executeOpcode_STOP(const Instruction &)
{
    stopMachine();
}

void Interpreter::executeOpcode_TSEL(const Instruction &instr)
{
    auto x = _memory.dataStack.popInteger();
    if (x == 0) {
        _control = instr.param2;
    } else {
        _control = instr.param1;
    }
}

void Interpreter::executeOpcode_TAP(const Instruction &instr)
{
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CLOSURE) {
        throw InterpreterError(TAG_MISMATCH);
    }
    Frame fp = _memory.frameHeap.allocFrame(instr.param1, _memory.getClosureFrame(x));
    for (int i = instr.param1 - 1; i >= 0; --i) {
        fp.set(i, _memory.dataStack.pop());
    }
    _memory.frameChain.setFirstFrame(fp);
    _control = _memory.getClosureCode(x);
}

void Interpreter::executeOpcode_TRAP(const Instruction &instr)
{
    // TODO: Ta sama uwaga co przy RAP
    Value x = _memory.dataStack.pop();
    if (x.tag() != TAG_CLOSURE) {
        throw InterpreterError(TAG_MISMATCH);
    }
    Frame dum_fp = _memory.frameChain.firstFrame();
    if (!dum_fp.hasDumTag() || dum_fp.size() != instr.param1) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    for (int i = instr.param1 - 1; i >= 0; --i) {
        dum_fp.set(i, _memory.dataStack.pop());
    }
    dum_fp.unsetDumTag();
    _memory.frameChain.setFirstFrame(_memory.getClosureFrame(x));
    _control = _memory.getClosureCode(x);
}

void Interpreter::executeOpcode_ST(const Instruction &instr)
{
    _memory.frameChain.store(instr.param1, instr.param2, _memory.dataStack.pop());
    ++_control;
}

void Interpreter::executeOpcode_DBUG(const Instruction &)
{
    Debug::debug(_memory.dataStack.pop());
    ++_control;
}

void Interpreter::executeOpcode_BRK(const Instruction &)
{
    Debug::breakPoint();
    ++_control;
}

void Interpreter::buildInterpreterTable()
{
    interpreterTable.resize(getCountOfOpcodes());

    interpreterTable[LDC] = &Interpreter::executeOpcode_LDC;
    interpreterTable[LD] = &Interpreter::executeOpcode_LD;
    interpreterTable[ADD] = &Interpreter::executeOpcode_ADD;
    interpreterTable[SUB] = &Interpreter::executeOpcode_SUB;
    interpreterTable[MUL] = &Interpreter::executeOpcode_MUL;
    interpreterTable[DIV] = &Interpreter::executeOpcode_DIV;
    interpreterTable[CEQ] = &Interpreter::executeOpcode_CEQ;
    interpreterTable[CGT] = &Interpreter::executeOpcode_CGT;
    interpreterTable[CGTE] = &Interpreter::executeOpcode_CGTE;
    interpreterTable[ATOM] = &Interpreter::executeOpcode_ATOM;
    interpreterTable[CONS] = &Interpreter::executeOpcode_CONS;
    interpreterTable[CAR] = &Interpreter::executeOpcode_CAR;
    interpreterTable[CDR] = &Interpreter::executeOpcode_CDR;
    interpreterTable[SEL] = &Interpreter::executeOpcode_SEL;
    interpreterTable[JOIN] = &Interpreter::executeOpcode_JOIN;
    interpreterTable[LDF] = &Interpreter::executeOpcode_LDF;
    interpreterTable[AP] = &Interpreter::executeOpcode_AP;
    interpreterTable[RTN] = &Interpreter::executeOpcode_RTN;
    interpreterTable[DUM] = &Interpreter::executeOpcode_DUM;
    interpreterTable[RAP] = &Interpreter::executeOpcode_RAP;
    interpreterTable[STOP] = &Interpreter::executeOpcode_STOP;
    interpreterTable[TSEL] = &Interpreter::executeOpcode_TSEL;
    interpreterTable[TAP] = &Interpreter::executeOpcode_TAP;
    interpreterTable[TRAP] = &Interpreter::executeOpcode_TRAP;
    interpreterTable[ST] = &Interpreter::executeOpcode_ST;
    interpreterTable[DBUG] = &Interpreter::executeOpcode_DBUG;
    interpreterTable[BRK] = &Interpreter::executeOpcode_BRK;
}


Memory &Interpreter::getMemory()
{
    return _memory;
}

