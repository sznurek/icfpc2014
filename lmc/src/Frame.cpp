#include "Frame.hpp"
#include "Value.hpp"
#include "InterpreterError.hpp"
#include <stdexcept>
#include <vector>

struct FrameData {
    FrameData(std::size_t s, Frame p);

    bool  dumTag;
    Frame parent;
    std::vector<Value> data;
};

FrameData::FrameData(std::size_t s, Frame p) :
    dumTag(false),
    parent(p),
    data(s)
{
}

Value Frame::get(int fieldId) {
    if (_data == nullptr || fieldId >= _data->data.size()) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    return _data->data[fieldId];
}

void Frame::set(int fieldId, Value value) {
    if (_data == nullptr || fieldId >= _data->data.size()) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    _data->data[fieldId] = value;
}

void Frame::setDumTag() {
    if (_data == nullptr) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    _data->dumTag = true;
}

void Frame::unsetDumTag() {
    if (_data == nullptr) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    _data->dumTag = false;
}

bool Frame::hasDumTag() {
    return _data != nullptr && _data->dumTag;
}

std::size_t Frame::size() const {
    return _data == nullptr ? 0 : _data->data.size();
}

Frame Frame::parent() const {
    if (_data == nullptr) {
        throw InterpreterError(FRAME_MISMATCH);
    }
    return _data->parent;
}

bool Frame::isEmpty() const {
    return _data == nullptr;
}

Frame Frame::createNilFrame() {
    Frame fr;
    fr._data = nullptr;
    return fr;
}

Frame Frame::createFrame(std::size_t size, Frame parent) {
    Frame fr;
    fr._data = new FrameData(size, parent);
    return fr;
}
