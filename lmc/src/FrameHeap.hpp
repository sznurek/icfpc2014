#pragma once

#include "DataTypes.hpp"
#include <vector>

class FrameHeap {
public:
    FrameHeap();

    Frame allocFrame(std::size_t size, Frame parent);

private:

    std::vector<Frame> _knownFrames;
};
