#include "Value.hpp"
#include <stdexcept>
#include <cassert>
#include <sstream>

Value::Value (Tag tag, int32_t intValue)
    : _tag(tag)
    , _intValue(intValue)
{
}

Value::Value (Tag tag, const Frame &frameValue)
    : _tag(tag)
    , _frameValue(frameValue)
{
}

Value::Value (Tag tag, HeapRef heapRef)
    : _tag(tag)
    , heapRef(heapRef)
{
}

const Frame &Value::asFrame() const {
    assert(_tag == TAG_ENV);
    return _frameValue;
}

Tag Value::tag() const {
    return _tag;
}

int32_t Value::intValue() const {
    return _intValue;
}

HeapRef Value::getHeapReference() const
{
    return heapRef;
}

void Value::assertTag(Tag expectedTag) const
{
    if (expectedTag != _tag)
    {
        throw InvalidTagException();
    }
}

std::string string_of_int(int32_t v)
{
    std::ostringstream ss;
    ss << v;
    return ss.str();
}

std::string Value::debugString() const
{
    std::string result;

    switch (_tag)
    {
        case TAG_INT:
            result += "<TAG_INT|";
            result += string_of_int(_intValue);
            result += ">";
            break;

        case TAG_CONS:
            result += "<TAG_CONS|";
            result += string_of_int(heapRef.getIndex());
            result += ">";
            break;

        case TAG_JOIN:
            result += "TAG_JOIN";
            break;

        case TAG_CLOSURE:
            result += "<TAG_CLOSURE";
            result += string_of_int(heapRef.getIndex());
            result += ">";
            break;

        case TAG_ENV:
            result += "TAG_ENV";
            break;

        case TAG_RET:
            result += "TAG_RET";
            break;

        case TAG_STOP:
            result += "TAG_STOP";
            break;

        default:
            result += "<INVALID TAG>";
            break;
    }

    return result;

}
