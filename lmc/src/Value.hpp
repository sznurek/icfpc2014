#pragma once

#include <cstdint>
#include <stdexcept>
#include "Frame.hpp"
#include "HeapRef.hpp"

enum Tag {
    TAG_INT, TAG_CONS, TAG_JOIN, TAG_CLOSURE, TAG_ENV, TAG_RET, TAG_STOP
};

class Value {
public:
    struct InvalidTagException : public std::runtime_error
    {
        InvalidTagException()
            : std::runtime_error("invalid tag")
        {
        }
    };

    Value () {}
    Value (Tag tag, int32_t intValue);
    Value (Tag tag, HeapRef heapRef);
    Value (Tag tag, const Frame &frameValue);

    Tag tag() const;
    int32_t intValue() const;

    const Frame &asFrame() const;

    HeapRef getHeapReference() const;

    static Value createInteger(int32_t integerValue)
    {
        return Value(TAG_INT, integerValue);
    }

    static Value createReturnAddress(uint32_t integerValue)
    {
        return Value(TAG_RET, integerValue);
    }

    static Value createFrame(const Frame &frame)
    {
        return Value(TAG_ENV, frame);
    }

    static Value createJoin(uint32_t integerValue)
    {
        return Value(TAG_JOIN, integerValue);
    }

    static Value createCons(HeapRef heapRef)
    {
        return Value(TAG_CONS, heapRef);
    }

    static Value createClosure(HeapRef heapRef)
    {
        return Value(TAG_CLOSURE, heapRef);
    }

    static Value createStop()
    {
        return Value(TAG_STOP, 0);
    }

    void assertTag(Tag expectedTag) const;

    std::string debugString() const;
private:

    Tag _tag;

    int32_t _intValue;
    HeapRef heapRef;
    Frame _frameValue;
};

// ============================================================================

