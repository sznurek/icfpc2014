#include "InterpreterError.hpp"

InterpreterError::InterpreterError(InterpreterErrorType type)
    : std::runtime_error(stringOfType(type))
{
}

const char *InterpreterError::stringOfType(InterpreterErrorType type) {
    switch (type) {
    case BAD_CODE_PTR:
        return "BAD_CODE_PTR";
    case TAG_MISMATCH:
        return "TAG_MISMATCH";
    case CONTROL_MISMATCH:
        return "CONTROL_MISMATCH";
    case FRAME_MISMATCH:
        return "FRAME_MISMATCH";
    case DIV_BY_ZERO:
        return "DIV_BY_ZERO";
    }
}
