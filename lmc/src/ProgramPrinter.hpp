#pragma once
#include <vector>
#include <string>
#include <map>
#include "DataTypes.hpp"

void printProgram(const std::map<std::string, unsigned int> &symbolTable, const std::vector<Instruction> &program, bool printingAddressFlag = true);

void printProgram(const std::vector<Instruction> &program, bool printingAddressFlag = true);
