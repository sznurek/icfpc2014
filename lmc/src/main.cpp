#include "InputParser.hpp"
#include "ProgramPrinter.hpp"
#include "Interpreter.hpp"
#include "Game.hpp"
#include <stdexcept>
#include <iostream>     // std::cerr
#include <fstream>      // std::ifstream
#include <algorithm>

class Application
{
public:
    Application(const InputParser::Result &parserResult)
        : interpreter(parserResult.program)
    {
    }

    void run()
    {
        working = true;
        initialize();
        mainLoop();
    }

    void initialize()
    {
        using namespace std;

        string command;
        if (cin  >> command && command == "INIT")
        {
            currentWorldState = parseCurrentWorldState(cin);
            auto statistics = executeInitializationFunctionInLMC();
            std::cout
                << "DONE "
                << statistics.countOfCPUTicks
                << " "
                << statistics.countOfActiveHeapCells
                << "\n";
        }
        else
        {
            throw std::runtime_error("invalid initialization sequence");
        }
    }

    Value putTupleOnHeap(const std::vector<Value> &values)
    {
        // works only for list of size >= 2
        auto list = values.back();
        auto &memory = interpreter.getMemory();

        for (int i = values.size() - 2; i >= 0; i--)
        {
            list = memory.allocateConsCell(values[i], list);
        }

        return list;
    }

    Value putListOnHeap(const std::vector<Value> &values)
    {
        auto list = Value::createInteger(0);
        auto &memory = interpreter.getMemory();

        for (int i = values.size() - 1; i >= 0; i--)
        {
            list = memory.allocateConsCell(values[i], list);
        }

        return list;
    }



    Value encodeMap(const Map &map)
    {
        using namespace std;

        Value encodedMap;
        vector<Value> encodedRows;

        for (auto &row : map.fields)
        {
            vector<Value> columns;
            columns.resize(row.size());

            transform(row.begin(), row.end(), columns.begin(), 
                [&] (unsigned char field)
                {
                    return Value::createInteger(field);
                }
            );

            encodedRows.push_back(putListOnHeap(columns));
        }
        return putListOnHeap(encodedRows);
    }

    Value encodeLambdaMan(const LambdaMan &lambdaMan)
    {
        Value encodedVitality   = Value::createInteger(lambdaMan.vitality);
        Value encodedPosition   = putTupleOnHeap({
            Value::createInteger(lambdaMan.position.x),
            Value::createInteger(lambdaMan.position.y)});
        Value encodedDirection  = Value::createInteger(lambdaMan.direction);
        Value encodedLives      = Value::createInteger(lambdaMan.lives);
        Value encodedScore      = Value::createInteger(lambdaMan.score);

        return putTupleOnHeap(
                { encodedVitality
                , encodedPosition
                , encodedDirection
                , encodedLives
                , encodedScore
                });
    }

    Value encodeGhosts(const std::vector<GhostState> &ghosts)
    {
        std::vector<Value> encodedGhosts;

        for (auto &ghost : ghosts)
        {
            encodedGhosts.push_back(putTupleOnHeap(
                { Value::createInteger(ghost.vitality)
                , putTupleOnHeap(
                    { Value::createInteger(ghost.position.x)
                    , Value::createInteger(ghost.position.y)
                    })
                , Value::createInteger(ghost.direction)
                })
            );
        }

        return putListOnHeap(encodedGhosts);
    }

    Value putCurrentWorldStateInsideMemory()
    {
        Value encodedMap        = encodeMap(currentWorldState.map);
        Value encodedLambdaMan  = encodeLambdaMan(currentWorldState.lambdaMan);
        Value encodedGhosts     = encodeGhosts(currentWorldState.ghosts);
        Value encodedFruits     = Value::createInteger(currentWorldState.fruits);

        auto world = putTupleOnHeap({encodedMap, encodedLambdaMan, encodedGhosts, encodedFruits});

        return world;
    }

    void mainLoop()
    {
        using namespace std;

        string command;
        while (working and cin >> command)
        {
            if (command == "STEP")
            {
                currentWorldState = parseCurrentWorldState(cin);
                auto result = executeOneGameIterationInLMC();
                auto statistics = result.first;
                auto move       = result.second;
                std::cout
                    << "DONE "
                    << statistics.countOfCPUTicks
                    << " "
                    << statistics.countOfActiveHeapCells
                    << " "
                    << move
                    << "\n";
            }
            else
            if (command == "STOP")
            {
                cout << "ROGER_THAT\n";
                working = false;
            }
            else
            {
                cout << "ERROR protocol error\n";
                working = false;
            }
        }
    }

    Interpreter::Statistics executeInitializationFunctionInLMC()
    {
        auto &memory                 = interpreter.getMemory();
        Frame f                      = memory.frameHeap.allocFrame(2, memory.frameChain.firstFrame());

        f.set(0, putCurrentWorldStateInsideMemory());
        f.set(1, Value::createInteger(0));

        memory.frameChain.setFirstFrame(f);

        auto statistics              = interpreter.executeCode();
        Value resultOfInitialization = memory.dataStack.pop();

        stepValue    = memory.getHeapCar(resultOfInitialization);
        closureValue = memory.getHeapCdr(resultOfInitialization);

        return statistics;
    }

    std::pair<Interpreter::Statistics, unsigned int> executeOneGameIterationInLMC()
    {
        auto &memory                 = interpreter.getMemory();
        memory.dataStack.push(stepValue);
        memory.dataStack.push(putCurrentWorldStateInsideMemory());
        auto statistics = interpreter.executeCodeFromClosure(closureValue, 2);

        Value resultOfStep = memory.dataStack.pop();
        stepValue   = memory.getHeapCar(resultOfStep);
        auto move   = memory.getHeapCdr(resultOfStep).intValue();
        return {statistics, move};
    }

private:
    Interpreter interpreter;
    Value closureValue;
    Value stepValue;
    CurrentWorldState currentWorldState;
    bool working;
};

void createFillerFrame(Interpreter& interpreter) {
    auto& memory = interpreter.getMemory();
    Frame frame  = memory.frameHeap.allocFrame(2, memory.frameChain.firstFrame());

    frame.set(0, Value::createInteger(0));
    frame.set(1, Value::createInteger(0));

    memory.frameChain.setFirstFrame(frame);
}

int main(int argc, char **argv)
{
    InputParser parser;
    InputParser::Result parserResult;

//    std::cin.exceptions ( std::ifstream::failbit | std::ifstream::badbit );

    if (argc == 1)
    {
        parserResult = parser.parseStream(std::cin);
        printProgram(parserResult.program, false);
    }
    else
    if (argc == 2)
    {
        parserResult = parser.parseFile(argv[1]);
        Application application(parserResult);
        application.run();
    }
    else
    if (argc == 3)
    {
        parserResult = parser.parseFile(argv[1]);
        printProgram(parserResult.symbolTable, parserResult.program);
        Interpreter interpreter(parserResult.program);
        createFillerFrame(interpreter);
        interpreter.executeCode();
    }
    else
    {
        fprintf(stderr, "%s: bad usage\n", argv[0]);
        return 1;
    }

    return 0;
}
