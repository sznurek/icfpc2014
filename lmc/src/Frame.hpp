#pragma once

#include <cstdint>

class Value;
struct FrameData;

class Frame {
public:

    Value get(int fieldId);
    void set(int fieldId, Value value);

    void setDumTag();
    void unsetDumTag();
    bool hasDumTag();

    std::size_t size() const;

    Frame parent() const;
    bool isEmpty() const;

    static Frame createNilFrame();
    static Frame createFrame(std::size_t size, Frame parent);

private:

    FrameData *_data;
};
