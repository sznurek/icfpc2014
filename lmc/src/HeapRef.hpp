#pragma once


class HeapRef
{
public:
    HeapRef()
        : index(-1)
    {
    }

    HeapRef(unsigned int index)
        : index(index)
    {
    };

    unsigned int getIndex() const
    {
        return index;
    }

private:
    unsigned int index;
};
