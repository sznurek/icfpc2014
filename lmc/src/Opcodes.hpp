#pragma once
#include <cstdint>

enum Opcode {
    LDC,
    LD,
    ADD,
    SUB,
    MUL,
    DIV,
    CEQ,
    CGT,
    CGTE,
    ATOM,
    CONS,
    CAR,
    CDR,
    SEL,
    JOIN,
    LDF,
    AP,
    RTN,
    DUM,
    RAP,
    STOP,
    TSEL,
    TAP,
    TRAP,
    ST,
    DBUG,
    BRK
};

const char *string_of_opcode(Opcode e);
unsigned int parameterCount_of_opcode(Opcode e);

std::size_t getCountOfOpcodes();
