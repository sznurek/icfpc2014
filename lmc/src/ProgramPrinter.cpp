#include "ProgramPrinter.hpp"
#include <stdio.h>


std::map<unsigned int, std::string> buildReverseSymbolTable(const std::map<std::string, unsigned int> &symbolTable)
{
    std::map<unsigned int, std::string> revmap;

    for (const auto &elem : symbolTable)
    {
        revmap[elem.second] = elem.first;
    }

    return revmap;
}

std::string getStringFromReversedSymbolTable(const std::map<unsigned int, std::string> &reversedSymbolTable, unsigned int currentInstructionNumber)
{
    auto iter = reversedSymbolTable.find(currentInstructionNumber);
    if (iter != reversedSymbolTable.end())
    {
        return iter->second;
    }

    return std::string();
}

void printCurrentAddressWhenEnabled(unsigned int currentInstructionNumber, bool printingAddressFlag)
{
    if (printingAddressFlag)
    {
        printf("%03u ", currentInstructionNumber);
    }
}

void printLabelIfExists(const std::map<unsigned int, std::string> &reversedSymbolTable, unsigned int currentInstructionNumber, bool printingAddressFlag)
{
    auto iter = reversedSymbolTable.find(currentInstructionNumber);
    if (iter != reversedSymbolTable.end())
    {
        printCurrentAddressWhenEnabled(currentInstructionNumber, printingAddressFlag);
        printf("%s:\n", iter->second.c_str());
    }
}


void printProgram(const std::map<std::string, unsigned int> &symbolTable, const std::vector<Instruction> &program, bool printingAddressFlag)
{
    auto reversedSymbolTable = buildReverseSymbolTable(symbolTable);
    unsigned int currentInstructionNumber = 0;

    for (const auto &instruction : program)
    {
        printLabelIfExists(reversedSymbolTable, currentInstructionNumber, printingAddressFlag);
        printCurrentAddressWhenEnabled(currentInstructionNumber, printingAddressFlag);
        printf("    %-5s", string_of_opcode(instruction.opcode));
        auto parameterCount = parameterCount_of_opcode(instruction.opcode);
        if (parameterCount == 1)
        {
            printf(" %i", instruction.param1);
        }
        else
        if (parameterCount == 2)
        {
            printf(" %i %i", instruction.param1, instruction.param2);
        }
        printf("\n");
        currentInstructionNumber++;
    }
}

void printProgram(const std::vector<Instruction> &program, bool printingAddressFlag)
{
    std::map<std::string, unsigned int> symbolTable;

    printProgram(symbolTable, program, printingAddressFlag);
}
