#pragma once
#include "Opcodes.hpp"
#include "DataTypes.hpp"
#include <string>
#include <iostream>
#include <vector>
#include <map>

class InputParser
{
public:
    struct Context;

    struct Result
    {
        std::map<std::string, unsigned int> symbolTable;
        std::vector<Instruction> program;
    };

    InputParser()
    {
    }

    Result parseFile(const std::string &path);

    Result parseStream(std::istream &stream);

private:

    void parseLine(Context &context, const std::string &line);

    void parseTokens(Context &context, const std::vector<std::string> &tokens);

    Result makeResult(Context &context);
};
