#include "InputParser.hpp"
#include "Opcodes.hpp"
#include "DataTypes.hpp"
#include <functional>
#include <fstream>
#include <sstream>
#include <map>
#include <cctype>
#include <cstdint>
#include <boost/algorithm/string.hpp>

namespace
{

struct IntermediateOpcode
{
    typedef std::function<Instruction::Param (const InputParser::Context &, const std::string &)> ParamHandler;

    IntermediateOpcode(
            Opcode opcode,
            const std::string &param1,
            const std::string &param2,
            const ParamHandler &paramHandler1,
            const ParamHandler &paramHandler2
        )
        : opcode(opcode)
        , param1(param1)
        , param2(param2)
        , paramHandler1(paramHandler1)
        , paramHandler2(paramHandler2)
    {
    }

    Opcode opcode;
    std::string param1;
    std::string param2;
    ParamHandler paramHandler1;
    ParamHandler paramHandler2;
};
}

struct InputParser::Context
{
    Context()
        : currentInstructionNumber()
    {
    }

    unsigned int currentInstructionNumber;

    std::map<std::string, unsigned int> symbolTable;
    std::vector<IntermediateOpcode> program;
};

namespace
{

Instruction::Param parseIntegerParam(const InputParser::Context &, const std::string &parameter)
{
    Instruction::Param parsedParameter;
    std::istringstream sstream(parameter);
    sstream >> parsedParameter;
    return parsedParameter;
}

Instruction::Param parseNonNegativeIntegerParam(const InputParser::Context &context, const std::string &parameter)
{
    Instruction::Param parsedParameter = parseIntegerParam(context, parameter);
    if (parsedParameter < 0) throw std::runtime_error("NonNegative integer required");
    return parsedParameter;
}


Instruction::Param parseUnusedParam(const InputParser::Context &, const std::string &)
{
    return 0;
}

Instruction::Param parseAddrParam(const InputParser::Context &context, const std::string &parameter)
{
    if (isdigit(parameter[0]))
    {
        return parseNonNegativeIntegerParam(context, parameter);
    }

    auto iter = context.symbolTable.find(parameter);
    if (iter == context.symbolTable.end())
    {
        throw std::runtime_error("unknown symbol");
    }

    return iter->second;
}


struct OpcodeParserHandler
{
    typedef std::function<Instruction::Param (const InputParser::Context &, const std::string &)> ParamHandler;

    OpcodeParserHandler(
            Opcode opcode,
            const char *token,
            unsigned int paramCount,
            ParamHandler paramHandler1 = parseUnusedParam,
            ParamHandler paramHandler2 = parseUnusedParam
        )
        : opcode(opcode)
        , token(token)
        , paramCount(paramCount)
        , paramHandler1(paramHandler1)
        , paramHandler2(paramHandler2)
    {
    }

    Opcode opcode;
    std::string token;
    unsigned int paramCount;

    ParamHandler paramHandler1;
    ParamHandler paramHandler2;
};


const std::map<std::string, OpcodeParserHandler> tokenMap 
    { { "LDC",  OpcodeParserHandler(LDC, "LDC",     1, parseIntegerParam)}
    , { "LD",   OpcodeParserHandler(LD, "LD",       2, parseNonNegativeIntegerParam, parseNonNegativeIntegerParam) }
    , { "ADD",  OpcodeParserHandler(ADD, "ADD",     0) }
    , { "SUB",  OpcodeParserHandler(SUB, "SUB",     0) }
    , { "MUL",  OpcodeParserHandler(MUL, "MUL",     0) }
    , { "DIV",  OpcodeParserHandler(DIV, "DIV",     0) }
    , { "CEQ",  OpcodeParserHandler(CEQ, "CEQ",     0) }
    , { "CGT",  OpcodeParserHandler(CGT, "CGT",     0) }
    , { "CGTE", OpcodeParserHandler(CGTE, "CGTE",   0) }
    , { "ATOM", OpcodeParserHandler(ATOM, "ATOM",   0) }
    , { "CONS", OpcodeParserHandler(CONS, "CONS",   0) }
    , { "CAR",  OpcodeParserHandler(CAR, "CAR",     0) }
    , { "CDR",  OpcodeParserHandler(CDR, "CDR",     0) }
    , { "SEL",  OpcodeParserHandler(SEL, "SEL",     2, parseAddrParam, parseAddrParam) }
    , { "JOIN", OpcodeParserHandler(JOIN, "JOIN",   0) }
    , { "LDF",  OpcodeParserHandler(LDF, "LDF",     1, parseAddrParam) }
    , { "AP",   OpcodeParserHandler(AP, "AP",       1, parseNonNegativeIntegerParam) }
    , { "RTN",  OpcodeParserHandler(RTN, "RTN",     0) }
    , { "DUM",  OpcodeParserHandler(DUM, "DUM",     1, parseNonNegativeIntegerParam) }
    , { "RAP",  OpcodeParserHandler(RAP, "RAP",     1, parseNonNegativeIntegerParam) }
    , { "STOP", OpcodeParserHandler(STOP, "STOP",   0) }
    , { "TSEL", OpcodeParserHandler(TSEL, "TSEL",   2, parseAddrParam, parseAddrParam) }
    , { "TAP",  OpcodeParserHandler(TAP, "TAP",     1, parseNonNegativeIntegerParam) }
    , { "TRAP", OpcodeParserHandler(TRAP, "TRAP",   1, parseNonNegativeIntegerParam) }
    , { "ST",   OpcodeParserHandler(ST, "ST",       2, parseNonNegativeIntegerParam, parseNonNegativeIntegerParam) }
    , { "DBUG", OpcodeParserHandler(DBUG, "DBUG",   0) }
    , { "BRK",  OpcodeParserHandler(BRK, "BRK",     0) }
    };
}

const OpcodeParserHandler &getOpcodeParserHandler(const std::string &token)
{
    auto iter = tokenMap.find(token);
    if (iter == tokenMap.end())
    {
        throw std::runtime_error("unknown opcode");
    }

    return iter->second;
}

InputParser::Result InputParser::parseStream(std::istream &stream)
{
    std::string line;
    Context context;

    while (std::getline (stream, line))
    {
        parseLine(context, line);
    }

    return makeResult(context);
}

InputParser::Result InputParser::parseFile(const std::string &path)
{
    std::ifstream infile(path);
    return parseStream(infile);
}


void InputParser::parseLine(Context &context, const std::string &line)
{
    using namespace std;
    using namespace boost;
    string lineWithoutComment = line;

    int comment_symbol_position = lineWithoutComment.find(';');
    if (comment_symbol_position != std::string::npos)
    {
        lineWithoutComment.resize(comment_symbol_position);
    }

    vector<string> tokens;
    split(tokens, lineWithoutComment, is_any_of(" "));

    tokens.erase(remove_if(tokens.begin(), tokens.end(), 
        [] (const std::string &elem) { return elem.empty(); }
    ), tokens.end());

    if (tokens.size() > 0)
    {
        parseTokens(context, tokens);
    }
}

void InputParser::parseTokens(Context &context, const std::vector<std::string> &tokens)
{
    using namespace std;
    if (tokens[0][tokens[0].size()-1] == ':')
    {
        string label = tokens[0];
        label.resize(label.size()-1);

        if (context.symbolTable.find(label) != context.symbolTable.end())
        {
            throw std::runtime_error("Redefined label");
        }

        context.symbolTable[label] = context.currentInstructionNumber;
    }
    else
    {
        const auto &opcodeParserHandler = getOpcodeParserHandler(tokens[0]);
        if (opcodeParserHandler.paramCount != tokens.size() - 1)
        {
            throw std::runtime_error("Invalid arguments");
        }

        auto first_token    = string();
        auto second_token   = string();

        if (opcodeParserHandler.paramCount == 1)
        {
            first_token = tokens[1];
        }
        else
        if (opcodeParserHandler.paramCount == 2)
        {
            first_token = tokens[1];
            second_token = tokens[2];
        }

        IntermediateOpcode intermediateOpcode(
            opcodeParserHandler.opcode,
            first_token,
            second_token,
            opcodeParserHandler.paramHandler1,
            opcodeParserHandler.paramHandler2
        );

        context.program.push_back(intermediateOpcode);
        context.currentInstructionNumber++;
    }
}


InputParser::Result InputParser::makeResult(Context &context)
{
    Result result;

    result.symbolTable = context.symbolTable;
    for (const auto &intermediateInstruction : context.program)
    {
        Instruction instruction;
        instruction.opcode = intermediateInstruction.opcode;
        instruction.param1 = intermediateInstruction.paramHandler1(context, intermediateInstruction.param1);
        instruction.param2 = intermediateInstruction.paramHandler2(context, intermediateInstruction.param2);

        result.program.push_back(instruction);
    }

    return result;
}

