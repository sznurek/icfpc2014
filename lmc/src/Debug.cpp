#include "Debug.hpp"
#include <stdio.h>
#include <string>

void Debug::debug(Value value)
{
    auto cstr = value.debugString().c_str();
    fprintf(stderr, "[VM]: debug value: %s\n", cstr);

}

void Debug::breakPoint() {
    // TODO: debug
}
