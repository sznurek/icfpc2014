#pragma once

#include "DataTypes.hpp"
#include "ValueStack.hpp"
#include "FrameChain.hpp"
#include "Heap.hpp"
#include "FrameHeap.hpp"

class Memory {
public:

    Memory();
    ~Memory();

    Memory(const Memory &) = delete;
    const Memory &operator = (const Memory &) = delete;

    const Value &getHeapCar(const Value &valueHeapRef);
    const Value &getHeapCdr(const Value &valueHeapRef);

    uint32_t getClosureCode(const Value &valueHeapRef);
    const Frame &getClosureFrame(const Value &valueHeapRef);

    Value allocateConsCell(const Value &value1, const Value &value2);
    Value allocateClosureCell(uint32_t code, const Frame &env);

    DataStack dataStack;
    ControlStack controlStack;
    FrameChain frameChain;
    Heap heap;
    FrameHeap frameHeap;
};
