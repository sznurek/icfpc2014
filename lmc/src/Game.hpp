#pragma once
#include <istream>
#include <vector>

struct Position {
    Position(unsigned char x=0, unsigned char y=0) : x(x), y(y) {}
    unsigned char x, y;
};

struct LambdaMan {
    LambdaMan() {}
    LambdaMan(unsigned int vitality, 
        Position position, 
        unsigned char direction, 
        unsigned int lives, 
        unsigned int score) :
        
        vitality(vitality),
        position(position),
        direction(direction),
        lives(lives),
        score(score)
    {
    }

    unsigned int vitality;
    Position position;
    unsigned char direction;
    unsigned int lives;
    unsigned int score;
};

struct GhostState {
    GhostState() {}
    GhostState(Position p=Position(), unsigned char vitality=0, unsigned char direction=0)
        : position(p), vitality(vitality), direction(direction) {}
    Position position;
    unsigned char vitality;
    unsigned char direction;
};

struct Map {
    Map() {}
    Map(std::vector<std::vector<unsigned char> > fields) : fields(fields) {}

    unsigned char width() const {
        return fields[0].size();
    };

    unsigned char height() const {
        return fields.size();
    }

    std::vector<std::vector<unsigned char> > fields;
};

struct CurrentWorldState {
    CurrentWorldState() {}
    CurrentWorldState(const LambdaMan &lambdaMan, const std::vector<GhostState> &ghosts, const Map &map, int fruits)
        : lambdaMan(lambdaMan), ghosts(ghosts), map(map), fruits(fruits) {}

    LambdaMan lambdaMan;
    std::vector<GhostState> ghosts;
    Map map;
    int fruits;
};


CurrentWorldState parseCurrentWorldState(std::istream& is);

