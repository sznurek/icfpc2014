#pragma once

#include "DataTypes.hpp"

class Debug {
public:

    static void debug(Value value);
    static void breakPoint();
};
