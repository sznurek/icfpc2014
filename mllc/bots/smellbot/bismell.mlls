require "../lib/fundament"
require "../lib/list"
require "../lib/option"
require "../lib/ref"
require "../lib/game"
require "../lib/vec"

(*****************************************************************************)
(*****************************************************************************)
(*****************************************************************************)

fun nope() =
    -10000000
end

fun neigh(cell, dir) =
    nth(snd(cell), dir)
end

fun mkCell() =
    ((ref(0),ref(0)), [ ref(0), ref(0), ref(0), ref(0) ])
end

fun setSmell(cell, v) =
    set_ref(fst(fst(cell)), v)
end

fun getSmell(cell) =
    unref(fst(fst(cell)))
end

fun setCoSmell(cell, v) =
    set_ref(snd(fst(cell)), v)
end

fun getCoSmell(cell) =
    unref(snd(fst(cell)))
end

fun getBismell(cell) =
    getSmell(cell) - getCoSmell(cell)
end

fun junctionCount(cell) =
    foldl(fun(c, n) ->
        if atom(unref(n)) then c
        else c + 1
        end
    end, 0, snd(cell))
end

fun mkFirstRow(width) =
    letrec aux(prev_cell, width) =
        if width = 0 then 0
        else
            let cell = mkCell() in
                set_ref(neigh(cell, 3), prev_cell);
                set_ref(neigh(prev_cell, 1), cell);
                (cell, aux(cell, width - 1))
            end
        end
    and let cell0 = mkCell() in
        (cell0, aux(cell0, width - 1))
    end
end

fun mkRow(prev_row, width) =
    letrec aux(prev_cell, prev_row, width) =
        if width = 0 then 0
        else
            let cell = mkCell() 
            and prev_head = fst(prev_row) in
                set_ref(neigh(cell, 3), prev_cell);
                set_ref(neigh(prev_cell, 1), cell);
                set_ref(neigh(cell, 0),      prev_head);
                set_ref(neigh(prev_head, 2), cell);
                (cell, aux(cell, snd(prev_row), width - 1))
            end
        end
    and let cell0 = mkCell() 
    and prev_head = fst(prev_row) in
        set_ref(neigh(cell0, 0),     prev_head);
        set_ref(neigh(prev_head, 2), cell0);
        (cell0, aux(cell0, snd(prev_row), width - 1))
    end
end

fun mkMeOneWithEverything(width, height) = // mkDungeon
    letrec aux(prev_row, height) =
        if height = 0 then 0
        else
            let row = mkRow(prev_row, width) in
                (row, aux(row, height - 1))
            end
        end
    and let row0 = mkFirstRow(width) in
        (row0, aux(row0, height - 1))
    end
end

fun getCell(dungeon, x, y) =
    nth(nth(dungeon, y), x)
end

fun getCell_vec(dungeon, v) =
    getCell(dungeon, fst(v), snd(v))
end

fun removeCellXY(dungeon, x, y) =
    let me = getCell(dungeon, x, y) in
        removeCell(me)
    end
end

fun removeCell(me) =
    let u  = unref(neigh(me, 0))
    and r  = unref(neigh(me, 1))
    and d  = unref(neigh(me, 2))
    and l  = unref(neigh(me, 3))
    and removeMe(n, dir) =
        if atom(n) then 0
        else
            set_ref(neigh(n, dir), 0)
        end
    in
        removeMe(u, 2);
        removeMe(r, 3);
        removeMe(d, 0);
        removeMe(l, 1)
    end
end

fun iterDungeon(f, dungeon) =
    iter(fun (row) -> iter(f, row) end, dungeon)
end

fun iterDungeon2(f, tiles, dungeon) =
    iter2(fun (tile_row, dun_row) -> iter2(f, tile_row, dun_row) end, tiles, dungeon)
end

fun iterDungeonWindow(f, dungeon, lu, rd) =
    let v_begin = max(snd(lu), 0)
    and v_end   = snd(rd)
    and h_begin = max(fst(lu), 0)
    and h_end   = snd(rd)
    in iterListRange(fun (row) ->
            iterListRange(f, row, h_begin, h_end)
        end, dungeon, v_begin, v_end)
    end
end

fun iterDungeonWindow_rev(f, dungeon, lu, rd) =
    let v_begin = max(snd(lu), 0)
    and v_end   = snd(rd)
    and h_begin = max(fst(lu), 0)
    and h_end   = snd(rd)
    in iterListRange_rev(fun (row) ->
            iterListRange_rev(f, row, h_begin, h_end)
        end, dungeon, v_begin, v_end)
    end
end

fun iterDungeonWindow2(f, tiles, dungeon, lu, rd) =
    let v_begin = max(snd(lu), 0)
    and v_end   = snd(rd)
    and h_begin = max(fst(lu), 0)
    and h_end   = snd(rd)
    in iterListRange2(fun (tile_row, dun_row) ->
            iterListRange2(f, tile_row, dun_row, h_begin, h_end)
        end, tiles, dungeon, v_begin, v_end)
    end
end

fun purgeCell(tile, cell) =
    if tile = 0 then 
        removeCell(cell)
    else
        0
    end
end

fun purgeDungeon(tiles, dungeon) =
    iterDungeon2(purgeCell, tiles, dungeon)
end

fun dirs() = 
    [0, 1, 2, 3]
end

fun smearCell(cell) =
    let foo = foldl(fun(sum, dir) ->
            let n = unref(neigh(cell, dir)) and
            if atom(n) then sum
            else (getSmell(n), sum)
            end
        end, 0, dirs()) and
    let sumLen = length(foo) and
    if sumLen = 0 || getSmell(cell) = nope() then 0
    else setSmell(cell, sum(foo) / sumLen)
    end
end

fun smearRight(dungeon, window_lu, window_rd) =
    iterDungeonWindow(smearCell, dungeon, window_lu, window_rd)
(*    iter(fun (row) -> iter(smearCell, row) end, dungeon) *)
end

fun smearLeft(dungeon, window_lu, window_rd) =
    iterDungeonWindow_rev(smearCell, dungeon, window_lu, window_rd)
(*    iter_rev(fun (row) -> iter_rev(smearCell, row) end, dungeon) *)
end

fun main(world, unused) =
    let dungeon_width  = length(fst(state_getMap(world)))
    and dungeon_height = length(state_getMap(world)) and
    let dungeon = mkMeOneWithEverything(dungeon_width, dungeon_height)
    and temptation     = ref(10000)
    and temptation_vel = ref(10000)
    in
        purgeDungeon(state_getMap(world), dungeon);

        let gate(state, world) =
            let player   = state_getLambdaman(world) 
            and cur_temptation = unref(temptation) and
            let my_cell   = getCell_vec(dungeon, lambdaman_getLocation(player))
            and window_lu = vec_sub(lambdaman_getLocation(player), (20, 20))
            and window_rd = vec_add(lambdaman_getLocation(player), (21, 21))
            and closest_ghost =
                foldl(fun (d, g) ->
                    let gd = vec_dist(lambdaman_getLocation(player), ghost_getLocation(g))
                    and if gd < d then gd else d end
                end, 10000000, state_getGhosts(world))
            and vitality = lambdaman_getVitality(player) and 
            let vitality_factor = if vitality > 500 then -1 else 1 end and
            let jc       = junctionCount(my_cell) 
            and initSmell(tile, cell) =
                if tile = 4 && state_getFruits(world) > 0 then
                    setSmell(cell, nope() * (dungeon_width * dungeon_height / 100) * -3 * vitality_factor)
                elif tile = 3 then
                    if vitality then (* Power pills are not attractive *)
                        setSmell(cell, 0 - nope())
                    elif closest_ghost > 10 then
                        setSmell(cell, 0 - 1000000)
                    else
                        setSmell(cell, cur_temptation)
                    end
                elif tile then
                    setSmell(cell, getSmell(cell) * 15 / 16)
                else 0 end
            and placeGhost(dungeon, loc) =
                setSmell(getCell_vec(dungeon, loc), nope())
            in

                set_ref(temptation, cur_temptation + unref(temptation_vel));
                set_ref(temptation_vel, unref(temptation_vel) + 10);
                if jc = 1 then
                    removeCell(my_cell)
                else 0 end;

                iterDungeonWindow2(initSmell, state_getMap(world), dungeon, window_lu, window_rd);
                iter(fun(g) -> placeGhost(dungeon, ghost_getLocation(g)) end, 
                    state_getGhosts(world));
                
                smearRight(dungeon, window_lu, window_rd);
                smearLeft(dungeon, window_lu, window_rd);

                let my_dir   = lambdaman_getDirection(player) and
                let back_dir = if my_dir < 2 then my_dir + 2 else my_dir - 2 end and
                let decision = foldl(fun(p, dir) ->
                        let n = unref(neigh(my_cell, dir)) and
                        if atom(n) then p
                        else
                            let smell =
                                if dir <> back_dir then
                                    getBismell(n) * vitality_factor
                                elif closest_ghost < 5 then
                                    getBismell(n) * vitality_factor - 1000
                                else
                                    nope() + 1
                                end
                            and if fst(p) < smell then (smell, dir)
                            else p
                            end
                        end
                    end, (nope(), 0), dirs())
                in
                    setCoSmell(my_cell, getCoSmell(my_cell) + 30000);
                    trace(getSmell(my_cell));
                    trace(getCoSmell(my_cell));
                    (state, snd(decision))
                end
            end
        in (0, gate) end
    end
end
