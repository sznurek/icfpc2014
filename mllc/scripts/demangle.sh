#!/bin/sh

LMC=../lmc/lmc
MLLC=_build/src/mllc.native
FILE=${1}

${MLLC} ${1} | ${LMC}
