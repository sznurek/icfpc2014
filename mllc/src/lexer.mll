(* File: lexer.mll *)

{

exception Invalid_character of char
exception Invalid_number    of string

let kw_map = Hashtbl.create 32
let _ =
	Hashtbl.add kw_map "and"       ParserGen.KW_AND;
	Hashtbl.add kw_map "atom"      ParserGen.KW_ATOM;
	Hashtbl.add kw_map "begin"     ParserGen.KW_BEGIN;
	Hashtbl.add kw_map "debug"     ParserGen.KW_DEBUG;
	Hashtbl.add kw_map "do"        ParserGen.KW_DO;
	Hashtbl.add kw_map "done"      ParserGen.KW_DONE;
	Hashtbl.add kw_map "elif"      ParserGen.KW_ELIF;
	Hashtbl.add kw_map "else"      ParserGen.KW_ELSE;
	Hashtbl.add kw_map "end"       ParserGen.KW_END;
	Hashtbl.add kw_map "fun"       ParserGen.KW_FUN;
	Hashtbl.add kw_map "if"        ParserGen.KW_IF;
	Hashtbl.add kw_map "in"        ParserGen.KW_IN;
	Hashtbl.add kw_map "let"       ParserGen.KW_LET;
	Hashtbl.add kw_map "letrec"    ParserGen.KW_LETREC;
	Hashtbl.add kw_map "not"       ParserGen.KW_NOT;
	Hashtbl.add kw_map "require"   ParserGen.KW_REQUIRE;
	Hashtbl.add kw_map "then"      ParserGen.KW_THEN;
	()

let nat_regexp = Str.regexp "^[-]?[0-9]+$"

let tokenize_number str =
	if Str.string_match nat_regexp str 0 then
		ParserGen.NUM (Int32.of_string str)
	else
		raise (Invalid_number str)


let trimQuotes s = String.sub s 1 (String.length s - 2)

}

let vstart = ['a'-'z' 'A'-'Z' '_']
let digit  = ['0'-'9']
let vchar  = vstart | digit

rule token = parse
	  [ ' ' '\t' '\r' ] { token lexbuf }
	| '\n'   { Lexing.new_line lexbuf; token lexbuf }
	| "//"   { line_comment lexbuf }
	| "(*"   { block_comment 1 lexbuf }
	| (vstart vchar*) as x {
			try Hashtbl.find kw_map x with
			| Not_found -> ParserGen.ID x
		}
  | '"' ([^ '"' ]*) '"' as x {
      ParserGen.PATH (trimQuotes x)
    }
	| ('-'? digit vchar*) as x { tokenize_number x }
	| '@' (vchar+ as x) {
			if x = "1" then ParserGen.PROJ_1
			else if x = "2" then ParserGen.PROJ_2
			else ParserGen.PROJ_BAD
		}
	| '('    { ParserGen.BR_OPN   }
	| ')'    { ParserGen.BR_CLS   }
	| '{'    { ParserGen.CBR_OPN  }
	| '}'    { ParserGen.CBR_CLS  }
	| '['    { ParserGen.SBR_OPN  }
	| ']'    { ParserGen.SBR_CLS  }
	| "&&"   { ParserGen.AND      }
	| "->"   { ParserGen.ARROW    }
	| ":="   { ParserGen.ASSIGN   }
	| '*'    { ParserGen.ASTERISK }
	| ','    { ParserGen.COMMA    }
	| '='    { ParserGen.EQ       }
	| '<'    { ParserGen.LT       }
	| "<="   { ParserGen.LE       }
	| '>'    { ParserGen.GT       }
	| ">="   { ParserGen.GE       }
	| "<>"   { ParserGen.NEQ      }
	| '-'    { ParserGen.MINUS    }
	| "||"   { ParserGen.OR       }
	| '+'    { ParserGen.PLUS     }
	| ';'    { ParserGen.SEMI     }
	| '/'    { ParserGen.SLASH    }
	| eof    { ParserGen.EOF      }
	| _ as c { raise (Invalid_character c) }

and line_comment = parse
	  '\n' { Lexing.new_line lexbuf; token lexbuf }
	| eof  { ParserGen.EOF }
	| _    { line_comment lexbuf }

and block_comment depth = parse
	  "(*" { block_comment (depth+1) lexbuf }
	| "*)" {
		if depth = 1 then token lexbuf 
		else block_comment (depth-1) lexbuf }
	| '\n' { Lexing.new_line lexbuf; block_comment depth lexbuf }
	| eof  {
			Errors.error_p lexbuf.Lexing.lex_curr_p
				"End of file inside block comment.";
			ParserGen.EOF
		}
	| _ { block_comment depth lexbuf }
