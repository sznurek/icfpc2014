let require_to_file dir req = Printf.sprintf "%s/%s.mlls" dir req

let rec parse_with_requires dir code =
  let (requires, defs) = Parser.parse code in
  let required_defs = List.map (fun x -> parse_with_requires dir (require_to_file dir x)) requires in
  (List.concat required_defs) @ defs

