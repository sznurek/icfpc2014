open Ast

let rec show_comma_separated_bound_variables' = function
    | [] -> ""
    | (x::xs) -> ", " ^ snd x ^ show_comma_separated_bound_variables' xs

let show_comma_separated_bound_variables = function
    | [] -> ""
    | (x::xs) -> snd x ^ show_comma_separated_bound_variables' xs

let rec show_expr expr =
    show_expr_kind expr.e_kind

and show_letdef = function
    | LD_Val(_, letname, letbody) ->
        "letval " ^ letname ^ " = " ^ show_expr letbody ^ "\n"

    | LD_Fun(_, letname, letargs, letbody) ->
        "letfun " ^ letname ^ "(" ^ show_comma_separated_bound_variables letargs ^ ") = " ^ show_expr letbody ^ "\n"

and show_expr_kind = function
    | EVar var ->
        var

    | ENum i ->
        Int32.to_string i

    | EFun (args, body) ->
        "<EFun expr>"

    | ECall(f_expr, arg_exprs) ->
        show_expr f_expr ^ "(" ^ 
            List.fold_left (fun str arg_expr -> str ^ ", " ^ show_expr arg_expr) "" arg_exprs
        ^ ")"

    | EIf(e1, e2, e3) ->
        "if " ^ show_expr e1 ^ " then\n" ^
        show_expr e2 ^
        "\nelse\n" ^
        show_expr e3 ^
        "\nend\n"

    | ELet(letdefs, inexpr) ->
        "LET {\n" ^
        List.fold_left (fun str letdef -> str ^ show_letdef letdef) "" letdefs ^
        "}\nin\n" ^
        show_expr inexpr ^
        "\nend\n"

    | ELetRec(letdefs, inexpr) ->
        ""

    | EPair(e1, e2) ->
        "(" ^ show_expr e1 ^ ", " ^ show_expr e2  ^ ")"

    | EProj1(e1) ->
        "(" ^ show_expr e1 ^ ")@1"

    | EProj2(e1) ->
        "(" ^ show_expr e1 ^ ")@2"

    | EBinOp(op, e1, e2) ->
        "(" ^ show_expr e1 ^ " <OP> " ^ show_expr e2  ^ ")"

    | EOr(e1, e2) ->
        "(" ^ show_expr e1 ^ " || " ^ show_expr e2  ^ ")"

    | EAnd(e1, e2) ->
        "(" ^ show_expr e1 ^ " && " ^ show_expr e2  ^ ")"

    | ENot(e1) ->
        "not(" ^ show_expr e1 ^ ")"

    | EAtom(e1) ->
        "atom(" ^ show_expr e1 ^ ")"

    | EAssign(x, e1) ->
        "<EAssign expr>"

    | ESeq(e1, e2) ->
        "<ESeq expr>"

    | EDebug(e1) ->
        "debug(" ^ show_expr e1 ^ ")"


let show_top_func top_func =
    "fun " ^ top_func.tf_name ^ "(" ^ show_comma_separated_bound_variables top_func.tf_args ^ ") = \n" ^
    show_expr top_func.tf_body^
    "\nend\n" ^
    "\n"




let show_defs xs =
    List.fold_left (fun str tf -> str ^ show_top_func tf) "" xs

let print_defs xs =
    Printf.eprintf "[INTERNAL CODE]\n%s[END OF INTERNAL CODE]\n%!" (show_defs xs)
