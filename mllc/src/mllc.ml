let transform_opt defs =
    let defs = Remove_unused_func.translate_defs defs in
    let defs = Inline_simple_func.translate_defs defs in 
    let defs = Remove_unused_func.translate_defs defs in
    let defs = Inline_simple_func.translate_defs defs in
    let defs = Remove_unused_func.translate_defs defs in
    let defs = Inline_simple_func.translate_defs defs in
    let defs = Remove_unused_func.translate_defs defs in
    let defs = Inline_simple_func.translate_defs defs in
    let defs = Remove_unused_func.translate_defs defs in
    defs

let transform_inline_once defs =
    let defs = Remove_unused_func.translate_defs defs in
    let defs = Inline_simple_func.translate_defs defs in 
    let defs = Remove_unused_func.translate_defs defs in
    defs

let transform_onlyremove defs =
    let defs = Remove_unused_func.translate_defs defs in 
    defs

let transform_noopt defs = defs

let transform =
    if Array.length Sys.argv > 2 then
        match Sys.argv.(2) with
        | "-O0" -> transform_noopt
        | "-O1" -> transform_onlyremove
        | "-O2" -> transform_inline_once
        | "-O3" -> transform_opt
        | _ -> transform_onlyremove
    else
        transform_onlyremove

let _ =
	let defs = Require.parse_with_requires (Filename.dirname Sys.argv.(1)) Sys.argv.(1) in
    let defs = transform defs in
    Print_ast.print_defs defs;
    let env  = Env.create defs in
    let code = Code.create () in
    CodeGen.gen_preamble code;
    List.iter (CodeGen.gen_function env code) defs ;
    begin try ignore (Env.lookup env "main") with
    | Not_found ->
        Errors.error "Undefined symbol `main'."
    end;
    if Errors.ok () then 
        Code.print_code code
