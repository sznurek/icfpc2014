open Ast

(*****************************************************************************
 * PARAMETERS OF INLINER

 * inliner_parameter_deepth_of_candidate - controls eagerness to inline function
 *)

let inliner_parameter_deepth_of_candidate = 3



(*
*)

type inline_candidate =
    InlineCandidate of string * string list * expr

type knowledge = inline_candidate list

let is_candidate_name name1 (InlineCandidate(name2, _, _))  = name1 = name2

let hide_inline_candidates knowledge bounded_variables =
    List.filter (function InlineCandidate(name, _, _) -> not (List.mem name bounded_variables)) knowledge

let rec zip xs ys = match xs, ys with
    | [], [] -> []
    | x::xs, y::ys -> (x,y)::zip xs ys
    | _ -> failwith "zip"


let find_candidate knowledge name actual_arguments =
    let InlineCandidate(_, formal_arguments, body) =  List.find (is_candidate_name name) knowledge in
    (zip formal_arguments actual_arguments, body)


let rec is_top_func_body_a_candidate body args  =
let parameter_deep = inliner_parameter_deepth_of_candidate  in
match body with
    | ECall ({ e_kind = (EVar called_var) }, es2) ->
        let f_good = not (List.mem called_var args)
        and args_good =  List.for_all (fun earg -> is_top_func_subbody_a_candidate parameter_deep earg.e_kind args) es2
        in
        f_good && args_good

    | _ ->
        is_top_func_subbody_a_candidate parameter_deep body args

and is_top_func_subbody_a_candidate n body args  =
if n = 0 then
false
else
match body with
    | EVar name ->
        List.mem name args

    | ENum _ ->
        true

    | EFun (args, body) ->
        false

    | ECall (e1,es2) ->
       false 

    | EIf (e1,e2,e3) ->
        false

    | ELet (defs, inexpr) ->
        false

    | ELetRec (defs,inexpr) ->
        false

    | EBinOp (_, e1, e2)
    | EOr (e1, e2) 
    | EAnd (e1, e2) 
    | EPair (e1,e2) ->
        is_top_func_subbody_a_candidate (pred n) e1.e_kind args && is_top_func_subbody_a_candidate (pred n) e2.e_kind args

    | EProj1 e1
    | EProj2 e1
    | ENot e1
    | EAtom e1
    | EDebug e1 ->
        is_top_func_subbody_a_candidate (pred n) e1.e_kind args

    | EAssign (_, e1) ->
        false

    | ESeq (e1, e2) ->
        false


let is_some = function
    | None -> false
    | _ -> true


let name_bound_by_letdef = function
    | LD_Val (_, name, _) -> name
    | LD_Fun (_, name, _, _) -> name


let compute_candidate_from_top_func top_func =
    let formal_arguments = List.map snd top_func.tf_args in
    if is_top_func_body_a_candidate top_func.tf_body.e_kind formal_arguments then begin
        Printf.eprintf "[simple inliner] marked %s as inline candidate \n%!" top_func.tf_name;
        Some (InlineCandidate(top_func.tf_name, formal_arguments, top_func.tf_body))
    end
    else
        None

let remove_bounded_variables_from_subst_data subst_data bound_variables =
    List.filter (fun (name, e) -> List.mem name bound_variables) subst_data

let rec parallel_subst_expr data expr =
    {expr with
        e_kind = parallel_subst_expr_kind data expr.e_kind
    }

and parallel_subst_letdef data = function
    | LD_Val (lexpos, letname, letbody) ->
        LD_Val (lexpos, letname, parallel_subst_expr data letbody)

    | LD_Fun (lexpos, letname, letargs, letbody) ->
        let bound_variables = List.map snd letargs in
        let inside_data = remove_bounded_variables_from_subst_data data bound_variables in
        LD_Val (lexpos, letname, parallel_subst_expr inside_data letbody)


and parallel_subst_expr_kind data expr_kind = match expr_kind with
    | EVar var ->
        begin try
            let pred (x, expr) = var = x in
            let (x,expr) = List.find pred data in
            expr.e_kind
        with Not_found ->
            expr_kind
        end

    | ENum _ ->
        expr_kind

    | EFun (args, body) ->
        expr_kind

    | ECall (e1,es2) ->
        ECall (parallel_subst_expr data e1, List.map (parallel_subst_expr data) es2)

    | EIf (e1,e2,e3) ->
        EIf (parallel_subst_expr data e1,parallel_subst_expr data e2,parallel_subst_expr data e3)

    | ELet (defs, inexpr) ->
        let bound_variables = List.map name_bound_by_letdef defs in
        let inside_data = remove_bounded_variables_from_subst_data data bound_variables in
        let new_defs = List.map (parallel_subst_letdef data) defs in
        let new_inexpr = parallel_subst_expr inside_data inexpr in
        ELet (new_defs, new_inexpr)

    | ELetRec (defs,inexpr) ->
        let bound_variables = List.map name_bound_by_letdef defs in
        let inside_data = remove_bounded_variables_from_subst_data data bound_variables in
        let new_defs = List.map (parallel_subst_letdef inside_data) defs in
        let new_inexpr = parallel_subst_expr inside_data inexpr in
        ELet (new_defs, new_inexpr)

    | EPair (e1,e2) ->
        EPair (parallel_subst_expr data e1, parallel_subst_expr data e2)

    | EProj1 expr1 ->
        EProj1 (parallel_subst_expr data expr1)

    | EProj2 expr1 ->
        EProj2 (parallel_subst_expr data expr1)

    | EBinOp (op, e1, e2) ->
        EBinOp(op, parallel_subst_expr data e1, parallel_subst_expr data e2)

    | EOr (e1, e2) ->
        EOr(parallel_subst_expr data e1, parallel_subst_expr data e2)

    | EAnd (e1, e2) ->
        EAnd(parallel_subst_expr data e1, parallel_subst_expr data e2)

    | ENot e1 ->
        ENot(parallel_subst_expr data e1)

    | EAtom e1 ->
        EAtom(parallel_subst_expr data e1)

    | EAssign (x, e1) ->
        EAssign(x, parallel_subst_expr data e1)

    | ESeq (e1, e2) ->
        ESeq(parallel_subst_expr data e1, parallel_subst_expr data e2)

    | EDebug e1 ->
        EDebug(parallel_subst_expr data e1)


let rec inliner_expr knowledge expr =
    {expr with
        e_kind = inliner_expr_kind knowledge expr.e_kind
    }

and inliner_letdef knowledge = function
    | LD_Val (lexpos, letname, letbody) ->
        LD_Val (lexpos, letname, inliner_expr knowledge letbody)

    | LD_Fun (lexpos, letname, letargs, letbody) ->
        let inside_knowledge = hide_inline_candidates knowledge (List.map snd letargs) in

        LD_Fun (lexpos, letname, letargs, inliner_expr inside_knowledge letbody)

and inliner_expr_kind knowledge expr_kind = match expr_kind with
    | EVar _ ->
        expr_kind

    | ENum _ ->
        expr_kind

    | EFun (args, body) ->
        let bound_variables = List.map snd args in
        let inside_knowledge = hide_inline_candidates knowledge bound_variables in
        EFun (args, inliner_expr inside_knowledge body)

    | ECall ({ e_kind = (EVar called_var)} as e1 ,es2) ->
        begin try
            (*Printf.eprintf "[simple inliner] TRYING %s\n%!" called_var; *)
            let (substitution_data, body) = find_candidate knowledge called_var es2 in
            begin
            Printf.eprintf "[simple inliner] inlined %s\n%!" called_var;
            let new_expr_kind = (parallel_subst_expr substitution_data body).e_kind in
            inliner_expr_kind knowledge new_expr_kind
            end
        with Not_found ->
            ECall (inliner_expr knowledge e1, List.map (inliner_expr knowledge) es2)
        end

    | ECall (e1,es2) ->
        ECall (inliner_expr knowledge e1, List.map (inliner_expr knowledge) es2)

    | EIf (e1,e2,e3) ->
        EIf (inliner_expr knowledge e1, inliner_expr knowledge e2, inliner_expr knowledge e3)

    | ELet (defs, inexpr) ->
        let bounded_variables = List.map name_bound_by_letdef defs in
        let new_defs = List.map (inliner_letdef knowledge) defs  in
        let inside_knowledge = hide_inline_candidates knowledge bounded_variables in
        let new_inexpr = inliner_expr inside_knowledge inexpr in
        ELet (new_defs, new_inexpr)

    | ELetRec (defs,inexpr) ->
        let bounded_variables = List.map name_bound_by_letdef defs in
        let inside_knowledge = hide_inline_candidates knowledge bounded_variables in
        let new_defs = List.map (inliner_letdef inside_knowledge) defs  in
        let new_inexpr = inliner_expr inside_knowledge inexpr in
        ELetRec (new_defs, new_inexpr)

    | EPair (e1,e2) ->
        EPair (inliner_expr knowledge e1, inliner_expr knowledge e2)

    | EProj1 expr1 ->
        EProj1 (inliner_expr knowledge expr1)

    | EProj2 expr1 ->
        EProj2 (inliner_expr knowledge expr1)

    | EBinOp (op, e1, e2) ->
        EBinOp(op, inliner_expr knowledge e1, inliner_expr knowledge e2)

    | EOr (e1, e2) ->
        EOr(inliner_expr knowledge e1, inliner_expr knowledge e2)

    | EAnd (e1, e2) ->
        EAnd(inliner_expr knowledge e1, inliner_expr knowledge e2)

    | ENot e1 ->
        ENot(inliner_expr knowledge e1)

    | EAtom e1 ->
        EAtom(inliner_expr knowledge e1)

    | EAssign (x, e1) ->
        EAssign(x, inliner_expr knowledge e1)

    | ESeq (e1, e2) ->
        ESeq(inliner_expr knowledge e1, inliner_expr knowledge e2)

    | EDebug e1 ->
        EDebug(inliner_expr knowledge e1)

let inliner_top_func knowledge top_func =
    (* Printf.eprintf "[simple inliner] TOP_FUNC %s\n%!" top_func.tf_name; *)
    let knowledge = hide_inline_candidates knowledge (List.map snd top_func.tf_args) in
    {top_func with
        tf_body = {top_func.tf_body with e_kind = inliner_expr_kind knowledge top_func.tf_body.e_kind }
    }

let unhask_some = function
    | Some(x) -> x
    | None -> failwith "unhask_some(None)"

let translate_defs xs = 
    Printf.eprintf "[simple inliner] CYCLE\n%!";
    let knowledge = List.map unhask_some (List.filter is_some (List.map compute_candidate_from_top_func xs)) in
    if List.length knowledge = 0 then
        xs
    else
        List.map (inliner_top_func knowledge) xs
