(* File: errors.mli *)

exception Fatal_error

val error_p : Lexing.position -> ('a, out_channel, unit) format -> 'a
val note_p  : Lexing.position -> ('a, out_channel, unit) format -> 'a
val error   : ('a, out_channel, unit) format -> 'a
val note    : ('a, out_channel, unit) format -> 'a

val not_implemented : string -> 'a

val ok : unit -> bool
