(* File: codeGen.ml *)

let fresh_id = ref 0
let fresh_label () =
    let r = !fresh_id in
    fresh_id := r + 1;
    Printf.sprintf "@l%d" r

let gen_preamble code =
    Code.add_instr code (Code.LD(0, 0));
    Code.add_instr code (Code.LD(0, 1));
    Code.add_instr code (Code.LDF "main");
    Code.add_instr code (Code.TAP 2);
    Code.add_label code "@pop";
    Code.add_instr code Code.RTN

let letdef_pos_name ld =
    match ld with
    | Ast.LD_Val(p, x, _) | Ast.LD_Fun(p, x, _, _) -> (p, x)

let check_arg_cnt env pos e cnt =
    match e.Ast.e_kind with
    | Ast.EVar x ->
        begin try match Env.lookup env x with
        | Env.Global(name, argc) ->
            if argc <> cnt then
                Errors.error_p pos 
                    "Function `%s' expects %d parameters, but here it is applied to %d."
                    name argc cnt
        | _ -> ()
        with
        | Not_found -> ()
        end
    | _ -> ()
        

let rec gen_code env code e cont =
    match e.Ast.e_kind with
    | Ast.EVar x ->
        begin try match Env.lookup env x with
        | Env.Local(fr, fld) ->
            Code.add_instr code ~comment:x (Code.LD(fr, fld))
        | Env.Global(name, _) ->
            Code.add_instr code (Code.LDF name)
        with
        | Not_found ->
            Errors.error_p e.Ast.e_pos "`%s' is undefined." x
        end;
        cont ()
    | Ast.ENum n ->
        Code.add_instr code (Code.LDC n);
        cont ()
    | Ast.EFun(args, body) ->
        let l = fresh_label () ^ "_anon" in
        Code.add_instr code (Code.LDF l);
        cont ();
        Code.add_label code l;
        gen_tail_code (Env.extend env args) code body
    | Ast.ECall(func, args) ->
        check_arg_cnt env e.Ast.e_pos func (List.length args);
        iter_gen_code env code args (fun () ->
        gen_code env code func (fun () ->
            Code.add_instr code (Code.AP (List.length args));
            cont ()
        ))
    | Ast.EIf(cond, b1, b2) ->
        let l1 = fresh_label () ^ "_tb" in
        let l2 = fresh_label () ^ "_fb" in
        gen_cond_code env code cond l1 l2 cont;
        Code.add_label code l1;
        gen_branch_code env code b1;
        Code.add_label code l2;
        gen_branch_code env code b2
    | Ast.ELet(letdefs, body) ->
        let l = fresh_label () ^ "_let" in
        gen_let_defs env code letdefs (fun () ->
            Code.add_instr code (Code.LDF l);
            Code.add_instr code (Code.AP (List.length letdefs));
            cont());
        Code.add_label code l;
        gen_tail_code (Env.extend env (List.map letdef_pos_name letdefs)) code body
    | Ast.ELetRec(letdefs, body) ->
        let l = fresh_label () ^ "_letrec" in
        Code.add_instr code (Code.DUM (List.length letdefs));
        let env' = Env.extend env (List.map letdef_pos_name letdefs) in
        gen_let_defs env' code letdefs (fun () ->
            Code.add_instr code (Code.LDF l);
            Code.add_instr code (Code.RAP (List.length letdefs));
            cont());
        Code.add_label code l;
        gen_tail_code env' code body
    | Ast.EPair(e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
            Code.add_instr code Code.CONS;
            cont ()))
    | Ast.EProj1 e ->
        gen_code env code e (fun () ->
            Code.add_instr code Code.CAR;
            cont ());
    | Ast.EProj2 e ->
        gen_code env code e (fun () ->
            Code.add_instr code Code.CDR;
            cont ());
    | Ast.EBinOp(Ast.BO_Eq, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.CEQ;
          cont()));
    | Ast.EBinOp(Ast.BO_Neq, e1, e2) ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.ENot
                { Ast.e_pos  = e.Ast.e_pos
                ; Ast.e_kind = Ast.EBinOp(Ast.BO_Eq, e1, e2)
                }
            } cont
    | Ast.EBinOp(Ast.BO_Le, e1, e2) ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.ENot
                { Ast.e_pos  = e.Ast.e_pos
                ; Ast.e_kind = Ast.EBinOp(Ast.BO_Gt, e1, e2)
                }
            } cont
    | Ast.EBinOp(Ast.BO_Lt, e1, e2) ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.ENot
                { Ast.e_pos  = e.Ast.e_pos
                ; Ast.e_kind = Ast.EBinOp(Ast.BO_Ge, e1, e2)
                }
            } cont
    | Ast.EBinOp(Ast.BO_Gt, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.CGT;
          cont()));
    | Ast.EBinOp(Ast.BO_Ge, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.CGTE;
          cont()));
    | Ast.EBinOp(Ast.BO_Add, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.ADD;
          cont()));
    | Ast.EBinOp(Ast.BO_Sub, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.SUB;
          cont()));
    | Ast.EBinOp(Ast.BO_Mul, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.MUL;
          cont()));
    | Ast.EBinOp(Ast.BO_Div, e1, e2) ->
        gen_code env code e1 (fun () ->
        gen_code env code e2 (fun () ->
          Code.add_instr code Code.DIV;
          cont()));
    | Ast.EOr(e1, e2) ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.EIf(e1, { Ast.e_pos = e.Ast.e_pos; Ast.e_kind = Ast.ENum 1l }, e2)
            } cont
    | Ast.EAnd(e1, e2) ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.EIf(e1, e2, { Ast.e_pos = e.Ast.e_pos; Ast.e_kind = Ast.ENum 0l })
            } cont
    | Ast.ENot e0 ->
        gen_code env code 
            { Ast.e_pos  = e.Ast.e_pos
            ; Ast.e_kind = Ast.EIf(e0,
                { Ast.e_pos  = e.Ast.e_pos
                ; Ast.e_kind = Ast.ENum 0l
                },
                { Ast.e_pos  = e.Ast.e_pos
                ; Ast.e_kind = Ast.ENum 1l
                })
            } cont
    | Ast.EAtom e ->
        gen_code env code e (fun () ->
            Code.add_instr code Code.ATOM;
            cont ());
    | Ast.EAssign(x, e0) ->
        gen_code env code e0 (fun () ->
            begin try match Env.lookup env x with
            | Env.Local(fr, fld) ->
                Code.add_instr code ~comment:x (Code.ST(fr, fld));
                Code.add_instr code (Code.LD(fr, fld));
                cont ()
            | Env.Global(x, _) ->
                Errors.error_p e.Ast.e_pos "Cannot write to global function `%s'." x
            with
            | Not_found ->
                Errors.error_p e.Ast.e_pos "`%s' is undefined." x
            end;
            cont ()
        )
    | Ast.ESeq(e1, e2) ->
        gen_code env code e1 (fun () ->
        Code.add_instr code (Code.LDF "@pop");
        Code.add_instr code (Code.AP 1);
        gen_code env code e2 cont)
    | Ast.EDebug e ->
        gen_code env code e (fun () ->
            Code.add_instr code Code.DBUG;
            Code.add_instr code (Code.LDC 0l);
            cont ());

and gen_tail_code env code e =
    match e.Ast.e_kind with
    | Ast.ECall(func, args) ->
        check_arg_cnt env e.Ast.e_pos func (List.length args);
        iter_gen_code env code args (fun () ->
        gen_code env code func (fun () ->
            Code.add_instr code (Code.TAP (List.length args))
        ))
    | Ast.EIf(cond, b1, b2) ->
        let l1 = fresh_label () ^ "_tb" in
        let l2 = fresh_label () ^ "_fb" in
        gen_cond_tail_code env code cond l1 l2;
        Code.add_label code l1;
        gen_tail_code env code b1;
        Code.add_label code l2;
        gen_tail_code env code b2
    | Ast.ELet(letdefs, body) ->
        let l = fresh_label () ^ "_let" in
        gen_let_defs env code letdefs (fun () ->
            Code.add_instr code (Code.LDF l);
            Code.add_instr code (Code.TAP (List.length letdefs)));
        Code.add_label code l;
        gen_tail_code (Env.extend env (List.map letdef_pos_name letdefs)) code body
    | Ast.ELetRec(letdefs, body) ->
        let l = fresh_label () ^ "_letrec" in
        Code.add_instr code (Code.DUM (List.length letdefs));
        let env' = Env.extend env (List.map letdef_pos_name letdefs) in
        gen_let_defs env' code letdefs (fun () ->
            Code.add_instr code (Code.LDF l);
            Code.add_instr code (Code.TRAP (List.length letdefs)));
        Code.add_label code l;
        gen_tail_code env' code body
    | Ast.ESeq(e1, e2) ->
        gen_code env code e1 (fun () ->
        Code.add_instr code (Code.LDF "@pop");
        Code.add_instr code (Code.AP 1);
        gen_tail_code env code e2)
    | _ ->
        gen_code env code e (fun () ->
            Code.add_instr code Code.RTN
        )

and iter_gen_code env code es cont =
    match es with
    | [] -> cont ()
    | e :: es -> gen_code env code e (fun () -> 
        iter_gen_code env code es cont)

and gen_cond_code env code e l1 l2 cont =
    match e.Ast.e_kind with
    | Ast.EOr(e1, e2) ->
        let l = fresh_label () ^ "_fbs" in
        gen_cond_code env code e1 l1 l cont;
        Code.add_label code l;
        gen_cond_tail_code env code e2 l1 l2
    | Ast.EAnd(e1, e2) ->
        let l = fresh_label () ^ "_tbs" in
        gen_cond_code env code e1 l l2 cont;
        Code.add_label code l;
        gen_cond_tail_code env code e2 l1 l2
    | Ast.ENot e0 ->
        gen_cond_code env code e0 l2 l1 cont
    | _ ->
        (* TODO: mozna jeszcze lepiej *)
        gen_code env code e (fun () ->
            Code.add_instr code (Code.SEL(l1, l2));
            cont())

and gen_cond_tail_code env code e l1 l2 =
    match e.Ast.e_kind with
    | Ast.EOr(e1, e2) ->
        let l = fresh_label () ^ "_fbs" in
        gen_cond_tail_code env code e1 l1 l;
        Code.add_label code l;
        gen_cond_tail_code env code e2 l1 l2
    | Ast.EAnd(e1, e2) ->
        let l = fresh_label () ^ "_tbs" in
        gen_cond_tail_code env code e1 l l2;
        Code.add_label code l;
        gen_cond_tail_code env code e2 l1 l2
    | Ast.ENot e0 ->
        gen_cond_tail_code env code e0 l2 l1
    | _ ->
        (* TODO: mozna jeszcze lepiej *)
        gen_code env code e (fun () ->
            Code.add_instr code (Code.TSEL(l1, l2)))

and gen_branch_code env code e =
    (* TODO: mozna lepiej *)
    gen_code env code e (fun () ->
        Code.add_instr code Code.JOIN)

and gen_let_defs env code letdefs cont =
    match letdefs with
    | [] -> cont ()
    | Ast.LD_Val(_, _, e) :: letdefs ->
        gen_code env code e (fun () ->
            gen_let_defs env code letdefs cont)
    | Ast.LD_Fun(_, name, args, body) :: letdefs ->
        let l = fresh_label () ^ "_locfun_" ^ name in
        Code.add_instr code (Code.LDF l);
        gen_let_defs env code letdefs cont;
        Code.add_label code l;
        gen_tail_code (Env.extend env args) code body

let gen_function env code tf =
    Code.add_label code tf.Ast.tf_name;
    gen_tail_code (Env.extend env tf.Ast.tf_args) code tf.Ast.tf_body
