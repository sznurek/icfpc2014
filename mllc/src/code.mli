(* File: code.mli *)

type label = string

type instr =
| LDC of Int32.t
| LD  of int * int
| ADD
| SUB
| MUL
| DIV
| CEQ
| CGT
| CGTE
| ATOM
| CONS
| CAR
| CDR
| SEL  of label * label
| JOIN
| LDF  of label
| AP   of int
| RTN 
| DUM  of int
| RAP  of int
| STOP
| TSEL of label * label
| TAP  of int
| TRAP of int
| ST   of int * int
| DBUG
| BRK

type code_elem =
| Instr of instr * string option
| Label of label

type t

val create     : unit -> t

val print_code : t -> unit

val add_instr : t -> ?comment:string -> instr -> unit
val add_label : t -> label -> unit
