(* File: env.ml *)

module StrMap = Map.Make(String)

type t =
| Top   of Ast.top_func StrMap.t
| Frame of int StrMap.t * t

type loc =
| Global of string * int
| Local  of int * int

let rec create_top_set acc top_names =
    match top_names with
    | [] -> acc
    | tf :: top_names ->
        if StrMap.mem tf.Ast.tf_name acc then begin
            Errors.error_p tf.Ast.tf_pos "Redefinition of top level function `%s'." tf.Ast.tf_name;
            Errors.note_p (StrMap.find tf.Ast.tf_name acc).Ast.tf_pos "Here is a previous definition.";
            create_top_set acc top_names
        end else
            create_top_set (StrMap.add tf.Ast.tf_name tf acc) top_names

let rec create_frame_map acc names =
    match names with
    | [] -> acc
    | (pos, name) :: names ->
        if StrMap.mem name acc then begin
            Errors.error_p pos "Redefinition of `%s'." name;
            create_frame_map acc names
        end else
            create_frame_map (StrMap.add name (StrMap.cardinal acc) acc) names

let create top_names =
    Top (create_top_set StrMap.empty top_names)

let extend env names =
    Frame(create_frame_map StrMap.empty names, env)

let rec lookup env name =
    match env with
    | Top s ->
        if StrMap.mem name s then
            Global(name, List.length (StrMap.find name s).Ast.tf_args)
        else
            raise Not_found
    | Frame(fr, env) ->
        if StrMap.mem name fr then
            Local(0, StrMap.find name fr)
        else begin match lookup env name with
        | Local(fr, fld) -> Local(fr + 1, fld)
        | glb -> glb
        end
