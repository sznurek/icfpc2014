(* File: parser.mli *)

val parse : string -> string list * Ast.top_func list
