open Ast

let rec uniq = function
    | []      -> []
    | x :: xs -> x :: uniq (List.filter (fun y -> y <> x) xs)

let (@@) a b = uniq (a @ b)

type knowledge =
    { bound_variables       : string list
    ; used_top_definitions  : string list
    }

let zero_knwowledge = { bound_variables = []; used_top_definitions = ["main"] } 

let join_knowledge kw1 kw2 =
    { bound_variables = kw1.bound_variables @@ kw2.bound_variables
    ; used_top_definitions = kw1.used_top_definitions @@ kw2.used_top_definitions
    }

let fold_join_knowledge xs =
    List.fold_left join_knowledge zero_knwowledge xs

let rec unhask_expr_kind_list xs = List.map (fun e -> e.e_kind) xs

let get_name_of_letdef = function
    | LD_Val (_, name, _) -> name
    | LD_Fun (_, name, _, _) -> name


let rec compute_knowledge_from_expr_kind knowledge = function
    | EVar name ->
        if List.mem name knowledge.bound_variables then
            knowledge
        else
            {knowledge with used_top_definitions = name :: knowledge.used_top_definitions
            }

    | ENum _ ->
        knowledge

    | EFun (args, body) ->
        let bound_variables = List.map snd args in
        let inside_knowledge =
            { knowledge with bound_variables = bound_variables @@ knowledge.bound_variables }
            in
        let computed_knowledge = compute_joined_knowledge_from_expr_list inside_knowledge [body]
        in
        {computed_knowledge with bound_variables = knowledge.bound_variables }

    | ECall (e1,es2) ->
        let kwes2 = compute_joined_knowledge_from_expr_list knowledge es2 in
        let kw1 = compute_joined_knowledge_from_expr_kind_list knowledge [e1.e_kind] in
        join_knowledge kw1 kwes2

    | EIf (e1,e2,e3) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2;e3]

    | ELet (defs, inexpr) ->
        let defs_kw = fold_join_knowledge (knowledge :: List.map (compute_knowledge_from_letdef knowledge) defs) in
        let bound_names = List.map get_name_of_letdef defs in
        let inside_knowledge = {defs_kw with bound_variables = bound_names @@ defs_kw.bound_variables} in
        let computed_knowledge = compute_joined_knowledge_from_expr_list defs_kw [inexpr] in
        {computed_knowledge with bound_variables = knowledge.bound_variables}

    | ELetRec (defs,inexpr) ->
        (* XXX: check itLD_Var also? *)
        let bound_names = List.map get_name_of_letdef defs in
        let inside_knowledge = {knowledge with bound_variables = bound_names @@ knowledge.bound_variables} in
        let defs_kw = fold_join_knowledge (inside_knowledge :: List.map (compute_knowledge_from_letdef inside_knowledge) defs) in
        let computed_knowledge = compute_joined_knowledge_from_expr_list defs_kw [inexpr] in
        {computed_knowledge with bound_variables = knowledge.bound_variables}

    | EPair (e1,e2) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2]

    | EProj1 expr1 ->
        compute_knowledge_from_expr_kind knowledge expr1.e_kind

    | EProj2 expr1 ->
        compute_knowledge_from_expr_kind knowledge expr1.e_kind

    | EBinOp (_, e1, e2) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2]

    | EOr (e1, e2) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2]

    | EAnd (e1, e2) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2]

    | ENot e1 ->
        compute_joined_knowledge_from_expr_list knowledge [e1]

    | EAtom e1 ->
        compute_joined_knowledge_from_expr_list knowledge [e1]

    | EAssign (_, e1) ->
        compute_joined_knowledge_from_expr_list knowledge [e1]

    | ESeq (e1, e2) ->
        compute_joined_knowledge_from_expr_list knowledge [e1;e2]

    | EDebug e1 ->
        compute_joined_knowledge_from_expr_list knowledge [e1]

and compute_knowledge_from_letdef knowledge = function
    | LD_Val (_, letname, letbody) ->
        compute_joined_knowledge_from_expr_list knowledge [letbody]

    | LD_Fun (_, letname, letargs, letbody) ->
        let inside_knowledge =
            { knowledge with
                bound_variables = letname :: knowledge.bound_variables
            } in
        let computed_knowledge = compute_joined_knowledge_from_expr_list inside_knowledge [letbody] in
        {computed_knowledge with
        bound_variables = knowledge.bound_variables
        }

and compute_joined_knowledge_from_expr_kind_list knowledge xs =
    List.fold_left join_knowledge knowledge (List.map (compute_knowledge_from_expr_kind knowledge) xs)

and compute_joined_knowledge_from_expr_list knowledge xs =
    compute_joined_knowledge_from_expr_kind_list knowledge (unhask_expr_kind_list xs)

let compute_knowledge_from_top_func top_func =
    let knowledge =
        { bound_variables = top_func.tf_name :: List.map snd top_func.tf_args
        ; used_top_definitions = []
        } in
    
    let knowledge = compute_knowledge_from_expr_kind knowledge top_func.tf_body.e_kind in
    {knowledge with bound_variables = []
    (*; used_top_definitions = List.fold_left (fun result elem -> List.filter (fun a -> a = elem) result) knowledge.used_top_definitions (List.map snd top_func.tf_args)*)
    }

let compute_knowledge xs =
    List.fold_left join_knowledge zero_knwowledge (List.map compute_knowledge_from_top_func xs)

let translate_defs xs = 
    let used_top_definitions = (compute_knowledge xs).used_top_definitions in
    let isTopDefUsed top_def = List.mem top_def.tf_name used_top_definitions in
    List.filter isTopDefUsed xs
