(* File.env *)

type t

type loc =
| Global of string * int
| Local  of int * int

val create : Ast.top_func list -> t
val extend : t -> (Lexing.position * string) list -> t

val lookup : t -> string -> loc
