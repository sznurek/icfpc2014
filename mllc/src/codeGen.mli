(* File: codeGen.mli *)

val gen_preamble : Code.t -> unit
val gen_function : Env.t -> Code.t -> Ast.top_func -> unit
