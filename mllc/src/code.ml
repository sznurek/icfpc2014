(* File: code.ml *)

type label = string

type instr =
| LDC of Int32.t
| LD  of int * int
| ADD
| SUB
| MUL
| DIV
| CEQ
| CGT
| CGTE
| ATOM
| CONS
| CAR
| CDR
| SEL  of label * label
| JOIN
| LDF  of label
| AP   of int
| RTN 
| DUM  of int
| RAP  of int
| STOP
| TSEL of label * label
| TAP  of int
| TRAP of int
| ST   of int * int
| DBUG
| BRK

type code_elem =
| Instr of instr * string option
| Label of label

type t = code_elem Queue.t

let create () = Queue.create ()

let instr_to_string = function
    | LDC const       -> Printf.sprintf "LDC %ld" const
    | LD (frame, pos) -> Printf.sprintf "LD %d %d" frame pos
    | ADD  -> "ADD"
    | SUB  -> "SUB"
    | MUL  -> "MUL"
    | DIV  -> "DIV"
    | CEQ  -> "CEQ"
    | CGT  -> "CGT"
    | CGTE -> "CGTE"
    | ATOM -> "ATOM"
    | CONS -> "CONS"
    | CAR  -> "CAR"
    | CDR  -> "CDR"
    | SEL (l1, l2) -> Printf.sprintf "SEL %s %s" l1 l2
    | JOIN  -> "JOIN"
    | LDF f -> Printf.sprintf "LDF %s" f
    | AP n  -> Printf.sprintf "AP %d" n
    | RTN   -> "RTN"
    | DUM n -> Printf.sprintf "DUM %d" n
    | RAP n -> Printf.sprintf "RAP %d" n
    | STOP  -> "STOP"
    | TSEL (l1, l2) -> Printf.sprintf "TSEL %s %s" l1 l2
    | TAP n  -> Printf.sprintf "TAP %d" n
    | TRAP n -> Printf.sprintf "TRAP %d" n
    | ST (x, y) -> Printf.sprintf "ST %d %d" x y
    | DBUG -> "DBUG"
    | BRK -> "BRK"

let print_code_elem elem =
    match elem with
    | Instr(instr, com) -> Printf.printf "    %s%s\n" (instr_to_string instr)
        (match com with None -> "" | Some c -> "   ; " ^ c)
    | Label label -> Printf.printf "%s:\n" label

let print_code code =
    Queue.iter print_code_elem code

let add_instr code ?comment instr =
    Queue.push (Instr(instr, comment)) code
let add_label code label =
    Queue.push (Label label) code
