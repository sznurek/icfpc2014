/* File: parserGen.mly */
%{
let mk_expr n kind =
    { Ast.e_pos  = Parsing.rhs_start_pos n
    ; Ast.e_kind = kind
    }

let change_pos n e = { e with Ast.e_pos = Parsing.rhs_start_pos n }

let rec list_to_cons_list n = function
  | []      -> mk_expr n (Ast.ENum 0l)
  | x :: xs -> 
    { Ast.e_pos  = x.Ast.e_pos
    ; Ast.e_kind = Ast.EPair(x, list_to_cons_list n xs)
    }
%}

%token <string> ID
%token <string> PATH
%token <Int32.t> NUM
%token EOF
%token BR_OPN BR_CLS CBR_OPN CBR_CLS SBR_OPN SBR_CLS
%token AND ARROW ASSIGN ASTERISK COMMA EQ LT LE GT GE NEQ MINUS OR PLUS SEMI 
%token SLASH 
%token PROJ_1 PROJ_2 PROJ_BAD
%token KW_AND KW_ATOM KW_BEGIN KW_DEBUG KW_DO KW_DONE KW_ELIF KW_ELSE KW_END 
%token KW_FUN KW_IF KW_IN KW_LET KW_LETREC KW_NOT KW_REQUIRE KW_THEN

%start main
%type <string list * Ast.top_func list> main

%left SEMI
%right COMMA
%right ASSIGN
%left OR
%left AND
%left KW_NOT
%left EQ NEQ LT LE GT GE
%left PLUS MINUS
%left ASTERISK SLASH

%%

/* ========================================================================= */

main
: require_list top_func_list_rev EOF { (List.rev $1, List.rev $2) }
;

require_list
: /* empty */ { [] }
| require_list KW_REQUIRE PATH { $3 :: $1 }
;

top_func_list_rev
: /* empty */ { [] }
| top_func_list_rev top_func { $2 :: $1 }
;

top_func
: KW_FUN ID fun_def_args EQ expr KW_END {
		{ Ast.tf_pos  = Parsing.rhs_start_pos 2
        ; Ast.tf_name = $2
		; Ast.tf_args = $3
		; Ast.tf_body = $5
		}
	}
;

/* ========================================================================= */

expr
: expr SEMI  expr { mk_expr 2 (Ast.ESeq($1,  $3)) }
| expr COMMA expr { mk_expr 2 (Ast.EPair($1, $3)) }
| expr_nc { $1 }
;

expr_nc
: ID  ASSIGN expr_nc { mk_expr 2 (Ast.EAssign($1, $3)) }
| expr_nc OR    expr_nc { mk_expr 2 (Ast.EOr($1, $3)) }
| expr_nc AND   expr_nc { mk_expr 2 (Ast.EAnd($1, $3)) }
| KW_NOT expr_nc { mk_expr 1 (Ast.ENot $2) }
| expr_nc EQ    expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Eq,  $1, $3)) }
| expr_nc NEQ   expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Neq, $1, $3)) }
| expr_nc LE    expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Le,  $1, $3)) }
| expr_nc LT    expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Lt,  $1, $3)) }
| expr_nc GE    expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Ge,  $1, $3)) }
| expr_nc GT    expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Gt,  $1, $3)) }
| expr_nc PLUS  expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Add, $1, $3)) }
| expr_nc MINUS expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Sub, $1, $3)) }
| expr_nc ASTERISK expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Mul, $1, $3)) }
| expr_nc SLASH expr_nc { mk_expr 2 (Ast.EBinOp(Ast.BO_Div, $1, $3)) }
| expr_atom { $1 }
;

expr_atom
: BR_OPN   expr BR_CLS { change_pos 1 $2 }
| KW_BEGIN expr KW_END { change_pos 1 $2 }
| ID                   { mk_expr 1 (Ast.EVar $1) }
| NUM                  { mk_expr 1 (Ast.ENum $1) }
| SBR_OPN SBR_CLS { list_to_cons_list 2 [] }
| SBR_OPN expr_nc expr_nc_list_rev_comma_sep SBR_CLS {
    list_to_cons_list 4 ($2 :: List.rev $3)
  }
| KW_LET letdef letdef_list_rev let_expr_cont {
		mk_expr 1 (Ast.ELet($2 :: List.rev $3, $4))
	}
| KW_LETREC letdef letdef_list_rev let_expr_cont {
		mk_expr 1 (Ast.ELetRec($2 :: List.rev $3, $4))
	}
| KW_IF expr KW_THEN expr if_expr_cont {
		mk_expr 1 (Ast.EIf($2, $4, $5))
	}
| KW_FUN fun_def_args ARROW expr KW_END {
		mk_expr 1 (Ast.EFun($2, $4))
	}
| expr_atom PROJ_1 { mk_expr 1 (Ast.EProj1 $1) }
| expr_atom PROJ_2 { mk_expr 1 (Ast.EProj2 $1) }
| expr_atom BR_OPN BR_CLS { mk_expr 2 (Ast.ECall($1, [])) }
| expr_atom BR_OPN expr_nc expr_nc_list_rev_comma_sep BR_CLS {
		mk_expr 2 (Ast.ECall($1, $3 :: List.rev $4))
	}
| KW_ATOM BR_OPN expr BR_CLS { mk_expr 1 (Ast.EAtom $3) }
| KW_DEBUG BR_OPN expr BR_CLS { mk_expr 1 (Ast.EDebug $3) }
;

expr_nc_list_rev_comma_sep
: /* empty */ { [] }
| expr_nc_list_rev_comma_sep COMMA expr_nc { $3 :: $1 }
;

/* ========================================================================= */

letdef
: ID EQ expr { Ast.LD_Val(Parsing.rhs_start_pos 1, $1, $3) }
| ID fun_def_args EQ expr { Ast.LD_Fun(Parsing.rhs_start_pos 1, $1, $2, $4) }
;

letdef_list_rev
: /* empty */ { [] }
| letdef_list_rev KW_AND letdef { $3 :: $1 }
;

/* ========================================================================= */

if_expr_cont
: KW_ELSE expr KW_END { $2 }
| KW_ELIF expr KW_THEN expr if_expr_cont { mk_expr 1 (Ast.EIf($2, $4, $5)) }
;

let_expr_cont
: KW_IN expr KW_END { $2 }
| KW_AND KW_LET letdef letdef_list_rev let_expr_cont {
		mk_expr 2 (Ast.ELet($3 :: List.rev $4, $5))
	}
| KW_AND KW_LETREC letdef letdef_list_rev let_expr_cont {
		mk_expr 2 (Ast.ELetRec($3 :: List.rev $4, $5))
	}
| KW_AND KW_IF expr KW_THEN expr if_expr_cont { 
        mk_expr 2 (Ast.EIf($3, $5, $6)) 
    }
;

/* ========================================================================= */

fun_def_args
: BR_OPN BR_CLS { [] }
| BR_OPN ID ids_comma_sep_rev BR_CLS { 
        (Parsing.rhs_start_pos 2, $2) :: List.rev $3 
    }
;

ids_comma_sep_rev
: /* empty */ { [] }
| ids_comma_sep_rev COMMA ID { (Parsing.rhs_start_pos 3, $3) :: $1 }
;
