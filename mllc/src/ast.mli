(* File: ast.mli *)

type binop =
| BO_Eq
| BO_Neq
| BO_Le
| BO_Lt
| BO_Ge
| BO_Gt
| BO_Add
| BO_Sub
| BO_Mul
| BO_Div

type expr =
    { e_pos  : Lexing.position
    ; e_kind : expr_kind
    }
and expr_kind =
| EVar    of string
| ENum    of Int32.t
| EFun    of (Lexing.position * string) list * expr
| ECall   of expr * expr list
| EIf     of expr * expr * expr
| ELet    of letdef list * expr
| ELetRec of letdef list * expr
| EPair   of expr * expr
| EProj1  of expr
| EProj2  of expr
| EBinOp  of binop * expr * expr
| EOr     of expr * expr
| EAnd    of expr * expr
| ENot    of expr
| EAtom   of expr
| EAssign of string * expr
| ESeq    of expr * expr
| EDebug  of expr
and letdef =
| LD_Val of Lexing.position * string * expr
| LD_Fun of Lexing.position * string * (Lexing.position * string) list * expr

type top_func =
	{ tf_pos  : Lexing.position
    ; tf_name : string
	; tf_args : (Lexing.position * string) list
	; tf_body : expr
	}
