(* File: parser.ml *)

let with_in_channel fname foo =
	let chan = 
		try open_in fname with Sys_error msg ->
			Errors.error "Can not open file `%s' (%s)" fname msg;
			raise Errors.Fatal_error
	in try
		let result = foo chan in
		close_in chan;
		result
	with
	| er ->
		close_in chan;
		raise er

let parse fname =
	with_in_channel fname (fun chan ->
		let lexbuf = Lexing.from_channel chan in
		lexbuf.Lexing.lex_curr_p <-
			{ lexbuf.Lexing.lex_curr_p with
			  Lexing.pos_fname = fname
			};
		try ParserGen.main Lexer.token lexbuf with
		| Parsing.Parse_error ->
			Errors.error_p lexbuf.Lexing.lex_start_p
				"Syntax error (Unexpected token `%s')."
					(Lexing.lexeme lexbuf);
			raise Errors.Fatal_error
		| Lexer.Invalid_character c ->
			Errors.error_p lexbuf.Lexing.lex_start_p
				"Invalid character '%s' (0x%02X)."
				(Char.escaped c) (Char.code c);
			raise Errors.Fatal_error
		| Lexer.Invalid_number x ->
			Errors.error_p lexbuf.Lexing.lex_start_p
				"Invalid number literal `%s'." x;
			raise Errors.Fatal_error
	)
