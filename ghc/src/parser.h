#pragma once
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <map>

#include "types.h"

class ParsingError : public std::runtime_error {
    public:
        ParsingError(std::string s) : std::runtime_error(s) {}
};

class Parser {
    public:
        Parser() : instructionNumber(0) {}
        std::vector<Instruction> parse(std::istream& input);

    protected:
        byte instructionNumber;
        std::map<std::string, byte> labelMapping;

        std::string stripIndirect(std::string s);
        byte registerNameToNumber(std::string s);
        byte parseConstant(std::string s);
        std::vector<std::string> splitArgs(std::string s, int nArgs);
        Argument parseArgument(std::string s);
        void gatherLabels(std::vector<std::string>& lines);
        Instruction parseInstruction(std::string line);
};

Argument parseArgument(std::string s);
Instruction parseInstruction(std::string line);
std::vector<Instruction> parse(std::istream& input);

