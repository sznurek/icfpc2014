#include "types.h"
#include "game.h"
#include <iostream>
#include <sstream>

static byte getValue(GhcState& state, Argument& arg) {
    byte value;
    if(arg.type == Constant)
        value = arg.value;
    else
        value = state.registers[arg.value];

    if(arg.mode == Indirect)
        return state.data[value];
    else
        return value;
}

static void setValue(GhcState& state, Argument& dest, byte val) {
    if(dest.type == Constant && dest.mode == Direct)
        throw GhcError("Destination agrument cannot be a constant.");

    if(dest.type == Constant)
        state.data[dest.value] = val;
    else if(dest.mode == Indirect)
        state.data[state.registers[dest.value]] = val;
    else
        state.registers[dest.value] = val;
}

static void handleInterrupt(GhcState& state, int n, WorldState& worldState) {
    byte ghostNum, x, y;

    switch(n) {
    case 0:
        if(state.registers[1] > 3)
            throw GhcError("Invalid direction.");

        worldState.persistent.direction = state.registers[1];
        break;
    case 1:
        state.registers[1] = worldState.current.lambdaPosition.x;
        state.registers[2] = worldState.current.lambdaPosition.y;
        break;
    case 2:
        throw GhcError("INT not implemented.");
    case 3:
        state.registers[1] = worldState.persistent.ghostNumber;
        break;
    case 4:
        ghostNum = state.registers[1];
        if(ghostNum >= 0 && ghostNum < worldState.persistent.ghostInitialPositions.size()) {
            state.registers[1] = worldState.persistent.ghostInitialPositions[ghostNum].x;
            state.registers[2] = worldState.persistent.ghostInitialPositions[ghostNum].y;
        }
        break;
    case 5:
        ghostNum = state.registers[1];
        if(ghostNum >= 0 && ghostNum < worldState.current.ghosts.size()) {
            state.registers[1] = worldState.current.ghosts[ghostNum].position.x;
            state.registers[2] = worldState.current.ghosts[ghostNum].position.y;
        }
        break;
    case 6:
        ghostNum = state.registers[1];
        if(ghostNum >= 0 && ghostNum < worldState.current.ghosts.size()) {
            state.registers[1] = worldState.current.ghosts[ghostNum].vitality;
            state.registers[2] = worldState.current.ghosts[ghostNum].direction;
        }
        break;
    case 7:
        x = state.registers[1];
        y = state.registers[2];

        if(x >= worldState.current.map.width() || y >= worldState.current.map.height()) {
            state.registers[1] = 0;
        } else {
            state.registers[1] = worldState.current.map.fields[y][x];
        }

        break;
    case 8:
        break; // NOOP
    default:
        throw GhcError("INT not implemented.");
    }
}

bool ghcStep(GhcState& state, WorldState& worldState) {
    bool stillOperating = true;

    Instruction& instr = state.code[state.registers[0]];
    byte lastPc = state.registers[0];

    /*
    std::cerr << "PC: " << (int) lastPc << " " << instr << std::endl;
    std::cerr << "H: " << (int)state.registers[8] << std::endl;
    for(int i = 255; i >= 250; i--)
        std::cerr << "MEM[" << i << "] = " << (int)state.data[i] << std::endl;
    */

    switch(instr.op) {
    case MOV:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[1]));
        break;
    case INC:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) + 1);
        break;
    case DEC:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) - 1);
        break;
    case ADD:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) + getValue(state, instr.arguments[1]));
        break;
    case SUB:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) - getValue(state, instr.arguments[1]));
        break;
    case MUL:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) * getValue(state, instr.arguments[1]));
        break;
    case DIV:
        if(getValue(state, instr.arguments[1]) == 0)
            throw GhcError("Division by zero.");

        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) / getValue(state, instr.arguments[1]));
        break;
    case AND:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) & getValue(state, instr.arguments[1]));
        break;
    case OR:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) | getValue(state, instr.arguments[1]));
        break;
    case XOR:
        setValue(state, instr.arguments[0], getValue(state, instr.arguments[0]) ^ getValue(state, instr.arguments[1]));
        break;
    case JLT:
        if(getValue(state, instr.arguments[1]) < getValue(state, instr.arguments[2]))
            state.registers[0] = getValue(state, instr.arguments[0]);
        break;
    case JEQ:
        if(getValue(state, instr.arguments[1]) == getValue(state, instr.arguments[2]))
            state.registers[0] = getValue(state, instr.arguments[0]);
        break;
    case JGT:
        if(getValue(state, instr.arguments[1]) > getValue(state, instr.arguments[2]))
            state.registers[0] = getValue(state, instr.arguments[0]);
        break;
    case INT:
        handleInterrupt(state, getValue(state, instr.arguments[0]), worldState);
        break;
    case HLT:
        stillOperating = false;
        break;
    default:
        std::stringstream ss;
        ss << "Invalid op code: " << instr << " (PC: " << (int)state.registers[0] << ")";
        throw GhcError(ss.str());
        break;
    }

    state.steps++;
    if(lastPc == state.registers[0])
        state.registers[0]++;

    return stillOperating;
}

