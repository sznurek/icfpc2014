#include "game.h"

#include <iostream>

Map parseMap(std::istream& is) {
    int width, height;
    std::vector<std::vector<unsigned char> > fields;

    is >> width >> height;
    fields.resize(height);

    for(int i = 0; i < height; i++) {
        fields[i].resize(width);

        std::string row;
        is >> row;

        for(int j = 0; j < width; j++) {
            fields[i][j] = row[j] - '0';
        }
    }

    return Map(fields);
}

Position parseLambdaMan(std::istream& is) {
    int x, y, ignore;

    is >> ignore >> x >> y >> ignore >> ignore >> ignore;

    return Position(x, y);
}

GhostState parseGhostState(std::istream& is) {
    int vitality, x, y, direction;

    is >> vitality >> x >> y >> direction;
    return GhostState(Position(x, y), (unsigned char)vitality, (unsigned char)direction);
}

std::vector<GhostState> parseGhostStates(std::istream& is) {
    int n;
    std::vector<GhostState> result;

    is >> n;
    for(int i = 0; i < n; i++) {
        result.push_back(parseGhostState(is));
    }
    return result;
}

CurrentWorldState parseCurrentWorldState(std::istream& is) {
    Map map = parseMap(is);
    Position lambdaMan = parseLambdaMan(is);
    std::vector<GhostState> ghosts = parseGhostStates(is);

    int ignore;
	is >> ignore; // Fruit countdown;
    return CurrentWorldState(lambdaMan, ghosts, map);
}

PersistentWorldState parsePersistentWorldState(std::istream& is) {
    (void)parseMap(is);
    (void)parseLambdaMan(is);

    std::vector<GhostState> ghosts = parseGhostStates(is);
    std::vector<Position> result;

    for(auto& ghost : ghosts) {
        result.push_back(ghost.position);
    }

    int ignore;
	is >> ignore; // Fruit countdown;

    return PersistentWorldState(result);
}

