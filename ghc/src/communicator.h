#pragma once

#include "types.h"
#include "game.h"

#include <istream>
#include <ostream>

class Communicator {
    public:
        Communicator(byte ghostNumber, GhcState ghcState)
            : ghcState(ghcState), ghostNumber(ghostNumber) {}
        void loop(std::istream& is, std::ostream& os);
    
    protected:
        GhcState ghcState;
        WorldState worldState;
        byte ghostNumber;
};

