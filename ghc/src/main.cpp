#include <iostream>
#include <fstream>
#include <cstdlib>
#include "parser.h"
#include "interpreter.h"
#include "communicator.h"

int main(int argc, char** argv) {
    if(argc > 4) {
        std::cerr << "Usage: " << argv[0] << " [program-file] [ghost-number]\n";
        return 1;
    }

    std::cin.exceptions ( std::ifstream::failbit | std::ifstream::badbit );

    Parser parser;
    std::vector<Instruction> instructions;

    if(argc == 1) {
        instructions = parser.parse(std::cin);
    } else {
        std::ifstream input;
        input.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
        input.open(argv[1]);
        instructions = parser.parse(input);
    }

    if(argc < 3) {
        for(auto& instr : instructions) {
            std::cout << instr << std::endl;
        }
    } else {
        int ghostNumber = atoi(argv[2]);

        GhcState state(instructions);
        Communicator comm((byte)ghostNumber, state);

        comm.loop(std::cin, std::cout);
    }

    return 0;
}

