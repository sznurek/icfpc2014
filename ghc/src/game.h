#pragma once
#include <istream>
#include <vector>
#include "types.h"

struct Position {
    Position(unsigned char x=0, unsigned char y=0) : x(x), y(y) {}
    unsigned char x, y;
};

struct GhostState {
    GhostState() {}
    GhostState(Position p=Position(), unsigned char vitality=0, unsigned char direction=0)
        : position(p), vitality(vitality), direction(direction) {}
    Position position;
    unsigned char vitality;
    unsigned char direction;
};

struct Map {
    Map() {}
    Map(std::vector<std::vector<unsigned char> > fields) : fields(fields) {}

    unsigned char width() const {
        return fields[0].size();
    };

    unsigned char height() const {
        return fields.size();
    }

    std::vector<std::vector<unsigned char> > fields;
};

struct PersistentWorldState {
    PersistentWorldState() {}
    PersistentWorldState(std::vector<Position> ghostInitialPositions)
        : ghostInitialPositions(ghostInitialPositions) {}

    unsigned char ghostNumber;
    unsigned char direction;
    std::vector<Position> ghostInitialPositions;
};

struct CurrentWorldState {
    CurrentWorldState() {}
    CurrentWorldState(Position lambdaPosition, std::vector<GhostState> ghosts, Map map)
        : lambdaPosition(lambdaPosition), ghosts(ghosts), map(map) {}

    Position lambdaPosition;
    std::vector<GhostState> ghosts;
    Map map;
};

struct WorldState {
    WorldState() {}
    WorldState(PersistentWorldState persistent, CurrentWorldState current)
        : persistent(persistent), current(current) {}

    PersistentWorldState persistent;
    CurrentWorldState current;
};

CurrentWorldState parseCurrentWorldState(std::istream& is);
PersistentWorldState parsePersistentWorldState(std::istream& is);

