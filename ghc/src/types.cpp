#include "types.h"

#include <iostream>

std::ostream& operator<<(std::ostream& os, OpCode op) {
    for(auto& opDesc : OPCODES) {
        if(opDesc.op != op)
            continue;

        os << opDesc.name;
        return os;
    }

    os << "[INVALID]";
    return os;
}

std::ostream& operator<<(std::ostream& os, AddressingMode mode) {
    if(mode == Direct) {
        os << "[direct]";
    } else {
        os << "[indirect]";
    }

    return os;
}

std::ostream& operator<<(std::ostream& os, ArgumentType type) {
    if(type == Register) {
        os << "[register]";
    } else {
        os << "[address]";
    }

    return os;
}

std::ostream& operator<<(std::ostream& os, Argument arg) {
    if(arg.type == NotUsed) {
        os << "NU";
        return os;
    }

    if(arg.mode == Indirect)
        os << "[";

    if(arg.type == Constant) {
        os << (int)arg.value;
    } else if(arg.value == 0) {
        os << "PC";
    } else {
        os << (char)('A' + (arg.value - 1));
    }

    if(arg.mode == Indirect)
        os << "]";

    return os;
}

std::ostream& operator<<(std::ostream& os, Instruction instr) {
    os << instr.op << " ";

    for(int i = 0; i < 3; i++) {
        if(instr.arguments[i].type == NotUsed)
            break;

        if(i != 0)
            os << ", ";

        os << instr.arguments[i];
    }

    return os;
}

