#pragma once
#include <string>
#include <stdexcept>
#include <vector>

enum OpCode {
    INV,
    MOV,
    INC,
    DEC,
    ADD,
    SUB,
    MUL,
    DIV,
    AND,
    OR,
    XOR,
    JLT,
    JEQ,
    JGT,
    INT,
    HLT
};

struct {
    OpCode op;
    std::string name;
    int argumentsNumber;
} OPCODES[] = {
    {MOV, "MOV", 2},
    {INC, "INC", 1},
    {DEC, "DEC", 1},
    {ADD, "ADD", 2},
    {SUB, "SUB", 2},
    {MUL, "MUL", 2},
    {DIV, "DIV", 2},
    {AND, "AND", 2},
    {OR,  "OR",  2},
    {XOR, "XOR", 2},
    {JLT, "JLT", 3},
    {JEQ, "JEQ", 3},
    {JGT, "JGT", 3},
    {INT, "INT", 1},
    {HLT, "HLT", 0},
};

constexpr int OPS_NUMBER = sizeof(OPCODES) / sizeof(OPCODES[0]);

typedef unsigned char byte;

enum ArgumentType {
    NotUsed,
    Constant,
    Register
};

enum AddressingMode {
    Direct,
    Indirect
};

class GhcError : public std::runtime_error {
    public:
        GhcError(std::string s) : std::runtime_error(s) {}
};

const int DATA_CELLS = 256;
const int CODE_CELLS = 256;
const int EXECUTION_CYCLES = 1024;
const int REGISTERS_NUMBER = 8;

// Register number 0 is PC, all other registers are numbered from 1.
struct Argument {
    Argument(byte value=0, ArgumentType type=NotUsed, AddressingMode mode=Direct) : value(value), type(type), mode(mode) {}
    byte value;
    ArgumentType type;
    AddressingMode mode;

    bool isValid() const {
        return type == Constant || (type == Register && value < REGISTERS_NUMBER);
    }
};

struct Instruction {
    Instruction(OpCode op=INV) : op(op) {}
    OpCode op;
    Argument arguments[3];
};

struct GhcState {
    GhcState(std::vector<Instruction>& instructions) : code(instructions), steps(0) {
        for(int i = 0; i < DATA_CELLS; i++)
            data[i] = 0;

        for(int i = 0; i <= REGISTERS_NUMBER; i++)
            registers[i] = 0;

        code.resize(CODE_CELLS);
    }

    void nextTick() {
        registers[0] = 0;
        steps = 0;
    }

    byte data[DATA_CELLS];
    std::vector<Instruction> code;
    byte registers[REGISTERS_NUMBER+1];
    int steps;
};

std::ostream& operator<<(std::ostream& os, OpCode op);
std::ostream& operator<<(std::ostream& os, AddressingMode mode);
std::ostream& operator<<(std::ostream& os, ArgumentType type);
std::ostream& operator<<(std::ostream& os, Argument arg);
std::ostream& operator<<(std::ostream& os, Instruction instr);

