#include <utility>
#include <cctype>
#include <algorithm>

#include <boost/algorithm/string.hpp>

#include "parser.h"
#include "types.h"

static std::string trim(std::string s) {
    int pos = 0;
    while(pos < s.length() && isspace(s[pos]))
        pos++;

    int rpos = s.length() - 1;
    while(rpos > 0 && isspace(s[rpos]))
        rpos--;

    return s.substr(pos, rpos - pos + 1);
}

static std::string restOfStream(std::stringstream& ss) {
    return trim(ss.str().substr(ss.tellg(), ss.str().length() - ss.tellg()));
}

std::string Parser::stripIndirect(std::string s) {
    if(s.length() < 3)
        throw ParsingError("Invalid argument (trying to skip []).");

    return s.substr(1, s.length() - 2);
}

byte Parser::registerNameToNumber(std::string s) {
    if(s == "PC")
        return 0;

    if(s.size() > 1 || s[0] < 'A' || s[0] > ('A' + REGISTERS_NUMBER))
        throw ParsingError("Invalid register");

    return (int)s[0] - 'A' + 1;
}

byte Parser::parseConstant(std::string s) {
    int n;
    std::stringstream ss(s);

    ss >> n;
    if(ss.fail())
        throw ParsingError("Cannot read constant/address.");

    return (byte)n;
}

std::vector<std::string> Parser::splitArgs(std::string s, int nArgs) {
    std::vector<std::string> args;
    if(nArgs == 0)
        return args;

    boost::split(args, s, boost::is_any_of(", \n\t"), boost::token_compress_on);

    if(args.size() < nArgs) {
        throw ParsingError("Invalid number of arguments " + s);
    }

    return args;
}

Argument Parser::parseArgument(std::string s) {
    byte value;
    ArgumentType type;
    AddressingMode mode = Direct;

    if(s[0] == '[') {
        mode = Indirect;
        s = stripIndirect(s);
    }

    if(isalpha(s[0])) {
        if(mode == Direct && s[s.size() - 1] == ':') {
            type = Constant;
            value = labelMapping[s];
        } else {
            type = Register;
            value = registerNameToNumber(s);
        }
    } else {
        type = Constant;
        value = parseConstant(s);
    }

    return Argument(value, type, mode);
}

// Assume that line is non-empty.
Instruction Parser::parseInstruction(std::string line) {
    std::stringstream ss(line);

    std::string op;
    ss >> op;

    for(unsigned int i = 0; i < OPS_NUMBER; i++) {
        if(op == OPCODES[i].name) {
            Instruction instr(OPCODES[i].op);

            if(OPCODES[i].argumentsNumber == 0)
                return instr;

            std::vector<std::string> args = splitArgs(restOfStream(ss), OPCODES[i].argumentsNumber);;
            for(unsigned int j = 0; j < OPCODES[i].argumentsNumber; j++) {
                instr.arguments[j] = parseArgument(args[j]);
            }

            return instr;
        }
    }

    throw ParsingError(std::string("Invalid instruction ") + op);
}

static bool isEmpty(std::string line) {
    int pos = 0;

    while(pos < line.length() && isspace(line[pos]))
        pos++;

    return line.length() == 0 || pos == line.length() - 1 || line[pos] == ';';
}

static bool isLabel(std::string line) {
    std::stringstream ss(line);
    std::string s;

    ss >> s;
    return s[s.size() - 1] == ':';
}

static std::string stripComment(std::string s) {
    int pos = 0;
    while(pos < s.size() && s[pos] != ';')
        pos++;

    return s.substr(0, pos);
}

void Parser::gatherLabels(std::vector<std::string>& lines) {
    for(auto& line : lines) {
        if(isLabel(line))
            labelMapping[line] = instructionNumber;
        else if(!isEmpty(line))
            instructionNumber++;
    }
}

std::vector<Instruction> Parser::parse(std::istream& input) {
    std::vector<Instruction> result;
    std::vector<std::string> lines;
    std::string line;

    try {
        while(std::getline(input, line)) {
            std::string nLine = trim(stripComment(line));
            std::transform(nLine.begin(), nLine.end(), nLine.begin(), ::toupper);
            lines.push_back(nLine);
        }
    } catch(std::ios_base::failure e) {
        if(!input.eof())
            throw e;
    }

    gatherLabels(lines);
    instructionNumber = 0;

    for(auto& line : lines) {
        if(isEmpty(line) || isLabel(line))
            continue;

        result.push_back(parseInstruction(line));
        instructionNumber++;
    }

    return result;
}

