#include "communicator.h"
#include "interpreter.h"

#include <iostream>

void Communicator::loop(std::istream& is, std::ostream& os) {
    std::string cmd;

    while(true) {
        is >> cmd;
        if(cmd == "STOP") {
            os << "ROGER_THAT\n";
            return;
        }

        if(cmd == "INIT") {
            PersistentWorldState persistent = parsePersistentWorldState(is);
            persistent.ghostNumber = ghostNumber;
            persistent.direction = 2; // FIXME: correct?

            worldState.persistent = persistent;
            os << "DONE 0 0\n";
        } else if(cmd == "STEP") {
            worldState.current = parseCurrentWorldState(is);

            try {
                ghcState.nextTick();
                while(ghcState.steps < EXECUTION_CYCLES * 50 && ghcStep(ghcState, worldState));
            } catch(GhcError e) {
                os << "ERROR " << e.what() << "\n";
                continue;
            }

            os << "DONE " << ghcState.steps << " 0 " << (int)worldState.persistent.direction << "\n";
        }
    }
}

