#!/usr/bin/env python2

import os
import sys

MAKE_DIRS = ['ghc', 'imp', 'lmc', 'mllc']
ROOT = os.path.dirname(os.path.abspath(__file__)) + '/..'
os.chdir(ROOT)

def compile_tools():
    for dir in MAKE_DIRS:
        os.system('make -C %s -j' % dir)

def recompile_tools():
    for dir in MAKE_DIRS:
        os.system('make -C %s clean; make -C %s -j' % (dir, dir))

def compile(name):
    _, ext = os.path.splitext(name)
    if ext.lower() == ".mlls":
        compile_lambdabot(name)
    elif ext.lower() == ".imp":
        compile_ghostbot(name)

def compile_lambdabot(name):
    bare_name, ext = os.path.splitext(name)

    source  = os.path.join('mllc/bots', name)
    labeled = os.path.join('lambdabots', '%s.gccl' % bare_name)
    final   = os.path.join('lambdabots', '%s.gcc' % bare_name)

    opt = 'O3'
    if os.getenv('OPTLEVEL'):
        opt = os.getenv('OPTLEVEL')

    args = '-' + opt

    os.system('mkdir -p %s' % os.path.dirname(final))
    os.system('./mllc/bin/mllc %s %s > %s' % (source, args, labeled))
    os.system('./mllc/bin/mllc %s %s | ./lmc/lmc > %s' % (source, args, final))

def compile_ghostbot(name):
    bare_name, ext = os.path.splitext(name)

    source  = os.path.join('imp/bots', name)
    preprocessed = os.path.join('ghostbots', '%s.impp' % bare_name)
    labeled = os.path.join('ghostbots', '%s.ghcl' % bare_name)
    final   = os.path.join('ghostbots', '%s.ghc' % bare_name)

    os.system('mkdir -p %s' % os.path.dirname(final))
    os.system('cpp -undef -nostdinc -P %s > %s' % (source, preprocessed))
    os.system('./imp/bin/imp %s > %s' % (preprocessed, labeled))
    os.system('./imp/bin/imp %s | ./ghc/ghc > %s' % (preprocessed, final))

def _prepare_lambdabot(name):
    bare_name, ext = os.path.splitext(name)
    if ext == '.mlls':
        compile_lambdabot(name)
        return os.path.join('lambdabots', '%s.gccl' % bare_name)

    if ext in ['.gcc', '.gccl']:
        return os.path.join('lambdabots', name)

    raise Error("Invalid lambdabot extension %s" % ext)

def _prepare_ghostbot(name):
    bare_name, ext = os.path.splitext(name)
    if ext == '.imp':
        compile_ghostbot(name)
        return os.path.join('ghostbots', '%s.ghcl' % bare_name)

    if ext in ['.ghc', '.ghcl']:
        return os.path.join('ghostbots', name)

    raise Error("Invalid ghostbot extension %s" % ext)

def simulate(map, lambdabot, ghostbot, *rest):
    lambdabot_path = _prepare_lambdabot(lambdabot)
    ghostbot_path  = _prepare_ghostbot(ghostbot)

    os.system('cd gov; node gov.js -s %s -p ../%s -e ../%s -l ../lmc/lmc -g ../ghc/ghc %s' %
             (map, lambdabot_path, ghostbot_path, ' '.join(rest)))

if __name__ == "__main__":
    cmds = {
        'compile-tools': compile_tools,
        'recompile-tools': recompile_tools,
        'compile': compile,
        'simulate': simulate,
    }

    cmd = cmds[sys.argv[1]]
    cmd(*sys.argv[2:])

